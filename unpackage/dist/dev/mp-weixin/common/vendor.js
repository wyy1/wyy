(global["webpackJsonp"] = global["webpackJsonp"] || []).push([["common/vendor"],{

/***/ 1:
/*!************************************************************!*\
  !*** ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {Object.defineProperty(exports, "__esModule", { value: true });exports.createApp = createApp;exports.createComponent = createComponent;exports.createPage = createPage;exports.createPlugin = createPlugin;exports.createSubpackageApp = createSubpackageApp;exports.default = void 0;var _uniI18n = __webpack_require__(/*! @dcloudio/uni-i18n */ 3);
var _vue = _interopRequireDefault(__webpack_require__(/*! vue */ 4));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}function ownKeys(object, enumerableOnly) {var keys = Object.keys(object);if (Object.getOwnPropertySymbols) {var symbols = Object.getOwnPropertySymbols(object);if (enumerableOnly) symbols = symbols.filter(function (sym) {return Object.getOwnPropertyDescriptor(object, sym).enumerable;});keys.push.apply(keys, symbols);}return keys;}function _objectSpread(target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i] != null ? arguments[i] : {};if (i % 2) {ownKeys(Object(source), true).forEach(function (key) {_defineProperty(target, key, source[key]);});} else if (Object.getOwnPropertyDescriptors) {Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));} else {ownKeys(Object(source)).forEach(function (key) {Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));});}}return target;}function _slicedToArray(arr, i) {return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest();}function _nonIterableRest() {throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");}function _iterableToArrayLimit(arr, i) {if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return;var _arr = [];var _n = true;var _d = false;var _e = undefined;try {for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {_arr.push(_s.value);if (i && _arr.length === i) break;}} catch (err) {_d = true;_e = err;} finally {try {if (!_n && _i["return"] != null) _i["return"]();} finally {if (_d) throw _e;}}return _arr;}function _arrayWithHoles(arr) {if (Array.isArray(arr)) return arr;}function _defineProperty(obj, key, value) {if (key in obj) {Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true });} else {obj[key] = value;}return obj;}function _toConsumableArray(arr) {return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread();}function _nonIterableSpread() {throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");}function _unsupportedIterableToArray(o, minLen) {if (!o) return;if (typeof o === "string") return _arrayLikeToArray(o, minLen);var n = Object.prototype.toString.call(o).slice(8, -1);if (n === "Object" && o.constructor) n = o.constructor.name;if (n === "Map" || n === "Set") return Array.from(o);if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);}function _iterableToArray(iter) {if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter);}function _arrayWithoutHoles(arr) {if (Array.isArray(arr)) return _arrayLikeToArray(arr);}function _arrayLikeToArray(arr, len) {if (len == null || len > arr.length) len = arr.length;for (var i = 0, arr2 = new Array(len); i < len; i++) {arr2[i] = arr[i];}return arr2;}

var realAtob;

var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
var b64re = /^(?:[A-Za-z\d+/]{4})*?(?:[A-Za-z\d+/]{2}(?:==)?|[A-Za-z\d+/]{3}=?)?$/;

if (typeof atob !== 'function') {
  realAtob = function realAtob(str) {
    str = String(str).replace(/[\t\n\f\r ]+/g, '');
    if (!b64re.test(str)) {throw new Error("Failed to execute 'atob' on 'Window': The string to be decoded is not correctly encoded.");}

    // Adding the padding if missing, for semplicity
    str += '=='.slice(2 - (str.length & 3));
    var bitmap;var result = '';var r1;var r2;var i = 0;
    for (; i < str.length;) {
      bitmap = b64.indexOf(str.charAt(i++)) << 18 | b64.indexOf(str.charAt(i++)) << 12 |
      (r1 = b64.indexOf(str.charAt(i++))) << 6 | (r2 = b64.indexOf(str.charAt(i++)));

      result += r1 === 64 ? String.fromCharCode(bitmap >> 16 & 255) :
      r2 === 64 ? String.fromCharCode(bitmap >> 16 & 255, bitmap >> 8 & 255) :
      String.fromCharCode(bitmap >> 16 & 255, bitmap >> 8 & 255, bitmap & 255);
    }
    return result;
  };
} else {
  // 注意atob只能在全局对象上调用，例如：`const Base64 = {atob};Base64.atob('xxxx')`是错误的用法
  realAtob = atob;
}

function b64DecodeUnicode(str) {
  return decodeURIComponent(realAtob(str).split('').map(function (c) {
    return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
  }).join(''));
}

function getCurrentUserInfo() {
  var token = wx.getStorageSync('uni_id_token') || '';
  var tokenArr = token.split('.');
  if (!token || tokenArr.length !== 3) {
    return {
      uid: null,
      role: [],
      permission: [],
      tokenExpired: 0 };

  }
  var userInfo;
  try {
    userInfo = JSON.parse(b64DecodeUnicode(tokenArr[1]));
  } catch (error) {
    throw new Error('获取当前用户信息出错，详细错误信息为：' + error.message);
  }
  userInfo.tokenExpired = userInfo.exp * 1000;
  delete userInfo.exp;
  delete userInfo.iat;
  return userInfo;
}

function uniIdMixin(Vue) {
  Vue.prototype.uniIDHasRole = function (roleId) {var _getCurrentUserInfo =


    getCurrentUserInfo(),role = _getCurrentUserInfo.role;
    return role.indexOf(roleId) > -1;
  };
  Vue.prototype.uniIDHasPermission = function (permissionId) {var _getCurrentUserInfo2 =


    getCurrentUserInfo(),permission = _getCurrentUserInfo2.permission;
    return this.uniIDHasRole('admin') || permission.indexOf(permissionId) > -1;
  };
  Vue.prototype.uniIDTokenValid = function () {var _getCurrentUserInfo3 =


    getCurrentUserInfo(),tokenExpired = _getCurrentUserInfo3.tokenExpired;
    return tokenExpired > Date.now();
  };
}

var _toString = Object.prototype.toString;
var hasOwnProperty = Object.prototype.hasOwnProperty;

function isFn(fn) {
  return typeof fn === 'function';
}

function isStr(str) {
  return typeof str === 'string';
}

function isPlainObject(obj) {
  return _toString.call(obj) === '[object Object]';
}

function hasOwn(obj, key) {
  return hasOwnProperty.call(obj, key);
}

function noop() {}

/**
                    * Create a cached version of a pure function.
                    */
function cached(fn) {
  var cache = Object.create(null);
  return function cachedFn(str) {
    var hit = cache[str];
    return hit || (cache[str] = fn(str));
  };
}

/**
   * Camelize a hyphen-delimited string.
   */
var camelizeRE = /-(\w)/g;
var camelize = cached(function (str) {
  return str.replace(camelizeRE, function (_, c) {return c ? c.toUpperCase() : '';});
});

function sortObject(obj) {
  var sortObj = {};
  if (isPlainObject(obj)) {
    Object.keys(obj).sort().forEach(function (key) {
      sortObj[key] = obj[key];
    });
  }
  return !Object.keys(sortObj) ? obj : sortObj;
}

var HOOKS = [
'invoke',
'success',
'fail',
'complete',
'returnValue'];


var globalInterceptors = {};
var scopedInterceptors = {};

function mergeHook(parentVal, childVal) {
  var res = childVal ?
  parentVal ?
  parentVal.concat(childVal) :
  Array.isArray(childVal) ?
  childVal : [childVal] :
  parentVal;
  return res ?
  dedupeHooks(res) :
  res;
}

function dedupeHooks(hooks) {
  var res = [];
  for (var i = 0; i < hooks.length; i++) {
    if (res.indexOf(hooks[i]) === -1) {
      res.push(hooks[i]);
    }
  }
  return res;
}

function removeHook(hooks, hook) {
  var index = hooks.indexOf(hook);
  if (index !== -1) {
    hooks.splice(index, 1);
  }
}

function mergeInterceptorHook(interceptor, option) {
  Object.keys(option).forEach(function (hook) {
    if (HOOKS.indexOf(hook) !== -1 && isFn(option[hook])) {
      interceptor[hook] = mergeHook(interceptor[hook], option[hook]);
    }
  });
}

function removeInterceptorHook(interceptor, option) {
  if (!interceptor || !option) {
    return;
  }
  Object.keys(option).forEach(function (hook) {
    if (HOOKS.indexOf(hook) !== -1 && isFn(option[hook])) {
      removeHook(interceptor[hook], option[hook]);
    }
  });
}

function addInterceptor(method, option) {
  if (typeof method === 'string' && isPlainObject(option)) {
    mergeInterceptorHook(scopedInterceptors[method] || (scopedInterceptors[method] = {}), option);
  } else if (isPlainObject(method)) {
    mergeInterceptorHook(globalInterceptors, method);
  }
}

function removeInterceptor(method, option) {
  if (typeof method === 'string') {
    if (isPlainObject(option)) {
      removeInterceptorHook(scopedInterceptors[method], option);
    } else {
      delete scopedInterceptors[method];
    }
  } else if (isPlainObject(method)) {
    removeInterceptorHook(globalInterceptors, method);
  }
}

function wrapperHook(hook) {
  return function (data) {
    return hook(data) || data;
  };
}

function isPromise(obj) {
  return !!obj && (typeof obj === 'object' || typeof obj === 'function') && typeof obj.then === 'function';
}

function queue(hooks, data) {
  var promise = false;
  for (var i = 0; i < hooks.length; i++) {
    var hook = hooks[i];
    if (promise) {
      promise = Promise.resolve(wrapperHook(hook));
    } else {
      var res = hook(data);
      if (isPromise(res)) {
        promise = Promise.resolve(res);
      }
      if (res === false) {
        return {
          then: function then() {} };

      }
    }
  }
  return promise || {
    then: function then(callback) {
      return callback(data);
    } };

}

function wrapperOptions(interceptor) {var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  ['success', 'fail', 'complete'].forEach(function (name) {
    if (Array.isArray(interceptor[name])) {
      var oldCallback = options[name];
      options[name] = function callbackInterceptor(res) {
        queue(interceptor[name], res).then(function (res) {
          /* eslint-disable no-mixed-operators */
          return isFn(oldCallback) && oldCallback(res) || res;
        });
      };
    }
  });
  return options;
}

function wrapperReturnValue(method, returnValue) {
  var returnValueHooks = [];
  if (Array.isArray(globalInterceptors.returnValue)) {
    returnValueHooks.push.apply(returnValueHooks, _toConsumableArray(globalInterceptors.returnValue));
  }
  var interceptor = scopedInterceptors[method];
  if (interceptor && Array.isArray(interceptor.returnValue)) {
    returnValueHooks.push.apply(returnValueHooks, _toConsumableArray(interceptor.returnValue));
  }
  returnValueHooks.forEach(function (hook) {
    returnValue = hook(returnValue) || returnValue;
  });
  return returnValue;
}

function getApiInterceptorHooks(method) {
  var interceptor = Object.create(null);
  Object.keys(globalInterceptors).forEach(function (hook) {
    if (hook !== 'returnValue') {
      interceptor[hook] = globalInterceptors[hook].slice();
    }
  });
  var scopedInterceptor = scopedInterceptors[method];
  if (scopedInterceptor) {
    Object.keys(scopedInterceptor).forEach(function (hook) {
      if (hook !== 'returnValue') {
        interceptor[hook] = (interceptor[hook] || []).concat(scopedInterceptor[hook]);
      }
    });
  }
  return interceptor;
}

function invokeApi(method, api, options) {for (var _len = arguments.length, params = new Array(_len > 3 ? _len - 3 : 0), _key = 3; _key < _len; _key++) {params[_key - 3] = arguments[_key];}
  var interceptor = getApiInterceptorHooks(method);
  if (interceptor && Object.keys(interceptor).length) {
    if (Array.isArray(interceptor.invoke)) {
      var res = queue(interceptor.invoke, options);
      return res.then(function (options) {
        return api.apply(void 0, [wrapperOptions(interceptor, options)].concat(params));
      });
    } else {
      return api.apply(void 0, [wrapperOptions(interceptor, options)].concat(params));
    }
  }
  return api.apply(void 0, [options].concat(params));
}

var promiseInterceptor = {
  returnValue: function returnValue(res) {
    if (!isPromise(res)) {
      return res;
    }
    return new Promise(function (resolve, reject) {
      res.then(function (res) {
        if (res[0]) {
          reject(res[0]);
        } else {
          resolve(res[1]);
        }
      });
    });
  } };


var SYNC_API_RE =
/^\$|Window$|WindowStyle$|sendHostEvent|sendNativeEvent|restoreGlobal|requireGlobal|getCurrentSubNVue|getMenuButtonBoundingClientRect|^report|interceptors|Interceptor$|getSubNVueById|requireNativePlugin|upx2px|hideKeyboard|canIUse|^create|Sync$|Manager$|base64ToArrayBuffer|arrayBufferToBase64|getLocale|setLocale|invokePushCallback|getWindowInfo|getDeviceInfo|getAppBaseInfo/;

var CONTEXT_API_RE = /^create|Manager$/;

// Context例外情况
var CONTEXT_API_RE_EXC = ['createBLEConnection'];

// 同步例外情况
var ASYNC_API = ['createBLEConnection'];

var CALLBACK_API_RE = /^on|^off/;

function isContextApi(name) {
  return CONTEXT_API_RE.test(name) && CONTEXT_API_RE_EXC.indexOf(name) === -1;
}
function isSyncApi(name) {
  return SYNC_API_RE.test(name) && ASYNC_API.indexOf(name) === -1;
}

function isCallbackApi(name) {
  return CALLBACK_API_RE.test(name) && name !== 'onPush';
}

function handlePromise(promise) {
  return promise.then(function (data) {
    return [null, data];
  }).
  catch(function (err) {return [err];});
}

function shouldPromise(name) {
  if (
  isContextApi(name) ||
  isSyncApi(name) ||
  isCallbackApi(name))
  {
    return false;
  }
  return true;
}

/* eslint-disable no-extend-native */
if (!Promise.prototype.finally) {
  Promise.prototype.finally = function (callback) {
    var promise = this.constructor;
    return this.then(
    function (value) {return promise.resolve(callback()).then(function () {return value;});},
    function (reason) {return promise.resolve(callback()).then(function () {
        throw reason;
      });});

  };
}

function promisify(name, api) {
  if (!shouldPromise(name)) {
    return api;
  }
  return function promiseApi() {var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};for (var _len2 = arguments.length, params = new Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {params[_key2 - 1] = arguments[_key2];}
    if (isFn(options.success) || isFn(options.fail) || isFn(options.complete)) {
      return wrapperReturnValue(name, invokeApi.apply(void 0, [name, api, options].concat(params)));
    }
    return wrapperReturnValue(name, handlePromise(new Promise(function (resolve, reject) {
      invokeApi.apply(void 0, [name, api, Object.assign({}, options, {
        success: resolve,
        fail: reject })].concat(
      params));
    })));
  };
}

var EPS = 1e-4;
var BASE_DEVICE_WIDTH = 750;
var isIOS = false;
var deviceWidth = 0;
var deviceDPR = 0;

function checkDeviceWidth() {var _wx$getSystemInfoSync =




  wx.getSystemInfoSync(),platform = _wx$getSystemInfoSync.platform,pixelRatio = _wx$getSystemInfoSync.pixelRatio,windowWidth = _wx$getSystemInfoSync.windowWidth; // uni=>wx runtime 编译目标是 uni 对象，内部不允许直接使用 uni

  deviceWidth = windowWidth;
  deviceDPR = pixelRatio;
  isIOS = platform === 'ios';
}

function upx2px(number, newDeviceWidth) {
  if (deviceWidth === 0) {
    checkDeviceWidth();
  }

  number = Number(number);
  if (number === 0) {
    return 0;
  }
  var result = number / BASE_DEVICE_WIDTH * (newDeviceWidth || deviceWidth);
  if (result < 0) {
    result = -result;
  }
  result = Math.floor(result + EPS);
  if (result === 0) {
    if (deviceDPR === 1 || !isIOS) {
      result = 1;
    } else {
      result = 0.5;
    }
  }
  return number < 0 ? -result : result;
}

var LOCALE_ZH_HANS = 'zh-Hans';
var LOCALE_ZH_HANT = 'zh-Hant';
var LOCALE_EN = 'en';
var LOCALE_FR = 'fr';
var LOCALE_ES = 'es';

var messages = {};

var locale;

{
  locale = normalizeLocale(wx.getSystemInfoSync().language) || LOCALE_EN;
}

function initI18nMessages() {
  if (!isEnableLocale()) {
    return;
  }
  var localeKeys = Object.keys(__uniConfig.locales);
  if (localeKeys.length) {
    localeKeys.forEach(function (locale) {
      var curMessages = messages[locale];
      var userMessages = __uniConfig.locales[locale];
      if (curMessages) {
        Object.assign(curMessages, userMessages);
      } else {
        messages[locale] = userMessages;
      }
    });
  }
}

initI18nMessages();

var i18n = (0, _uniI18n.initVueI18n)(
locale,
{});

var t = i18n.t;
var i18nMixin = i18n.mixin = {
  beforeCreate: function beforeCreate() {var _this = this;
    var unwatch = i18n.i18n.watchLocale(function () {
      _this.$forceUpdate();
    });
    this.$once('hook:beforeDestroy', function () {
      unwatch();
    });
  },
  methods: {
    $$t: function $$t(key, values) {
      return t(key, values);
    } } };


var setLocale = i18n.setLocale;
var getLocale = i18n.getLocale;

function initAppLocale(Vue, appVm, locale) {
  var state = Vue.observable({
    locale: locale || i18n.getLocale() });

  var localeWatchers = [];
  appVm.$watchLocale = function (fn) {
    localeWatchers.push(fn);
  };
  Object.defineProperty(appVm, '$locale', {
    get: function get() {
      return state.locale;
    },
    set: function set(v) {
      state.locale = v;
      localeWatchers.forEach(function (watch) {return watch(v);});
    } });

}

function isEnableLocale() {
  return typeof __uniConfig !== 'undefined' && __uniConfig.locales && !!Object.keys(__uniConfig.locales).length;
}

function include(str, parts) {
  return !!parts.find(function (part) {return str.indexOf(part) !== -1;});
}

function startsWith(str, parts) {
  return parts.find(function (part) {return str.indexOf(part) === 0;});
}

function normalizeLocale(locale, messages) {
  if (!locale) {
    return;
  }
  locale = locale.trim().replace(/_/g, '-');
  if (messages && messages[locale]) {
    return locale;
  }
  locale = locale.toLowerCase();
  if (locale === 'chinese') {
    // 支付宝
    return LOCALE_ZH_HANS;
  }
  if (locale.indexOf('zh') === 0) {
    if (locale.indexOf('-hans') > -1) {
      return LOCALE_ZH_HANS;
    }
    if (locale.indexOf('-hant') > -1) {
      return LOCALE_ZH_HANT;
    }
    if (include(locale, ['-tw', '-hk', '-mo', '-cht'])) {
      return LOCALE_ZH_HANT;
    }
    return LOCALE_ZH_HANS;
  }
  var lang = startsWith(locale, [LOCALE_EN, LOCALE_FR, LOCALE_ES]);
  if (lang) {
    return lang;
  }
}
// export function initI18n() {
//   const localeKeys = Object.keys(__uniConfig.locales || {})
//   if (localeKeys.length) {
//     localeKeys.forEach((locale) =>
//       i18n.add(locale, __uniConfig.locales[locale])
//     )
//   }
// }

function getLocale$1() {
  // 优先使用 $locale
  var app = getApp({
    allowDefault: true });

  if (app && app.$vm) {
    return app.$vm.$locale;
  }
  return normalizeLocale(wx.getSystemInfoSync().language) || LOCALE_EN;
}

function setLocale$1(locale) {
  var app = getApp();
  if (!app) {
    return false;
  }
  var oldLocale = app.$vm.$locale;
  if (oldLocale !== locale) {
    app.$vm.$locale = locale;
    onLocaleChangeCallbacks.forEach(function (fn) {return fn({
        locale: locale });});

    return true;
  }
  return false;
}

var onLocaleChangeCallbacks = [];
function onLocaleChange(fn) {
  if (onLocaleChangeCallbacks.indexOf(fn) === -1) {
    onLocaleChangeCallbacks.push(fn);
  }
}

if (typeof global !== 'undefined') {
  global.getLocale = getLocale$1;
}

var interceptors = {
  promiseInterceptor: promiseInterceptor };


var baseApi = /*#__PURE__*/Object.freeze({
  __proto__: null,
  upx2px: upx2px,
  getLocale: getLocale$1,
  setLocale: setLocale$1,
  onLocaleChange: onLocaleChange,
  addInterceptor: addInterceptor,
  removeInterceptor: removeInterceptor,
  interceptors: interceptors });


function findExistsPageIndex(url) {
  var pages = getCurrentPages();
  var len = pages.length;
  while (len--) {
    var page = pages[len];
    if (page.$page && page.$page.fullPath === url) {
      return len;
    }
  }
  return -1;
}

var redirectTo = {
  name: function name(fromArgs) {
    if (fromArgs.exists === 'back' && fromArgs.delta) {
      return 'navigateBack';
    }
    return 'redirectTo';
  },
  args: function args(fromArgs) {
    if (fromArgs.exists === 'back' && fromArgs.url) {
      var existsPageIndex = findExistsPageIndex(fromArgs.url);
      if (existsPageIndex !== -1) {
        var delta = getCurrentPages().length - 1 - existsPageIndex;
        if (delta > 0) {
          fromArgs.delta = delta;
        }
      }
    }
  } };


var previewImage = {
  args: function args(fromArgs) {
    var currentIndex = parseInt(fromArgs.current);
    if (isNaN(currentIndex)) {
      return;
    }
    var urls = fromArgs.urls;
    if (!Array.isArray(urls)) {
      return;
    }
    var len = urls.length;
    if (!len) {
      return;
    }
    if (currentIndex < 0) {
      currentIndex = 0;
    } else if (currentIndex >= len) {
      currentIndex = len - 1;
    }
    if (currentIndex > 0) {
      fromArgs.current = urls[currentIndex];
      fromArgs.urls = urls.filter(
      function (item, index) {return index < currentIndex ? item !== urls[currentIndex] : true;});

    } else {
      fromArgs.current = urls[0];
    }
    return {
      indicator: false,
      loop: false };

  } };


var UUID_KEY = '__DC_STAT_UUID';
var deviceId;
function useDeviceId(result) {
  deviceId = deviceId || wx.getStorageSync(UUID_KEY);
  if (!deviceId) {
    deviceId = Date.now() + '' + Math.floor(Math.random() * 1e7);
    wx.setStorage({
      key: UUID_KEY,
      data: deviceId });

  }
  result.deviceId = deviceId;
}

function addSafeAreaInsets(result) {
  if (result.safeArea) {
    var safeArea = result.safeArea;
    result.safeAreaInsets = {
      top: safeArea.top,
      left: safeArea.left,
      right: result.windowWidth - safeArea.right,
      bottom: result.screenHeight - safeArea.bottom };

  }
}

function populateParameters(result) {var _result$brand =





  result.brand,brand = _result$brand === void 0 ? '' : _result$brand,_result$model = result.model,model = _result$model === void 0 ? '' : _result$model,_result$system = result.system,system = _result$system === void 0 ? '' : _result$system,_result$language = result.language,language = _result$language === void 0 ? '' : _result$language,theme = result.theme,version = result.version,platform = result.platform,fontSizeSetting = result.fontSizeSetting,SDKVersion = result.SDKVersion,pixelRatio = result.pixelRatio,deviceOrientation = result.deviceOrientation;
  // const isQuickApp = "mp-weixin".indexOf('quickapp-webview') !== -1

  // osName osVersion
  var osName = '';
  var osVersion = '';
  {
    osName = system.split(' ')[0] || '';
    osVersion = system.split(' ')[1] || '';
  }
  var hostVersion = version;

  // deviceType
  var deviceType = getGetDeviceType(result, model);

  // deviceModel
  var deviceBrand = getDeviceBrand(brand);

  // hostName
  var _hostName = getHostName(result);

  // deviceOrientation
  var _deviceOrientation = deviceOrientation; // 仅 微信 百度 支持

  // devicePixelRatio
  var _devicePixelRatio = pixelRatio;

  // SDKVersion
  var _SDKVersion = SDKVersion;

  // hostLanguage
  var hostLanguage = language.replace(/_/g, '-');

  // wx.getAccountInfoSync

  var parameters = {
    appId: "",
    appName: "wyy",
    appVersion: "1.0.0",
    appVersionCode: "100",
    appLanguage: getAppLanguage(hostLanguage),
    uniCompileVersion: "3.4.15",
    uniRuntimeVersion: "3.4.15",
    uniPlatform: undefined || "mp-weixin",
    deviceBrand: deviceBrand,
    deviceModel: model,
    deviceType: deviceType,
    devicePixelRatio: _devicePixelRatio,
    deviceOrientation: _deviceOrientation,
    osName: osName.toLocaleLowerCase(),
    osVersion: osVersion,
    hostTheme: theme,
    hostVersion: hostVersion,
    hostLanguage: hostLanguage,
    hostName: _hostName,
    hostSDKVersion: _SDKVersion,
    hostFontSizeSetting: fontSizeSetting,
    windowTop: 0,
    windowBottom: 0,
    // TODO
    osLanguage: undefined,
    osTheme: undefined,
    ua: undefined,
    hostPackageName: undefined,
    browserName: undefined,
    browserVersion: undefined };


  Object.assign(result, parameters);
}

function getGetDeviceType(result, model) {
  var deviceType = result.deviceType || 'phone';
  {
    var deviceTypeMaps = {
      ipad: 'pad',
      windows: 'pc',
      mac: 'pc' };

    var deviceTypeMapsKeys = Object.keys(deviceTypeMaps);
    var _model = model.toLocaleLowerCase();
    for (var index = 0; index < deviceTypeMapsKeys.length; index++) {
      var _m = deviceTypeMapsKeys[index];
      if (_model.indexOf(_m) !== -1) {
        deviceType = deviceTypeMaps[_m];
        break;
      }
    }
  }
  return deviceType;
}

function getDeviceBrand(brand) {
  var deviceBrand = brand;
  if (deviceBrand) {
    deviceBrand = brand.toLocaleLowerCase();
  }
  return deviceBrand;
}

function getAppLanguage(defaultLanguage) {
  return getLocale$1 ?
  getLocale$1() :
  defaultLanguage;
}

function getHostName(result) {
  var _platform = 'WeChat';
  var _hostName = result.hostName || _platform; // mp-jd
  {
    if (result.environment) {
      _hostName = result.environment;
    } else if (result.host && result.host.env) {
      _hostName = result.host.env;
    }
  }

  return _hostName;
}

var getSystemInfo = {
  returnValue: function returnValue(result) {
    useDeviceId(result);
    addSafeAreaInsets(result);
    populateParameters(result);
  } };


var showActionSheet = {
  args: function args(fromArgs) {
    if (typeof fromArgs === 'object') {
      fromArgs.alertText = fromArgs.title;
    }
  } };


var getAppBaseInfo = {
  returnValue: function returnValue(result) {var _result =
    result,version = _result.version,language = _result.language,SDKVersion = _result.SDKVersion,theme = _result.theme;

    var _hostName = getHostName(result);

    var hostLanguage = language.replace('_', '-');

    result = sortObject(Object.assign(result, {
      appId: "",
      appName: "wyy",
      appVersion: "1.0.0",
      appVersionCode: "100",
      appLanguage: getAppLanguage(hostLanguage),
      hostVersion: version,
      hostLanguage: hostLanguage,
      hostName: _hostName,
      hostSDKVersion: SDKVersion,
      hostTheme: theme }));

  } };


var getDeviceInfo = {
  returnValue: function returnValue(result) {var _result2 =
    result,brand = _result2.brand,model = _result2.model;
    var deviceType = getGetDeviceType(result, model);
    var deviceBrand = getDeviceBrand(brand);
    useDeviceId(result);

    result = sortObject(Object.assign(result, {
      deviceType: deviceType,
      deviceBrand: deviceBrand,
      deviceModel: model }));

  } };


var getWindowInfo = {
  returnValue: function returnValue(result) {
    addSafeAreaInsets(result);

    result = sortObject(Object.assign(result, {
      windowTop: 0,
      windowBottom: 0 }));

  } };


// import navigateTo from 'uni-helpers/navigate-to'

var protocols = {
  redirectTo: redirectTo,
  // navigateTo,  // 由于在微信开发者工具的页面参数，会显示__id__参数，因此暂时关闭mp-weixin对于navigateTo的AOP
  previewImage: previewImage,
  getSystemInfo: getSystemInfo,
  getSystemInfoSync: getSystemInfo,
  showActionSheet: showActionSheet,
  getAppBaseInfo: getAppBaseInfo,
  getDeviceInfo: getDeviceInfo,
  getWindowInfo: getWindowInfo };

var todos = [
'vibrate',
'preloadPage',
'unPreloadPage',
'loadSubPackage'];

var canIUses = [];

var CALLBACKS = ['success', 'fail', 'cancel', 'complete'];

function processCallback(methodName, method, returnValue) {
  return function (res) {
    return method(processReturnValue(methodName, res, returnValue));
  };
}

function processArgs(methodName, fromArgs) {var argsOption = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};var returnValue = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};var keepFromArgs = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;
  if (isPlainObject(fromArgs)) {// 一般 api 的参数解析
    var toArgs = keepFromArgs === true ? fromArgs : {}; // returnValue 为 false 时，说明是格式化返回值，直接在返回值对象上修改赋值
    if (isFn(argsOption)) {
      argsOption = argsOption(fromArgs, toArgs) || {};
    }
    for (var key in fromArgs) {
      if (hasOwn(argsOption, key)) {
        var keyOption = argsOption[key];
        if (isFn(keyOption)) {
          keyOption = keyOption(fromArgs[key], fromArgs, toArgs);
        }
        if (!keyOption) {// 不支持的参数
          console.warn("The '".concat(methodName, "' method of platform '\u5FAE\u4FE1\u5C0F\u7A0B\u5E8F' does not support option '").concat(key, "'"));
        } else if (isStr(keyOption)) {// 重写参数 key
          toArgs[keyOption] = fromArgs[key];
        } else if (isPlainObject(keyOption)) {// {name:newName,value:value}可重新指定参数 key:value
          toArgs[keyOption.name ? keyOption.name : key] = keyOption.value;
        }
      } else if (CALLBACKS.indexOf(key) !== -1) {
        if (isFn(fromArgs[key])) {
          toArgs[key] = processCallback(methodName, fromArgs[key], returnValue);
        }
      } else {
        if (!keepFromArgs) {
          toArgs[key] = fromArgs[key];
        }
      }
    }
    return toArgs;
  } else if (isFn(fromArgs)) {
    fromArgs = processCallback(methodName, fromArgs, returnValue);
  }
  return fromArgs;
}

function processReturnValue(methodName, res, returnValue) {var keepReturnValue = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
  if (isFn(protocols.returnValue)) {// 处理通用 returnValue
    res = protocols.returnValue(methodName, res);
  }
  return processArgs(methodName, res, returnValue, {}, keepReturnValue);
}

function wrapper(methodName, method) {
  if (hasOwn(protocols, methodName)) {
    var protocol = protocols[methodName];
    if (!protocol) {// 暂不支持的 api
      return function () {
        console.error("Platform '\u5FAE\u4FE1\u5C0F\u7A0B\u5E8F' does not support '".concat(methodName, "'."));
      };
    }
    return function (arg1, arg2) {// 目前 api 最多两个参数
      var options = protocol;
      if (isFn(protocol)) {
        options = protocol(arg1);
      }

      arg1 = processArgs(methodName, arg1, options.args, options.returnValue);

      var args = [arg1];
      if (typeof arg2 !== 'undefined') {
        args.push(arg2);
      }
      if (isFn(options.name)) {
        methodName = options.name(arg1);
      } else if (isStr(options.name)) {
        methodName = options.name;
      }
      var returnValue = wx[methodName].apply(wx, args);
      if (isSyncApi(methodName)) {// 同步 api
        return processReturnValue(methodName, returnValue, options.returnValue, isContextApi(methodName));
      }
      return returnValue;
    };
  }
  return method;
}

var todoApis = Object.create(null);

var TODOS = [
'onTabBarMidButtonTap',
'subscribePush',
'unsubscribePush',
'onPush',
'offPush',
'share'];


function createTodoApi(name) {
  return function todoApi(_ref)


  {var fail = _ref.fail,complete = _ref.complete;
    var res = {
      errMsg: "".concat(name, ":fail method '").concat(name, "' not supported") };

    isFn(fail) && fail(res);
    isFn(complete) && complete(res);
  };
}

TODOS.forEach(function (name) {
  todoApis[name] = createTodoApi(name);
});

var providers = {
  oauth: ['weixin'],
  share: ['weixin'],
  payment: ['wxpay'],
  push: ['weixin'] };


function getProvider(_ref2)




{var service = _ref2.service,success = _ref2.success,fail = _ref2.fail,complete = _ref2.complete;
  var res = false;
  if (providers[service]) {
    res = {
      errMsg: 'getProvider:ok',
      service: service,
      provider: providers[service] };

    isFn(success) && success(res);
  } else {
    res = {
      errMsg: 'getProvider:fail service not found' };

    isFn(fail) && fail(res);
  }
  isFn(complete) && complete(res);
}

var extraApi = /*#__PURE__*/Object.freeze({
  __proto__: null,
  getProvider: getProvider });


var getEmitter = function () {
  var Emitter;
  return function getUniEmitter() {
    if (!Emitter) {
      Emitter = new _vue.default();
    }
    return Emitter;
  };
}();

function apply(ctx, method, args) {
  return ctx[method].apply(ctx, args);
}

function $on() {
  return apply(getEmitter(), '$on', Array.prototype.slice.call(arguments));
}
function $off() {
  return apply(getEmitter(), '$off', Array.prototype.slice.call(arguments));
}
function $once() {
  return apply(getEmitter(), '$once', Array.prototype.slice.call(arguments));
}
function $emit() {
  return apply(getEmitter(), '$emit', Array.prototype.slice.call(arguments));
}

var eventApi = /*#__PURE__*/Object.freeze({
  __proto__: null,
  $on: $on,
  $off: $off,
  $once: $once,
  $emit: $emit });


/**
                    * 框架内 try-catch
                    */
/**
                        * 开发者 try-catch
                        */
function tryCatch(fn) {
  return function () {
    try {
      return fn.apply(fn, arguments);
    } catch (e) {
      // TODO
      console.error(e);
    }
  };
}

function getApiCallbacks(params) {
  var apiCallbacks = {};
  for (var name in params) {
    var param = params[name];
    if (isFn(param)) {
      apiCallbacks[name] = tryCatch(param);
      delete params[name];
    }
  }
  return apiCallbacks;
}

var cid;
var cidErrMsg;

function normalizePushMessage(message) {
  try {
    return JSON.parse(message);
  } catch (e) {}
  return message;
}

function invokePushCallback(
args)
{
  if (args.type === 'clientId') {
    cid = args.cid;
    cidErrMsg = args.errMsg;
    invokeGetPushCidCallbacks(cid, args.errMsg);
  } else if (args.type === 'pushMsg') {
    onPushMessageCallbacks.forEach(function (callback) {
      callback({
        type: 'receive',
        data: normalizePushMessage(args.message) });

    });
  } else if (args.type === 'click') {
    onPushMessageCallbacks.forEach(function (callback) {
      callback({
        type: 'click',
        data: normalizePushMessage(args.message) });

    });
  }
}

var getPushCidCallbacks = [];

function invokeGetPushCidCallbacks(cid, errMsg) {
  getPushCidCallbacks.forEach(function (callback) {
    callback(cid, errMsg);
  });
  getPushCidCallbacks.length = 0;
}

function getPushClientid(args) {
  if (!isPlainObject(args)) {
    args = {};
  }var _getApiCallbacks =




  getApiCallbacks(args),success = _getApiCallbacks.success,fail = _getApiCallbacks.fail,complete = _getApiCallbacks.complete;
  var hasSuccess = isFn(success);
  var hasFail = isFn(fail);
  var hasComplete = isFn(complete);
  getPushCidCallbacks.push(function (cid, errMsg) {
    var res;
    if (cid) {
      res = {
        errMsg: 'getPushClientid:ok',
        cid: cid };

      hasSuccess && success(res);
    } else {
      res = {
        errMsg: 'getPushClientid:fail' + (errMsg ? ' ' + errMsg : '') };

      hasFail && fail(res);
    }
    hasComplete && complete(res);
  });
  if (typeof cid !== 'undefined') {
    Promise.resolve().then(function () {return invokeGetPushCidCallbacks(cid, cidErrMsg);});
  }
}

var onPushMessageCallbacks = [];
// 不使用 defineOnApi 实现，是因为 defineOnApi 依赖 UniServiceJSBridge ，该对象目前在小程序上未提供，故简单实现
var onPushMessage = function onPushMessage(fn) {
  if (onPushMessageCallbacks.indexOf(fn) === -1) {
    onPushMessageCallbacks.push(fn);
  }
};

var offPushMessage = function offPushMessage(fn) {
  if (!fn) {
    onPushMessageCallbacks.length = 0;
  } else {
    var index = onPushMessageCallbacks.indexOf(fn);
    if (index > -1) {
      onPushMessageCallbacks.splice(index, 1);
    }
  }
};

var api = /*#__PURE__*/Object.freeze({
  __proto__: null,
  getPushClientid: getPushClientid,
  onPushMessage: onPushMessage,
  offPushMessage: offPushMessage,
  invokePushCallback: invokePushCallback });


var MPPage = Page;
var MPComponent = Component;

var customizeRE = /:/g;

var customize = cached(function (str) {
  return camelize(str.replace(customizeRE, '-'));
});

function initTriggerEvent(mpInstance) {
  var oldTriggerEvent = mpInstance.triggerEvent;
  var newTriggerEvent = function newTriggerEvent(event) {for (var _len3 = arguments.length, args = new Array(_len3 > 1 ? _len3 - 1 : 0), _key3 = 1; _key3 < _len3; _key3++) {args[_key3 - 1] = arguments[_key3];}
    return oldTriggerEvent.apply(mpInstance, [customize(event)].concat(args));
  };
  try {
    // 京东小程序 triggerEvent 为只读
    mpInstance.triggerEvent = newTriggerEvent;
  } catch (error) {
    mpInstance._triggerEvent = newTriggerEvent;
  }
}

function initHook(name, options, isComponent) {
  var oldHook = options[name];
  if (!oldHook) {
    options[name] = function () {
      initTriggerEvent(this);
    };
  } else {
    options[name] = function () {
      initTriggerEvent(this);for (var _len4 = arguments.length, args = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {args[_key4] = arguments[_key4];}
      return oldHook.apply(this, args);
    };
  }
}
if (!MPPage.__$wrappered) {
  MPPage.__$wrappered = true;
  Page = function Page() {var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    initHook('onLoad', options);
    return MPPage(options);
  };
  Page.after = MPPage.after;

  Component = function Component() {var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    initHook('created', options);
    return MPComponent(options);
  };
}

var PAGE_EVENT_HOOKS = [
'onPullDownRefresh',
'onReachBottom',
'onAddToFavorites',
'onShareTimeline',
'onShareAppMessage',
'onPageScroll',
'onResize',
'onTabItemTap'];


function initMocks(vm, mocks) {
  var mpInstance = vm.$mp[vm.mpType];
  mocks.forEach(function (mock) {
    if (hasOwn(mpInstance, mock)) {
      vm[mock] = mpInstance[mock];
    }
  });
}

function hasHook(hook, vueOptions) {
  if (!vueOptions) {
    return true;
  }

  if (_vue.default.options && Array.isArray(_vue.default.options[hook])) {
    return true;
  }

  vueOptions = vueOptions.default || vueOptions;

  if (isFn(vueOptions)) {
    if (isFn(vueOptions.extendOptions[hook])) {
      return true;
    }
    if (vueOptions.super &&
    vueOptions.super.options &&
    Array.isArray(vueOptions.super.options[hook])) {
      return true;
    }
    return false;
  }

  if (isFn(vueOptions[hook])) {
    return true;
  }
  var mixins = vueOptions.mixins;
  if (Array.isArray(mixins)) {
    return !!mixins.find(function (mixin) {return hasHook(hook, mixin);});
  }
}

function initHooks(mpOptions, hooks, vueOptions) {
  hooks.forEach(function (hook) {
    if (hasHook(hook, vueOptions)) {
      mpOptions[hook] = function (args) {
        return this.$vm && this.$vm.__call_hook(hook, args);
      };
    }
  });
}

function initVueComponent(Vue, vueOptions) {
  vueOptions = vueOptions.default || vueOptions;
  var VueComponent;
  if (isFn(vueOptions)) {
    VueComponent = vueOptions;
  } else {
    VueComponent = Vue.extend(vueOptions);
  }
  vueOptions = VueComponent.options;
  return [VueComponent, vueOptions];
}

function initSlots(vm, vueSlots) {
  if (Array.isArray(vueSlots) && vueSlots.length) {
    var $slots = Object.create(null);
    vueSlots.forEach(function (slotName) {
      $slots[slotName] = true;
    });
    vm.$scopedSlots = vm.$slots = $slots;
  }
}

function initVueIds(vueIds, mpInstance) {
  vueIds = (vueIds || '').split(',');
  var len = vueIds.length;

  if (len === 1) {
    mpInstance._$vueId = vueIds[0];
  } else if (len === 2) {
    mpInstance._$vueId = vueIds[0];
    mpInstance._$vuePid = vueIds[1];
  }
}

function initData(vueOptions, context) {
  var data = vueOptions.data || {};
  var methods = vueOptions.methods || {};

  if (typeof data === 'function') {
    try {
      data = data.call(context); // 支持 Vue.prototype 上挂的数据
    } catch (e) {
      if (Object({"NODE_ENV":"development","VUE_APP_NAME":"wyy","VUE_APP_PLATFORM":"mp-weixin","BASE_URL":"/"}).VUE_APP_DEBUG) {
        console.warn('根据 Vue 的 data 函数初始化小程序 data 失败，请尽量确保 data 函数中不访问 vm 对象，否则可能影响首次数据渲染速度。', data);
      }
    }
  } else {
    try {
      // 对 data 格式化
      data = JSON.parse(JSON.stringify(data));
    } catch (e) {}
  }

  if (!isPlainObject(data)) {
    data = {};
  }

  Object.keys(methods).forEach(function (methodName) {
    if (context.__lifecycle_hooks__.indexOf(methodName) === -1 && !hasOwn(data, methodName)) {
      data[methodName] = methods[methodName];
    }
  });

  return data;
}

var PROP_TYPES = [String, Number, Boolean, Object, Array, null];

function createObserver(name) {
  return function observer(newVal, oldVal) {
    if (this.$vm) {
      this.$vm[name] = newVal; // 为了触发其他非 render watcher
    }
  };
}

function initBehaviors(vueOptions, initBehavior) {
  var vueBehaviors = vueOptions.behaviors;
  var vueExtends = vueOptions.extends;
  var vueMixins = vueOptions.mixins;

  var vueProps = vueOptions.props;

  if (!vueProps) {
    vueOptions.props = vueProps = [];
  }

  var behaviors = [];
  if (Array.isArray(vueBehaviors)) {
    vueBehaviors.forEach(function (behavior) {
      behaviors.push(behavior.replace('uni://', "wx".concat("://")));
      if (behavior === 'uni://form-field') {
        if (Array.isArray(vueProps)) {
          vueProps.push('name');
          vueProps.push('value');
        } else {
          vueProps.name = {
            type: String,
            default: '' };

          vueProps.value = {
            type: [String, Number, Boolean, Array, Object, Date],
            default: '' };

        }
      }
    });
  }
  if (isPlainObject(vueExtends) && vueExtends.props) {
    behaviors.push(
    initBehavior({
      properties: initProperties(vueExtends.props, true) }));


  }
  if (Array.isArray(vueMixins)) {
    vueMixins.forEach(function (vueMixin) {
      if (isPlainObject(vueMixin) && vueMixin.props) {
        behaviors.push(
        initBehavior({
          properties: initProperties(vueMixin.props, true) }));


      }
    });
  }
  return behaviors;
}

function parsePropType(key, type, defaultValue, file) {
  // [String]=>String
  if (Array.isArray(type) && type.length === 1) {
    return type[0];
  }
  return type;
}

function initProperties(props) {var isBehavior = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;var file = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';
  var properties = {};
  if (!isBehavior) {
    properties.vueId = {
      type: String,
      value: '' };

    // 用于字节跳动小程序模拟抽象节点
    properties.generic = {
      type: Object,
      value: null };

    // scopedSlotsCompiler auto
    properties.scopedSlotsCompiler = {
      type: String,
      value: '' };

    properties.vueSlots = { // 小程序不能直接定义 $slots 的 props，所以通过 vueSlots 转换到 $slots
      type: null,
      value: [],
      observer: function observer(newVal, oldVal) {
        var $slots = Object.create(null);
        newVal.forEach(function (slotName) {
          $slots[slotName] = true;
        });
        this.setData({
          $slots: $slots });

      } };

  }
  if (Array.isArray(props)) {// ['title']
    props.forEach(function (key) {
      properties[key] = {
        type: null,
        observer: createObserver(key) };

    });
  } else if (isPlainObject(props)) {// {title:{type:String,default:''},content:String}
    Object.keys(props).forEach(function (key) {
      var opts = props[key];
      if (isPlainObject(opts)) {// title:{type:String,default:''}
        var value = opts.default;
        if (isFn(value)) {
          value = value();
        }

        opts.type = parsePropType(key, opts.type);

        properties[key] = {
          type: PROP_TYPES.indexOf(opts.type) !== -1 ? opts.type : null,
          value: value,
          observer: createObserver(key) };

      } else {// content:String
        var type = parsePropType(key, opts);
        properties[key] = {
          type: PROP_TYPES.indexOf(type) !== -1 ? type : null,
          observer: createObserver(key) };

      }
    });
  }
  return properties;
}

function wrapper$1(event) {
  // TODO 又得兼容 mpvue 的 mp 对象
  try {
    event.mp = JSON.parse(JSON.stringify(event));
  } catch (e) {}

  event.stopPropagation = noop;
  event.preventDefault = noop;

  event.target = event.target || {};

  if (!hasOwn(event, 'detail')) {
    event.detail = {};
  }

  if (hasOwn(event, 'markerId')) {
    event.detail = typeof event.detail === 'object' ? event.detail : {};
    event.detail.markerId = event.markerId;
  }

  if (isPlainObject(event.detail)) {
    event.target = Object.assign({}, event.target, event.detail);
  }

  return event;
}

function getExtraValue(vm, dataPathsArray) {
  var context = vm;
  dataPathsArray.forEach(function (dataPathArray) {
    var dataPath = dataPathArray[0];
    var value = dataPathArray[2];
    if (dataPath || typeof value !== 'undefined') {// ['','',index,'disable']
      var propPath = dataPathArray[1];
      var valuePath = dataPathArray[3];

      var vFor;
      if (Number.isInteger(dataPath)) {
        vFor = dataPath;
      } else if (!dataPath) {
        vFor = context;
      } else if (typeof dataPath === 'string' && dataPath) {
        if (dataPath.indexOf('#s#') === 0) {
          vFor = dataPath.substr(3);
        } else {
          vFor = vm.__get_value(dataPath, context);
        }
      }

      if (Number.isInteger(vFor)) {
        context = value;
      } else if (!propPath) {
        context = vFor[value];
      } else {
        if (Array.isArray(vFor)) {
          context = vFor.find(function (vForItem) {
            return vm.__get_value(propPath, vForItem) === value;
          });
        } else if (isPlainObject(vFor)) {
          context = Object.keys(vFor).find(function (vForKey) {
            return vm.__get_value(propPath, vFor[vForKey]) === value;
          });
        } else {
          console.error('v-for 暂不支持循环数据：', vFor);
        }
      }

      if (valuePath) {
        context = vm.__get_value(valuePath, context);
      }
    }
  });
  return context;
}

function processEventExtra(vm, extra, event) {
  var extraObj = {};

  if (Array.isArray(extra) && extra.length) {
    /**
                                              *[
                                              *    ['data.items', 'data.id', item.data.id],
                                              *    ['metas', 'id', meta.id]
                                              *],
                                              *[
                                              *    ['data.items', 'data.id', item.data.id],
                                              *    ['metas', 'id', meta.id]
                                              *],
                                              *'test'
                                              */
    extra.forEach(function (dataPath, index) {
      if (typeof dataPath === 'string') {
        if (!dataPath) {// model,prop.sync
          extraObj['$' + index] = vm;
        } else {
          if (dataPath === '$event') {// $event
            extraObj['$' + index] = event;
          } else if (dataPath === 'arguments') {
            if (event.detail && event.detail.__args__) {
              extraObj['$' + index] = event.detail.__args__;
            } else {
              extraObj['$' + index] = [event];
            }
          } else if (dataPath.indexOf('$event.') === 0) {// $event.target.value
            extraObj['$' + index] = vm.__get_value(dataPath.replace('$event.', ''), event);
          } else {
            extraObj['$' + index] = vm.__get_value(dataPath);
          }
        }
      } else {
        extraObj['$' + index] = getExtraValue(vm, dataPath);
      }
    });
  }

  return extraObj;
}

function getObjByArray(arr) {
  var obj = {};
  for (var i = 1; i < arr.length; i++) {
    var element = arr[i];
    obj[element[0]] = element[1];
  }
  return obj;
}

function processEventArgs(vm, event) {var args = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];var extra = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : [];var isCustom = arguments.length > 4 ? arguments[4] : undefined;var methodName = arguments.length > 5 ? arguments[5] : undefined;
  var isCustomMPEvent = false; // wxcomponent 组件，传递原始 event 对象
  if (isCustom) {// 自定义事件
    isCustomMPEvent = event.currentTarget &&
    event.currentTarget.dataset &&
    event.currentTarget.dataset.comType === 'wx';
    if (!args.length) {// 无参数，直接传入 event 或 detail 数组
      if (isCustomMPEvent) {
        return [event];
      }
      return event.detail.__args__ || event.detail;
    }
  }

  var extraObj = processEventExtra(vm, extra, event);

  var ret = [];
  args.forEach(function (arg) {
    if (arg === '$event') {
      if (methodName === '__set_model' && !isCustom) {// input v-model value
        ret.push(event.target.value);
      } else {
        if (isCustom && !isCustomMPEvent) {
          ret.push(event.detail.__args__[0]);
        } else {// wxcomponent 组件或内置组件
          ret.push(event);
        }
      }
    } else {
      if (Array.isArray(arg) && arg[0] === 'o') {
        ret.push(getObjByArray(arg));
      } else if (typeof arg === 'string' && hasOwn(extraObj, arg)) {
        ret.push(extraObj[arg]);
      } else {
        ret.push(arg);
      }
    }
  });

  return ret;
}

var ONCE = '~';
var CUSTOM = '^';

function isMatchEventType(eventType, optType) {
  return eventType === optType ||

  optType === 'regionchange' && (

  eventType === 'begin' ||
  eventType === 'end');


}

function getContextVm(vm) {
  var $parent = vm.$parent;
  // 父组件是 scoped slots 或者其他自定义组件时继续查找
  while ($parent && $parent.$parent && ($parent.$options.generic || $parent.$parent.$options.generic || $parent.$scope._$vuePid)) {
    $parent = $parent.$parent;
  }
  return $parent && $parent.$parent;
}

function handleEvent(event) {var _this2 = this;
  event = wrapper$1(event);

  // [['tap',[['handle',[1,2,a]],['handle1',[1,2,a]]]]]
  var dataset = (event.currentTarget || event.target).dataset;
  if (!dataset) {
    return console.warn('事件信息不存在');
  }
  var eventOpts = dataset.eventOpts || dataset['event-opts']; // 支付宝 web-view 组件 dataset 非驼峰
  if (!eventOpts) {
    return console.warn('事件信息不存在');
  }

  // [['handle',[1,2,a]],['handle1',[1,2,a]]]
  var eventType = event.type;

  var ret = [];

  eventOpts.forEach(function (eventOpt) {
    var type = eventOpt[0];
    var eventsArray = eventOpt[1];

    var isCustom = type.charAt(0) === CUSTOM;
    type = isCustom ? type.slice(1) : type;
    var isOnce = type.charAt(0) === ONCE;
    type = isOnce ? type.slice(1) : type;

    if (eventsArray && isMatchEventType(eventType, type)) {
      eventsArray.forEach(function (eventArray) {
        var methodName = eventArray[0];
        if (methodName) {
          var handlerCtx = _this2.$vm;
          if (handlerCtx.$options.generic) {// mp-weixin,mp-toutiao 抽象节点模拟 scoped slots
            handlerCtx = getContextVm(handlerCtx) || handlerCtx;
          }
          if (methodName === '$emit') {
            handlerCtx.$emit.apply(handlerCtx,
            processEventArgs(
            _this2.$vm,
            event,
            eventArray[1],
            eventArray[2],
            isCustom,
            methodName));

            return;
          }
          var handler = handlerCtx[methodName];
          if (!isFn(handler)) {
            throw new Error(" _vm.".concat(methodName, " is not a function"));
          }
          if (isOnce) {
            if (handler.once) {
              return;
            }
            handler.once = true;
          }
          var params = processEventArgs(
          _this2.$vm,
          event,
          eventArray[1],
          eventArray[2],
          isCustom,
          methodName);

          params = Array.isArray(params) ? params : [];
          // 参数尾部增加原始事件对象用于复杂表达式内获取额外数据
          if (/=\s*\S+\.eventParams\s*\|\|\s*\S+\[['"]event-params['"]\]/.test(handler.toString())) {
            // eslint-disable-next-line no-sparse-arrays
            params = params.concat([,,,,,,,,,, event]);
          }
          ret.push(handler.apply(handlerCtx, params));
        }
      });
    }
  });

  if (
  eventType === 'input' &&
  ret.length === 1 &&
  typeof ret[0] !== 'undefined')
  {
    return ret[0];
  }
}

var eventChannels = {};

var eventChannelStack = [];

function getEventChannel(id) {
  if (id) {
    var eventChannel = eventChannels[id];
    delete eventChannels[id];
    return eventChannel;
  }
  return eventChannelStack.shift();
}

var hooks = [
'onShow',
'onHide',
'onError',
'onPageNotFound',
'onThemeChange',
'onUnhandledRejection'];


function initEventChannel() {
  _vue.default.prototype.getOpenerEventChannel = function () {
    // 微信小程序使用自身getOpenerEventChannel
    {
      return this.$scope.getOpenerEventChannel();
    }
  };
  var callHook = _vue.default.prototype.__call_hook;
  _vue.default.prototype.__call_hook = function (hook, args) {
    if (hook === 'onLoad' && args && args.__id__) {
      this.__eventChannel__ = getEventChannel(args.__id__);
      delete args.__id__;
    }
    return callHook.call(this, hook, args);
  };
}

function initScopedSlotsParams() {
  var center = {};
  var parents = {};

  _vue.default.prototype.$hasScopedSlotsParams = function (vueId) {
    var has = center[vueId];
    if (!has) {
      parents[vueId] = this;
      this.$on('hook:destroyed', function () {
        delete parents[vueId];
      });
    }
    return has;
  };

  _vue.default.prototype.$getScopedSlotsParams = function (vueId, name, key) {
    var data = center[vueId];
    if (data) {
      var object = data[name] || {};
      return key ? object[key] : object;
    } else {
      parents[vueId] = this;
      this.$on('hook:destroyed', function () {
        delete parents[vueId];
      });
    }
  };

  _vue.default.prototype.$setScopedSlotsParams = function (name, value) {
    var vueIds = this.$options.propsData.vueId;
    if (vueIds) {
      var vueId = vueIds.split(',')[0];
      var object = center[vueId] = center[vueId] || {};
      object[name] = value;
      if (parents[vueId]) {
        parents[vueId].$forceUpdate();
      }
    }
  };

  _vue.default.mixin({
    destroyed: function destroyed() {
      var propsData = this.$options.propsData;
      var vueId = propsData && propsData.vueId;
      if (vueId) {
        delete center[vueId];
        delete parents[vueId];
      }
    } });

}

function parseBaseApp(vm, _ref3)


{var mocks = _ref3.mocks,initRefs = _ref3.initRefs;
  initEventChannel();
  {
    initScopedSlotsParams();
  }
  if (vm.$options.store) {
    _vue.default.prototype.$store = vm.$options.store;
  }
  uniIdMixin(_vue.default);

  _vue.default.prototype.mpHost = "mp-weixin";

  _vue.default.mixin({
    beforeCreate: function beforeCreate() {
      if (!this.$options.mpType) {
        return;
      }

      this.mpType = this.$options.mpType;

      this.$mp = _defineProperty({
        data: {} },
      this.mpType, this.$options.mpInstance);


      this.$scope = this.$options.mpInstance;

      delete this.$options.mpType;
      delete this.$options.mpInstance;
      if (this.mpType === 'page' && typeof getApp === 'function') {// hack vue-i18n
        var app = getApp();
        if (app.$vm && app.$vm.$i18n) {
          this._i18n = app.$vm.$i18n;
        }
      }
      if (this.mpType !== 'app') {
        initRefs(this);
        initMocks(this, mocks);
      }
    } });


  var appOptions = {
    onLaunch: function onLaunch(args) {
      if (this.$vm) {// 已经初始化过了，主要是为了百度，百度 onShow 在 onLaunch 之前
        return;
      }
      {
        if (wx.canIUse && !wx.canIUse('nextTick')) {// 事实 上2.2.3 即可，简单使用 2.3.0 的 nextTick 判断
          console.error('当前微信基础库版本过低，请将 微信开发者工具-详情-项目设置-调试基础库版本 更换为`2.3.0`以上');
        }
      }

      this.$vm = vm;

      this.$vm.$mp = {
        app: this };


      this.$vm.$scope = this;
      // vm 上也挂载 globalData
      this.$vm.globalData = this.globalData;

      this.$vm._isMounted = true;
      this.$vm.__call_hook('mounted', args);

      this.$vm.__call_hook('onLaunch', args);
    } };


  // 兼容旧版本 globalData
  appOptions.globalData = vm.$options.globalData || {};
  // 将 methods 中的方法挂在 getApp() 中
  var methods = vm.$options.methods;
  if (methods) {
    Object.keys(methods).forEach(function (name) {
      appOptions[name] = methods[name];
    });
  }

  initAppLocale(_vue.default, vm, normalizeLocale(wx.getSystemInfoSync().language) || LOCALE_EN);

  initHooks(appOptions, hooks);

  return appOptions;
}

var mocks = ['__route__', '__wxExparserNodeId__', '__wxWebviewId__'];

function findVmByVueId(vm, vuePid) {
  var $children = vm.$children;
  // 优先查找直属(反向查找:https://github.com/dcloudio/uni-app/issues/1200)
  for (var i = $children.length - 1; i >= 0; i--) {
    var childVm = $children[i];
    if (childVm.$scope._$vueId === vuePid) {
      return childVm;
    }
  }
  // 反向递归查找
  var parentVm;
  for (var _i = $children.length - 1; _i >= 0; _i--) {
    parentVm = findVmByVueId($children[_i], vuePid);
    if (parentVm) {
      return parentVm;
    }
  }
}

function initBehavior(options) {
  return Behavior(options);
}

function isPage() {
  return !!this.route;
}

function initRelation(detail) {
  this.triggerEvent('__l', detail);
}

function selectAllComponents(mpInstance, selector, $refs) {
  var components = mpInstance.selectAllComponents(selector);
  components.forEach(function (component) {
    var ref = component.dataset.ref;
    $refs[ref] = component.$vm || component;
    {
      if (component.dataset.vueGeneric === 'scoped') {
        component.selectAllComponents('.scoped-ref').forEach(function (scopedComponent) {
          selectAllComponents(scopedComponent, selector, $refs);
        });
      }
    }
  });
}

function initRefs(vm) {
  var mpInstance = vm.$scope;
  Object.defineProperty(vm, '$refs', {
    get: function get() {
      var $refs = {};
      selectAllComponents(mpInstance, '.vue-ref', $refs);
      // TODO 暂不考虑 for 中的 scoped
      var forComponents = mpInstance.selectAllComponents('.vue-ref-in-for');
      forComponents.forEach(function (component) {
        var ref = component.dataset.ref;
        if (!$refs[ref]) {
          $refs[ref] = [];
        }
        $refs[ref].push(component.$vm || component);
      });
      return $refs;
    } });

}

function handleLink(event) {var _ref4 =



  event.detail || event.value,vuePid = _ref4.vuePid,vueOptions = _ref4.vueOptions; // detail 是微信,value 是百度(dipatch)

  var parentVm;

  if (vuePid) {
    parentVm = findVmByVueId(this.$vm, vuePid);
  }

  if (!parentVm) {
    parentVm = this.$vm;
  }

  vueOptions.parent = parentVm;
}

function parseApp(vm) {
  return parseBaseApp(vm, {
    mocks: mocks,
    initRefs: initRefs });

}

function createApp(vm) {
  App(parseApp(vm));
  return vm;
}

var encodeReserveRE = /[!'()*]/g;
var encodeReserveReplacer = function encodeReserveReplacer(c) {return '%' + c.charCodeAt(0).toString(16);};
var commaRE = /%2C/g;

// fixed encodeURIComponent which is more conformant to RFC3986:
// - escapes [!'()*]
// - preserve commas
var encode = function encode(str) {return encodeURIComponent(str).
  replace(encodeReserveRE, encodeReserveReplacer).
  replace(commaRE, ',');};

function stringifyQuery(obj) {var encodeStr = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : encode;
  var res = obj ? Object.keys(obj).map(function (key) {
    var val = obj[key];

    if (val === undefined) {
      return '';
    }

    if (val === null) {
      return encodeStr(key);
    }

    if (Array.isArray(val)) {
      var result = [];
      val.forEach(function (val2) {
        if (val2 === undefined) {
          return;
        }
        if (val2 === null) {
          result.push(encodeStr(key));
        } else {
          result.push(encodeStr(key) + '=' + encodeStr(val2));
        }
      });
      return result.join('&');
    }

    return encodeStr(key) + '=' + encodeStr(val);
  }).filter(function (x) {return x.length > 0;}).join('&') : null;
  return res ? "?".concat(res) : '';
}

function parseBaseComponent(vueComponentOptions)


{var _ref5 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},isPage = _ref5.isPage,initRelation = _ref5.initRelation;var _initVueComponent =
  initVueComponent(_vue.default, vueComponentOptions),_initVueComponent2 = _slicedToArray(_initVueComponent, 2),VueComponent = _initVueComponent2[0],vueOptions = _initVueComponent2[1];

  var options = _objectSpread({
    multipleSlots: true,
    addGlobalClass: true },
  vueOptions.options || {});


  {
    // 微信 multipleSlots 部分情况有 bug，导致内容顺序错乱 如 u-list，提供覆盖选项
    if (vueOptions['mp-weixin'] && vueOptions['mp-weixin'].options) {
      Object.assign(options, vueOptions['mp-weixin'].options);
    }
  }

  var componentOptions = {
    options: options,
    data: initData(vueOptions, _vue.default.prototype),
    behaviors: initBehaviors(vueOptions, initBehavior),
    properties: initProperties(vueOptions.props, false, vueOptions.__file),
    lifetimes: {
      attached: function attached() {
        var properties = this.properties;

        var options = {
          mpType: isPage.call(this) ? 'page' : 'component',
          mpInstance: this,
          propsData: properties };


        initVueIds(properties.vueId, this);

        // 处理父子关系
        initRelation.call(this, {
          vuePid: this._$vuePid,
          vueOptions: options });


        // 初始化 vue 实例
        this.$vm = new VueComponent(options);

        // 处理$slots,$scopedSlots（暂不支持动态变化$slots）
        initSlots(this.$vm, properties.vueSlots);

        // 触发首次 setData
        this.$vm.$mount();
      },
      ready: function ready() {
        // 当组件 props 默认值为 true，初始化时传入 false 会导致 created,ready 触发, 但 attached 不触发
        // https://developers.weixin.qq.com/community/develop/doc/00066ae2844cc0f8eb883e2a557800
        if (this.$vm) {
          this.$vm._isMounted = true;
          this.$vm.__call_hook('mounted');
          this.$vm.__call_hook('onReady');
        }
      },
      detached: function detached() {
        this.$vm && this.$vm.$destroy();
      } },

    pageLifetimes: {
      show: function show(args) {
        this.$vm && this.$vm.__call_hook('onPageShow', args);
      },
      hide: function hide() {
        this.$vm && this.$vm.__call_hook('onPageHide');
      },
      resize: function resize(size) {
        this.$vm && this.$vm.__call_hook('onPageResize', size);
      } },

    methods: {
      __l: handleLink,
      __e: handleEvent } };


  // externalClasses
  if (vueOptions.externalClasses) {
    componentOptions.externalClasses = vueOptions.externalClasses;
  }

  if (Array.isArray(vueOptions.wxsCallMethods)) {
    vueOptions.wxsCallMethods.forEach(function (callMethod) {
      componentOptions.methods[callMethod] = function (args) {
        return this.$vm[callMethod](args);
      };
    });
  }

  if (isPage) {
    return componentOptions;
  }
  return [componentOptions, VueComponent];
}

function parseComponent(vueComponentOptions) {
  return parseBaseComponent(vueComponentOptions, {
    isPage: isPage,
    initRelation: initRelation });

}

var hooks$1 = [
'onShow',
'onHide',
'onUnload'];


hooks$1.push.apply(hooks$1, PAGE_EVENT_HOOKS);

function parseBasePage(vuePageOptions, _ref6)


{var isPage = _ref6.isPage,initRelation = _ref6.initRelation;
  var pageOptions = parseComponent(vuePageOptions);

  initHooks(pageOptions.methods, hooks$1, vuePageOptions);

  pageOptions.methods.onLoad = function (query) {
    this.options = query;
    var copyQuery = Object.assign({}, query);
    delete copyQuery.__id__;
    this.$page = {
      fullPath: '/' + (this.route || this.is) + stringifyQuery(copyQuery) };

    this.$vm.$mp.query = query; // 兼容 mpvue
    this.$vm.__call_hook('onLoad', query);
  };

  return pageOptions;
}

function parsePage(vuePageOptions) {
  return parseBasePage(vuePageOptions, {
    isPage: isPage,
    initRelation: initRelation });

}

function createPage(vuePageOptions) {
  {
    return Component(parsePage(vuePageOptions));
  }
}

function createComponent(vueOptions) {
  {
    return Component(parseComponent(vueOptions));
  }
}

function createSubpackageApp(vm) {
  var appOptions = parseApp(vm);
  var app = getApp({
    allowDefault: true });

  vm.$scope = app;
  var globalData = app.globalData;
  if (globalData) {
    Object.keys(appOptions.globalData).forEach(function (name) {
      if (!hasOwn(globalData, name)) {
        globalData[name] = appOptions.globalData[name];
      }
    });
  }
  Object.keys(appOptions).forEach(function (name) {
    if (!hasOwn(app, name)) {
      app[name] = appOptions[name];
    }
  });
  if (isFn(appOptions.onShow) && wx.onAppShow) {
    wx.onAppShow(function () {for (var _len5 = arguments.length, args = new Array(_len5), _key5 = 0; _key5 < _len5; _key5++) {args[_key5] = arguments[_key5];}
      vm.__call_hook('onShow', args);
    });
  }
  if (isFn(appOptions.onHide) && wx.onAppHide) {
    wx.onAppHide(function () {for (var _len6 = arguments.length, args = new Array(_len6), _key6 = 0; _key6 < _len6; _key6++) {args[_key6] = arguments[_key6];}
      vm.__call_hook('onHide', args);
    });
  }
  if (isFn(appOptions.onLaunch)) {
    var args = wx.getLaunchOptionsSync && wx.getLaunchOptionsSync();
    vm.__call_hook('onLaunch', args);
  }
  return vm;
}

function createPlugin(vm) {
  var appOptions = parseApp(vm);
  if (isFn(appOptions.onShow) && wx.onAppShow) {
    wx.onAppShow(function () {for (var _len7 = arguments.length, args = new Array(_len7), _key7 = 0; _key7 < _len7; _key7++) {args[_key7] = arguments[_key7];}
      vm.__call_hook('onShow', args);
    });
  }
  if (isFn(appOptions.onHide) && wx.onAppHide) {
    wx.onAppHide(function () {for (var _len8 = arguments.length, args = new Array(_len8), _key8 = 0; _key8 < _len8; _key8++) {args[_key8] = arguments[_key8];}
      vm.__call_hook('onHide', args);
    });
  }
  if (isFn(appOptions.onLaunch)) {
    var args = wx.getLaunchOptionsSync && wx.getLaunchOptionsSync();
    vm.__call_hook('onLaunch', args);
  }
  return vm;
}

todos.forEach(function (todoApi) {
  protocols[todoApi] = false;
});

canIUses.forEach(function (canIUseApi) {
  var apiName = protocols[canIUseApi] && protocols[canIUseApi].name ? protocols[canIUseApi].name :
  canIUseApi;
  if (!wx.canIUse(apiName)) {
    protocols[canIUseApi] = false;
  }
});

var uni = {};

if (typeof Proxy !== 'undefined' && "mp-weixin" !== 'app-plus') {
  uni = new Proxy({}, {
    get: function get(target, name) {
      if (hasOwn(target, name)) {
        return target[name];
      }
      if (baseApi[name]) {
        return baseApi[name];
      }
      if (api[name]) {
        return promisify(name, api[name]);
      }
      {
        if (extraApi[name]) {
          return promisify(name, extraApi[name]);
        }
        if (todoApis[name]) {
          return promisify(name, todoApis[name]);
        }
      }
      if (eventApi[name]) {
        return eventApi[name];
      }
      if (!hasOwn(wx, name) && !hasOwn(protocols, name)) {
        return;
      }
      return promisify(name, wrapper(name, wx[name]));
    },
    set: function set(target, name, value) {
      target[name] = value;
      return true;
    } });

} else {
  Object.keys(baseApi).forEach(function (name) {
    uni[name] = baseApi[name];
  });

  {
    Object.keys(todoApis).forEach(function (name) {
      uni[name] = promisify(name, todoApis[name]);
    });
    Object.keys(extraApi).forEach(function (name) {
      uni[name] = promisify(name, todoApis[name]);
    });
  }

  Object.keys(eventApi).forEach(function (name) {
    uni[name] = eventApi[name];
  });

  Object.keys(api).forEach(function (name) {
    uni[name] = promisify(name, api[name]);
  });

  Object.keys(wx).forEach(function (name) {
    if (hasOwn(wx, name) || hasOwn(protocols, name)) {
      uni[name] = promisify(name, wrapper(name, wx[name]));
    }
  });
}

wx.createApp = createApp;
wx.createPage = createPage;
wx.createComponent = createComponent;
wx.createSubpackageApp = createSubpackageApp;
wx.createPlugin = createPlugin;

var uni$1 = uni;var _default =

uni$1;exports.default = _default;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../webpack/buildin/global.js */ 2)))

/***/ }),

/***/ 11:
/*!**********************************************************************************************************!*\
  !*** ./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib/runtime/componentNormalizer.js ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode, /* vue-cli only */
  components, // fixed by xxxxxx auto components
  renderjs // fixed by xxxxxx renderjs
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // fixed by xxxxxx auto components
  if (components) {
    if (!options.components) {
      options.components = {}
    }
    var hasOwn = Object.prototype.hasOwnProperty
    for (var name in components) {
      if (hasOwn.call(components, name) && !hasOwn.call(options.components, name)) {
        options.components[name] = components[name]
      }
    }
  }
  // fixed by xxxxxx renderjs
  if (renderjs) {
    (renderjs.beforeCreate || (renderjs.beforeCreate = [])).unshift(function() {
      this[renderjs.__module] = this
    });
    (options.mixins || (options.mixins = [])).push(renderjs)
  }

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 18:
/*!***************************************************!*\
  !*** C:/Users/王佳豪/Desktop/wyy/common/wjh_home.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(uni) {Object.defineProperty(exports, "__esModule", { value: true });exports.banner = banner;exports.ground = ground;exports.tui = tui;exports.alge = alge;exports.song = song;exports.allsong = allsong;exports.plays = plays;exports.play = play;exports.playAuido = playAuido;
var _config = __webpack_require__(/*! ./config */ 19);

function banner() {
  return new Promise(function (res, req) {
    uni.request({
      url: "".concat(_config.baseURL, "/banner"),
      method: "GET",
      data: {},
      success: function success(item) {
        res(item.data);
      } });

  });

}
function ground() {
  return new Promise(function (res, req) {
    uni.request({
      url: "".concat(_config.baseURL, "/homepage/dragon/ball/limit=8"),
      method: "GET",
      data: {},
      success: function success(item) {
        res(item.data);
      } });

  });

}
// 推荐歌单
function tui() {
  return new Promise(function (res, req) {
    uni.request({
      url: "".concat(_config.baseURL, "/personalized?limit=6"),
      method: "GET",
      data: {},
      success: function success(item) {
        res(item.data);
      } });

  });

}
// 推荐歌单
function alge() {
  return new Promise(function (res, req) {
    uni.request({
      url: "".concat(_config.baseURL, "/personalized/newsong"),
      method: "GET",
      data: {},
      success: function success(item) {
        res(item.data);
      } });

  });

}
// 获取歌单
function song(id) {
  return new Promise(function (res, req) {
    uni.request({
      url: "".concat(_config.baseURL, "/playlist/detail?id=").concat(id),
      method: "GET",
      data: {},
      success: function success(item) {
        res(item.data);
      } });

  });

}
// 获取歌单详情
function allsong(id) {
  return new Promise(function (res, req) {
    uni.request({
      url: "".concat(_config.baseURL, "/playlist/track/all?id=").concat(id),
      method: "GET",
      data: {},
      success: function success(item) {
        res(item.data);
      } });

  });

}
// 播放歌词
function plays(id) {
  return new Promise(function (res, req) {
    uni.request({
      url: "".concat(_config.baseURL, "/lyric?id=").concat(id),
      method: "GET",
      data: {},
      success: function success(item) {
        res(item.data);
      } });

  });

}
// 获取歌单详情
function play(id) {
  return new Promise(function (res, req) {
    uni.request({
      url: "".concat(_config.baseURL, "/song/detail?ids=").concat(id),
      method: "GET",
      data: {},
      success: function success(item) {
        res(item.data);
      } });

  });

}
// 播放歌曲
function playAuido(id) {
  return new Promise(function (res, req) {
    uni.request({
      url: "".concat(_config.baseURL, "/song/url?id=").concat(id),
      method: "GET",
      data: {},
      success: function success(item) {
        res(item.data);
      } });

  });

}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js */ 1)["default"]))

/***/ }),

/***/ 19:
/*!*************************************************!*\
  !*** C:/Users/王佳豪/Desktop/wyy/common/config.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.baseURL = void 0;var baseURL = "http://localhost:3000";exports.baseURL = baseURL;

/***/ }),

/***/ 2:
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),

/***/ 3:
/*!*************************************************************!*\
  !*** ./node_modules/@dcloudio/uni-i18n/dist/uni-i18n.es.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(uni, global) {Object.defineProperty(exports, "__esModule", { value: true });exports.compileI18nJsonStr = compileI18nJsonStr;exports.hasI18nJson = hasI18nJson;exports.initVueI18n = initVueI18n;exports.isI18nStr = isI18nStr;exports.normalizeLocale = normalizeLocale;exports.parseI18nJson = parseI18nJson;exports.resolveLocale = resolveLocale;exports.isString = exports.LOCALE_ZH_HANT = exports.LOCALE_ZH_HANS = exports.LOCALE_FR = exports.LOCALE_ES = exports.LOCALE_EN = exports.I18n = exports.Formatter = void 0;function _slicedToArray(arr, i) {return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest();}function _nonIterableRest() {throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");}function _unsupportedIterableToArray(o, minLen) {if (!o) return;if (typeof o === "string") return _arrayLikeToArray(o, minLen);var n = Object.prototype.toString.call(o).slice(8, -1);if (n === "Object" && o.constructor) n = o.constructor.name;if (n === "Map" || n === "Set") return Array.from(o);if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);}function _arrayLikeToArray(arr, len) {if (len == null || len > arr.length) len = arr.length;for (var i = 0, arr2 = new Array(len); i < len; i++) {arr2[i] = arr[i];}return arr2;}function _iterableToArrayLimit(arr, i) {if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return;var _arr = [];var _n = true;var _d = false;var _e = undefined;try {for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {_arr.push(_s.value);if (i && _arr.length === i) break;}} catch (err) {_d = true;_e = err;} finally {try {if (!_n && _i["return"] != null) _i["return"]();} finally {if (_d) throw _e;}}return _arr;}function _arrayWithHoles(arr) {if (Array.isArray(arr)) return arr;}function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}function _defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}function _createClass(Constructor, protoProps, staticProps) {if (protoProps) _defineProperties(Constructor.prototype, protoProps);if (staticProps) _defineProperties(Constructor, staticProps);return Constructor;}var isArray = Array.isArray;
var isObject = function isObject(val) {return val !== null && typeof val === 'object';};
var defaultDelimiters = ['{', '}'];var
BaseFormatter = /*#__PURE__*/function () {
  function BaseFormatter() {_classCallCheck(this, BaseFormatter);
    this._caches = Object.create(null);
  }_createClass(BaseFormatter, [{ key: "interpolate", value: function interpolate(
    message, values) {var delimiters = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : defaultDelimiters;
      if (!values) {
        return [message];
      }
      var tokens = this._caches[message];
      if (!tokens) {
        tokens = parse(message, delimiters);
        this._caches[message] = tokens;
      }
      return compile(tokens, values);
    } }]);return BaseFormatter;}();exports.Formatter = BaseFormatter;

var RE_TOKEN_LIST_VALUE = /^(?:\d)+/;
var RE_TOKEN_NAMED_VALUE = /^(?:\w)+/;
function parse(format, _ref) {var _ref2 = _slicedToArray(_ref, 2),startDelimiter = _ref2[0],endDelimiter = _ref2[1];
  var tokens = [];
  var position = 0;
  var text = '';
  while (position < format.length) {
    var char = format[position++];
    if (char === startDelimiter) {
      if (text) {
        tokens.push({ type: 'text', value: text });
      }
      text = '';
      var sub = '';
      char = format[position++];
      while (char !== undefined && char !== endDelimiter) {
        sub += char;
        char = format[position++];
      }
      var isClosed = char === endDelimiter;
      var type = RE_TOKEN_LIST_VALUE.test(sub) ?
      'list' :
      isClosed && RE_TOKEN_NAMED_VALUE.test(sub) ?
      'named' :
      'unknown';
      tokens.push({ value: sub, type: type });
    }
    //  else if (char === '%') {
    //   // when found rails i18n syntax, skip text capture
    //   if (format[position] !== '{') {
    //     text += char
    //   }
    // }
    else {
        text += char;
      }
  }
  text && tokens.push({ type: 'text', value: text });
  return tokens;
}
function compile(tokens, values) {
  var compiled = [];
  var index = 0;
  var mode = isArray(values) ?
  'list' :
  isObject(values) ?
  'named' :
  'unknown';
  if (mode === 'unknown') {
    return compiled;
  }
  while (index < tokens.length) {
    var token = tokens[index];
    switch (token.type) {
      case 'text':
        compiled.push(token.value);
        break;
      case 'list':
        compiled.push(values[parseInt(token.value, 10)]);
        break;
      case 'named':
        if (mode === 'named') {
          compiled.push(values[token.value]);
        } else
        {
          if (true) {
            console.warn("Type of token '".concat(token.type, "' and format of value '").concat(mode, "' don't match!"));
          }
        }
        break;
      case 'unknown':
        if (true) {
          console.warn("Detect 'unknown' type of token!");
        }
        break;}

    index++;
  }
  return compiled;
}

var LOCALE_ZH_HANS = 'zh-Hans';exports.LOCALE_ZH_HANS = LOCALE_ZH_HANS;
var LOCALE_ZH_HANT = 'zh-Hant';exports.LOCALE_ZH_HANT = LOCALE_ZH_HANT;
var LOCALE_EN = 'en';exports.LOCALE_EN = LOCALE_EN;
var LOCALE_FR = 'fr';exports.LOCALE_FR = LOCALE_FR;
var LOCALE_ES = 'es';exports.LOCALE_ES = LOCALE_ES;
var hasOwnProperty = Object.prototype.hasOwnProperty;
var hasOwn = function hasOwn(val, key) {return hasOwnProperty.call(val, key);};
var defaultFormatter = new BaseFormatter();
function include(str, parts) {
  return !!parts.find(function (part) {return str.indexOf(part) !== -1;});
}
function startsWith(str, parts) {
  return parts.find(function (part) {return str.indexOf(part) === 0;});
}
function normalizeLocale(locale, messages) {
  if (!locale) {
    return;
  }
  locale = locale.trim().replace(/_/g, '-');
  if (messages && messages[locale]) {
    return locale;
  }
  locale = locale.toLowerCase();
  if (locale.indexOf('zh') === 0) {
    if (locale.indexOf('-hans') > -1) {
      return LOCALE_ZH_HANS;
    }
    if (locale.indexOf('-hant') > -1) {
      return LOCALE_ZH_HANT;
    }
    if (include(locale, ['-tw', '-hk', '-mo', '-cht'])) {
      return LOCALE_ZH_HANT;
    }
    return LOCALE_ZH_HANS;
  }
  var lang = startsWith(locale, [LOCALE_EN, LOCALE_FR, LOCALE_ES]);
  if (lang) {
    return lang;
  }
}var
I18n = /*#__PURE__*/function () {
  function I18n(_ref3) {var locale = _ref3.locale,fallbackLocale = _ref3.fallbackLocale,messages = _ref3.messages,watcher = _ref3.watcher,formater = _ref3.formater;_classCallCheck(this, I18n);
    this.locale = LOCALE_EN;
    this.fallbackLocale = LOCALE_EN;
    this.message = {};
    this.messages = {};
    this.watchers = [];
    if (fallbackLocale) {
      this.fallbackLocale = fallbackLocale;
    }
    this.formater = formater || defaultFormatter;
    this.messages = messages || {};
    this.setLocale(locale || LOCALE_EN);
    if (watcher) {
      this.watchLocale(watcher);
    }
  }_createClass(I18n, [{ key: "setLocale", value: function setLocale(
    locale) {var _this = this;
      var oldLocale = this.locale;
      this.locale = normalizeLocale(locale, this.messages) || this.fallbackLocale;
      if (!this.messages[this.locale]) {
        // 可能初始化时不存在
        this.messages[this.locale] = {};
      }
      this.message = this.messages[this.locale];
      // 仅发生变化时，通知
      if (oldLocale !== this.locale) {
        this.watchers.forEach(function (watcher) {
          watcher(_this.locale, oldLocale);
        });
      }
    } }, { key: "getLocale", value: function getLocale()
    {
      return this.locale;
    } }, { key: "watchLocale", value: function watchLocale(
    fn) {var _this2 = this;
      var index = this.watchers.push(fn) - 1;
      return function () {
        _this2.watchers.splice(index, 1);
      };
    } }, { key: "add", value: function add(
    locale, message) {var override = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
      var curMessages = this.messages[locale];
      if (curMessages) {
        if (override) {
          Object.assign(curMessages, message);
        } else
        {
          Object.keys(message).forEach(function (key) {
            if (!hasOwn(curMessages, key)) {
              curMessages[key] = message[key];
            }
          });
        }
      } else
      {
        this.messages[locale] = message;
      }
    } }, { key: "f", value: function f(
    message, values, delimiters) {
      return this.formater.interpolate(message, values, delimiters).join('');
    } }, { key: "t", value: function t(
    key, locale, values) {
      var message = this.message;
      if (typeof locale === 'string') {
        locale = normalizeLocale(locale, this.messages);
        locale && (message = this.messages[locale]);
      } else
      {
        values = locale;
      }
      if (!hasOwn(message, key)) {
        console.warn("Cannot translate the value of keypath ".concat(key, ". Use the value of keypath as default."));
        return key;
      }
      return this.formater.interpolate(message[key], values).join('');
    } }]);return I18n;}();exports.I18n = I18n;


function watchAppLocale(appVm, i18n) {
  // 需要保证 watch 的触发在组件渲染之前
  if (appVm.$watchLocale) {
    // vue2
    appVm.$watchLocale(function (newLocale) {
      i18n.setLocale(newLocale);
    });
  } else
  {
    appVm.$watch(function () {return appVm.$locale;}, function (newLocale) {
      i18n.setLocale(newLocale);
    });
  }
}
function getDefaultLocale() {
  if (typeof uni !== 'undefined' && uni.getLocale) {
    return uni.getLocale();
  }
  // 小程序平台，uni 和 uni-i18n 互相引用，导致访问不到 uni，故在 global 上挂了 getLocale
  if (typeof global !== 'undefined' && global.getLocale) {
    return global.getLocale();
  }
  return LOCALE_EN;
}
function initVueI18n(locale) {var messages = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};var fallbackLocale = arguments.length > 2 ? arguments[2] : undefined;var watcher = arguments.length > 3 ? arguments[3] : undefined;
  // 兼容旧版本入参
  if (typeof locale !== 'string') {var _ref4 =
    [
    messages,
    locale];locale = _ref4[0];messages = _ref4[1];

  }
  if (typeof locale !== 'string') {
    // 因为小程序平台，uni-i18n 和 uni 互相引用，导致此时访问 uni 时，为 undefined
    locale = getDefaultLocale();
  }
  if (typeof fallbackLocale !== 'string') {
    fallbackLocale =
    typeof __uniConfig !== 'undefined' && __uniConfig.fallbackLocale ||
    LOCALE_EN;
  }
  var i18n = new I18n({
    locale: locale,
    fallbackLocale: fallbackLocale,
    messages: messages,
    watcher: watcher });

  var _t = function t(key, values) {
    if (typeof getApp !== 'function') {
      // app view
      /* eslint-disable no-func-assign */
      _t = function t(key, values) {
        return i18n.t(key, values);
      };
    } else
    {
      var isWatchedAppLocale = false;
      _t = function t(key, values) {
        var appVm = getApp().$vm;
        // 可能$vm还不存在，比如在支付宝小程序中，组件定义较早，在props的default里使用了t()函数（如uni-goods-nav），此时app还未初始化
        // options: {
        // 	type: Array,
        // 	default () {
        // 		return [{
        // 			icon: 'shop',
        // 			text: t("uni-goods-nav.options.shop"),
        // 		}, {
        // 			icon: 'cart',
        // 			text: t("uni-goods-nav.options.cart")
        // 		}]
        // 	}
        // },
        if (appVm) {
          // 触发响应式
          appVm.$locale;
          if (!isWatchedAppLocale) {
            isWatchedAppLocale = true;
            watchAppLocale(appVm, i18n);
          }
        }
        return i18n.t(key, values);
      };
    }
    return _t(key, values);
  };
  return {
    i18n: i18n,
    f: function f(message, values, delimiters) {
      return i18n.f(message, values, delimiters);
    },
    t: function t(key, values) {
      return _t(key, values);
    },
    add: function add(locale, message) {var override = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
      return i18n.add(locale, message, override);
    },
    watch: function watch(fn) {
      return i18n.watchLocale(fn);
    },
    getLocale: function getLocale() {
      return i18n.getLocale();
    },
    setLocale: function setLocale(newLocale) {
      return i18n.setLocale(newLocale);
    } };

}

var isString = function isString(val) {return typeof val === 'string';};exports.isString = isString;
var formater;
function hasI18nJson(jsonObj, delimiters) {
  if (!formater) {
    formater = new BaseFormatter();
  }
  return walkJsonObj(jsonObj, function (jsonObj, key) {
    var value = jsonObj[key];
    if (isString(value)) {
      if (isI18nStr(value, delimiters)) {
        return true;
      }
    } else
    {
      return hasI18nJson(value, delimiters);
    }
  });
}
function parseI18nJson(jsonObj, values, delimiters) {
  if (!formater) {
    formater = new BaseFormatter();
  }
  walkJsonObj(jsonObj, function (jsonObj, key) {
    var value = jsonObj[key];
    if (isString(value)) {
      if (isI18nStr(value, delimiters)) {
        jsonObj[key] = compileStr(value, values, delimiters);
      }
    } else
    {
      parseI18nJson(value, values, delimiters);
    }
  });
  return jsonObj;
}
function compileI18nJsonStr(jsonStr, _ref5) {var locale = _ref5.locale,locales = _ref5.locales,delimiters = _ref5.delimiters;
  if (!isI18nStr(jsonStr, delimiters)) {
    return jsonStr;
  }
  if (!formater) {
    formater = new BaseFormatter();
  }
  var localeValues = [];
  Object.keys(locales).forEach(function (name) {
    if (name !== locale) {
      localeValues.push({
        locale: name,
        values: locales[name] });

    }
  });
  localeValues.unshift({ locale: locale, values: locales[locale] });
  try {
    return JSON.stringify(compileJsonObj(JSON.parse(jsonStr), localeValues, delimiters), null, 2);
  }
  catch (e) {}
  return jsonStr;
}
function isI18nStr(value, delimiters) {
  return value.indexOf(delimiters[0]) > -1;
}
function compileStr(value, values, delimiters) {
  return formater.interpolate(value, values, delimiters).join('');
}
function compileValue(jsonObj, key, localeValues, delimiters) {
  var value = jsonObj[key];
  if (isString(value)) {
    // 存在国际化
    if (isI18nStr(value, delimiters)) {
      jsonObj[key] = compileStr(value, localeValues[0].values, delimiters);
      if (localeValues.length > 1) {
        // 格式化国际化语言
        var valueLocales = jsonObj[key + 'Locales'] = {};
        localeValues.forEach(function (localValue) {
          valueLocales[localValue.locale] = compileStr(value, localValue.values, delimiters);
        });
      }
    }
  } else
  {
    compileJsonObj(value, localeValues, delimiters);
  }
}
function compileJsonObj(jsonObj, localeValues, delimiters) {
  walkJsonObj(jsonObj, function (jsonObj, key) {
    compileValue(jsonObj, key, localeValues, delimiters);
  });
  return jsonObj;
}
function walkJsonObj(jsonObj, walk) {
  if (isArray(jsonObj)) {
    for (var i = 0; i < jsonObj.length; i++) {
      if (walk(jsonObj, i)) {
        return true;
      }
    }
  } else
  if (isObject(jsonObj)) {
    for (var key in jsonObj) {
      if (walk(jsonObj, key)) {
        return true;
      }
    }
  }
  return false;
}

function resolveLocale(locales) {
  return function (locale) {
    if (!locale) {
      return locale;
    }
    locale = normalizeLocale(locale) || locale;
    return resolveLocaleChain(locale).find(function (locale) {return locales.indexOf(locale) > -1;});
  };
}
function resolveLocaleChain(locale) {
  var chain = [];
  var tokens = locale.split('-');
  while (tokens.length) {
    chain.push(tokens.join('-'));
    tokens.pop();
  }
  return chain;
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js */ 1)["default"], __webpack_require__(/*! ./../../../webpack/buildin/global.js */ 2)))

/***/ }),

/***/ 4:
/*!******************************************************************************************!*\
  !*** ./node_modules/@dcloudio/vue-cli-plugin-uni/packages/mp-vue/dist/mp.runtime.esm.js ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {/*!
 * Vue.js v2.6.11
 * (c) 2014-2022 Evan You
 * Released under the MIT License.
 */
/*  */

var emptyObject = Object.freeze({});

// These helpers produce better VM code in JS engines due to their
// explicitness and function inlining.
function isUndef (v) {
  return v === undefined || v === null
}

function isDef (v) {
  return v !== undefined && v !== null
}

function isTrue (v) {
  return v === true
}

function isFalse (v) {
  return v === false
}

/**
 * Check if value is primitive.
 */
function isPrimitive (value) {
  return (
    typeof value === 'string' ||
    typeof value === 'number' ||
    // $flow-disable-line
    typeof value === 'symbol' ||
    typeof value === 'boolean'
  )
}

/**
 * Quick object check - this is primarily used to tell
 * Objects from primitive values when we know the value
 * is a JSON-compliant type.
 */
function isObject (obj) {
  return obj !== null && typeof obj === 'object'
}

/**
 * Get the raw type string of a value, e.g., [object Object].
 */
var _toString = Object.prototype.toString;

function toRawType (value) {
  return _toString.call(value).slice(8, -1)
}

/**
 * Strict object type check. Only returns true
 * for plain JavaScript objects.
 */
function isPlainObject (obj) {
  return _toString.call(obj) === '[object Object]'
}

function isRegExp (v) {
  return _toString.call(v) === '[object RegExp]'
}

/**
 * Check if val is a valid array index.
 */
function isValidArrayIndex (val) {
  var n = parseFloat(String(val));
  return n >= 0 && Math.floor(n) === n && isFinite(val)
}

function isPromise (val) {
  return (
    isDef(val) &&
    typeof val.then === 'function' &&
    typeof val.catch === 'function'
  )
}

/**
 * Convert a value to a string that is actually rendered.
 */
function toString (val) {
  return val == null
    ? ''
    : Array.isArray(val) || (isPlainObject(val) && val.toString === _toString)
      ? JSON.stringify(val, null, 2)
      : String(val)
}

/**
 * Convert an input value to a number for persistence.
 * If the conversion fails, return original string.
 */
function toNumber (val) {
  var n = parseFloat(val);
  return isNaN(n) ? val : n
}

/**
 * Make a map and return a function for checking if a key
 * is in that map.
 */
function makeMap (
  str,
  expectsLowerCase
) {
  var map = Object.create(null);
  var list = str.split(',');
  for (var i = 0; i < list.length; i++) {
    map[list[i]] = true;
  }
  return expectsLowerCase
    ? function (val) { return map[val.toLowerCase()]; }
    : function (val) { return map[val]; }
}

/**
 * Check if a tag is a built-in tag.
 */
var isBuiltInTag = makeMap('slot,component', true);

/**
 * Check if an attribute is a reserved attribute.
 */
var isReservedAttribute = makeMap('key,ref,slot,slot-scope,is');

/**
 * Remove an item from an array.
 */
function remove (arr, item) {
  if (arr.length) {
    var index = arr.indexOf(item);
    if (index > -1) {
      return arr.splice(index, 1)
    }
  }
}

/**
 * Check whether an object has the property.
 */
var hasOwnProperty = Object.prototype.hasOwnProperty;
function hasOwn (obj, key) {
  return hasOwnProperty.call(obj, key)
}

/**
 * Create a cached version of a pure function.
 */
function cached (fn) {
  var cache = Object.create(null);
  return (function cachedFn (str) {
    var hit = cache[str];
    return hit || (cache[str] = fn(str))
  })
}

/**
 * Camelize a hyphen-delimited string.
 */
var camelizeRE = /-(\w)/g;
var camelize = cached(function (str) {
  return str.replace(camelizeRE, function (_, c) { return c ? c.toUpperCase() : ''; })
});

/**
 * Capitalize a string.
 */
var capitalize = cached(function (str) {
  return str.charAt(0).toUpperCase() + str.slice(1)
});

/**
 * Hyphenate a camelCase string.
 */
var hyphenateRE = /\B([A-Z])/g;
var hyphenate = cached(function (str) {
  return str.replace(hyphenateRE, '-$1').toLowerCase()
});

/**
 * Simple bind polyfill for environments that do not support it,
 * e.g., PhantomJS 1.x. Technically, we don't need this anymore
 * since native bind is now performant enough in most browsers.
 * But removing it would mean breaking code that was able to run in
 * PhantomJS 1.x, so this must be kept for backward compatibility.
 */

/* istanbul ignore next */
function polyfillBind (fn, ctx) {
  function boundFn (a) {
    var l = arguments.length;
    return l
      ? l > 1
        ? fn.apply(ctx, arguments)
        : fn.call(ctx, a)
      : fn.call(ctx)
  }

  boundFn._length = fn.length;
  return boundFn
}

function nativeBind (fn, ctx) {
  return fn.bind(ctx)
}

var bind = Function.prototype.bind
  ? nativeBind
  : polyfillBind;

/**
 * Convert an Array-like object to a real Array.
 */
function toArray (list, start) {
  start = start || 0;
  var i = list.length - start;
  var ret = new Array(i);
  while (i--) {
    ret[i] = list[i + start];
  }
  return ret
}

/**
 * Mix properties into target object.
 */
function extend (to, _from) {
  for (var key in _from) {
    to[key] = _from[key];
  }
  return to
}

/**
 * Merge an Array of Objects into a single Object.
 */
function toObject (arr) {
  var res = {};
  for (var i = 0; i < arr.length; i++) {
    if (arr[i]) {
      extend(res, arr[i]);
    }
  }
  return res
}

/* eslint-disable no-unused-vars */

/**
 * Perform no operation.
 * Stubbing args to make Flow happy without leaving useless transpiled code
 * with ...rest (https://flow.org/blog/2017/05/07/Strict-Function-Call-Arity/).
 */
function noop (a, b, c) {}

/**
 * Always return false.
 */
var no = function (a, b, c) { return false; };

/* eslint-enable no-unused-vars */

/**
 * Return the same value.
 */
var identity = function (_) { return _; };

/**
 * Check if two values are loosely equal - that is,
 * if they are plain objects, do they have the same shape?
 */
function looseEqual (a, b) {
  if (a === b) { return true }
  var isObjectA = isObject(a);
  var isObjectB = isObject(b);
  if (isObjectA && isObjectB) {
    try {
      var isArrayA = Array.isArray(a);
      var isArrayB = Array.isArray(b);
      if (isArrayA && isArrayB) {
        return a.length === b.length && a.every(function (e, i) {
          return looseEqual(e, b[i])
        })
      } else if (a instanceof Date && b instanceof Date) {
        return a.getTime() === b.getTime()
      } else if (!isArrayA && !isArrayB) {
        var keysA = Object.keys(a);
        var keysB = Object.keys(b);
        return keysA.length === keysB.length && keysA.every(function (key) {
          return looseEqual(a[key], b[key])
        })
      } else {
        /* istanbul ignore next */
        return false
      }
    } catch (e) {
      /* istanbul ignore next */
      return false
    }
  } else if (!isObjectA && !isObjectB) {
    return String(a) === String(b)
  } else {
    return false
  }
}

/**
 * Return the first index at which a loosely equal value can be
 * found in the array (if value is a plain object, the array must
 * contain an object of the same shape), or -1 if it is not present.
 */
function looseIndexOf (arr, val) {
  for (var i = 0; i < arr.length; i++) {
    if (looseEqual(arr[i], val)) { return i }
  }
  return -1
}

/**
 * Ensure a function is called only once.
 */
function once (fn) {
  var called = false;
  return function () {
    if (!called) {
      called = true;
      fn.apply(this, arguments);
    }
  }
}

var ASSET_TYPES = [
  'component',
  'directive',
  'filter'
];

var LIFECYCLE_HOOKS = [
  'beforeCreate',
  'created',
  'beforeMount',
  'mounted',
  'beforeUpdate',
  'updated',
  'beforeDestroy',
  'destroyed',
  'activated',
  'deactivated',
  'errorCaptured',
  'serverPrefetch'
];

/*  */



var config = ({
  /**
   * Option merge strategies (used in core/util/options)
   */
  // $flow-disable-line
  optionMergeStrategies: Object.create(null),

  /**
   * Whether to suppress warnings.
   */
  silent: false,

  /**
   * Show production mode tip message on boot?
   */
  productionTip: "development" !== 'production',

  /**
   * Whether to enable devtools
   */
  devtools: "development" !== 'production',

  /**
   * Whether to record perf
   */
  performance: false,

  /**
   * Error handler for watcher errors
   */
  errorHandler: null,

  /**
   * Warn handler for watcher warns
   */
  warnHandler: null,

  /**
   * Ignore certain custom elements
   */
  ignoredElements: [],

  /**
   * Custom user key aliases for v-on
   */
  // $flow-disable-line
  keyCodes: Object.create(null),

  /**
   * Check if a tag is reserved so that it cannot be registered as a
   * component. This is platform-dependent and may be overwritten.
   */
  isReservedTag: no,

  /**
   * Check if an attribute is reserved so that it cannot be used as a component
   * prop. This is platform-dependent and may be overwritten.
   */
  isReservedAttr: no,

  /**
   * Check if a tag is an unknown element.
   * Platform-dependent.
   */
  isUnknownElement: no,

  /**
   * Get the namespace of an element
   */
  getTagNamespace: noop,

  /**
   * Parse the real tag name for the specific platform.
   */
  parsePlatformTagName: identity,

  /**
   * Check if an attribute must be bound using property, e.g. value
   * Platform-dependent.
   */
  mustUseProp: no,

  /**
   * Perform updates asynchronously. Intended to be used by Vue Test Utils
   * This will significantly reduce performance if set to false.
   */
  async: true,

  /**
   * Exposed for legacy reasons
   */
  _lifecycleHooks: LIFECYCLE_HOOKS
});

/*  */

/**
 * unicode letters used for parsing html tags, component names and property paths.
 * using https://www.w3.org/TR/html53/semantics-scripting.html#potentialcustomelementname
 * skipping \u10000-\uEFFFF due to it freezing up PhantomJS
 */
var unicodeRegExp = /a-zA-Z\u00B7\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u037D\u037F-\u1FFF\u200C-\u200D\u203F-\u2040\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD/;

/**
 * Check if a string starts with $ or _
 */
function isReserved (str) {
  var c = (str + '').charCodeAt(0);
  return c === 0x24 || c === 0x5F
}

/**
 * Define a property.
 */
function def (obj, key, val, enumerable) {
  Object.defineProperty(obj, key, {
    value: val,
    enumerable: !!enumerable,
    writable: true,
    configurable: true
  });
}

/**
 * Parse simple path.
 */
var bailRE = new RegExp(("[^" + (unicodeRegExp.source) + ".$_\\d]"));
function parsePath (path) {
  if (bailRE.test(path)) {
    return
  }
  var segments = path.split('.');
  return function (obj) {
    for (var i = 0; i < segments.length; i++) {
      if (!obj) { return }
      obj = obj[segments[i]];
    }
    return obj
  }
}

/*  */

// can we use __proto__?
var hasProto = '__proto__' in {};

// Browser environment sniffing
var inBrowser = typeof window !== 'undefined';
var inWeex = typeof WXEnvironment !== 'undefined' && !!WXEnvironment.platform;
var weexPlatform = inWeex && WXEnvironment.platform.toLowerCase();
var UA = inBrowser && window.navigator.userAgent.toLowerCase();
var isIE = UA && /msie|trident/.test(UA);
var isIE9 = UA && UA.indexOf('msie 9.0') > 0;
var isEdge = UA && UA.indexOf('edge/') > 0;
var isAndroid = (UA && UA.indexOf('android') > 0) || (weexPlatform === 'android');
var isIOS = (UA && /iphone|ipad|ipod|ios/.test(UA)) || (weexPlatform === 'ios');
var isChrome = UA && /chrome\/\d+/.test(UA) && !isEdge;
var isPhantomJS = UA && /phantomjs/.test(UA);
var isFF = UA && UA.match(/firefox\/(\d+)/);

// Firefox has a "watch" function on Object.prototype...
var nativeWatch = ({}).watch;
if (inBrowser) {
  try {
    var opts = {};
    Object.defineProperty(opts, 'passive', ({
      get: function get () {
      }
    })); // https://github.com/facebook/flow/issues/285
    window.addEventListener('test-passive', null, opts);
  } catch (e) {}
}

// this needs to be lazy-evaled because vue may be required before
// vue-server-renderer can set VUE_ENV
var _isServer;
var isServerRendering = function () {
  if (_isServer === undefined) {
    /* istanbul ignore if */
    if (!inBrowser && !inWeex && typeof global !== 'undefined') {
      // detect presence of vue-server-renderer and avoid
      // Webpack shimming the process
      _isServer = global['process'] && global['process'].env.VUE_ENV === 'server';
    } else {
      _isServer = false;
    }
  }
  return _isServer
};

// detect devtools
var devtools = inBrowser && window.__VUE_DEVTOOLS_GLOBAL_HOOK__;

/* istanbul ignore next */
function isNative (Ctor) {
  return typeof Ctor === 'function' && /native code/.test(Ctor.toString())
}

var hasSymbol =
  typeof Symbol !== 'undefined' && isNative(Symbol) &&
  typeof Reflect !== 'undefined' && isNative(Reflect.ownKeys);

var _Set;
/* istanbul ignore if */ // $flow-disable-line
if (typeof Set !== 'undefined' && isNative(Set)) {
  // use native Set when available.
  _Set = Set;
} else {
  // a non-standard Set polyfill that only works with primitive keys.
  _Set = /*@__PURE__*/(function () {
    function Set () {
      this.set = Object.create(null);
    }
    Set.prototype.has = function has (key) {
      return this.set[key] === true
    };
    Set.prototype.add = function add (key) {
      this.set[key] = true;
    };
    Set.prototype.clear = function clear () {
      this.set = Object.create(null);
    };

    return Set;
  }());
}

/*  */

var warn = noop;
var tip = noop;
var generateComponentTrace = (noop); // work around flow check
var formatComponentName = (noop);

if (true) {
  var hasConsole = typeof console !== 'undefined';
  var classifyRE = /(?:^|[-_])(\w)/g;
  var classify = function (str) { return str
    .replace(classifyRE, function (c) { return c.toUpperCase(); })
    .replace(/[-_]/g, ''); };

  warn = function (msg, vm) {
    var trace = vm ? generateComponentTrace(vm) : '';

    if (config.warnHandler) {
      config.warnHandler.call(null, msg, vm, trace);
    } else if (hasConsole && (!config.silent)) {
      console.error(("[Vue warn]: " + msg + trace));
    }
  };

  tip = function (msg, vm) {
    if (hasConsole && (!config.silent)) {
      console.warn("[Vue tip]: " + msg + (
        vm ? generateComponentTrace(vm) : ''
      ));
    }
  };

  formatComponentName = function (vm, includeFile) {
    if (vm.$root === vm) {
      if (vm.$options && vm.$options.__file) { // fixed by xxxxxx
        return ('') + vm.$options.__file
      }
      return '<Root>'
    }
    var options = typeof vm === 'function' && vm.cid != null
      ? vm.options
      : vm._isVue
        ? vm.$options || vm.constructor.options
        : vm;
    var name = options.name || options._componentTag;
    var file = options.__file;
    if (!name && file) {
      var match = file.match(/([^/\\]+)\.vue$/);
      name = match && match[1];
    }

    return (
      (name ? ("<" + (classify(name)) + ">") : "<Anonymous>") +
      (file && includeFile !== false ? (" at " + file) : '')
    )
  };

  var repeat = function (str, n) {
    var res = '';
    while (n) {
      if (n % 2 === 1) { res += str; }
      if (n > 1) { str += str; }
      n >>= 1;
    }
    return res
  };

  generateComponentTrace = function (vm) {
    if (vm._isVue && vm.$parent) {
      var tree = [];
      var currentRecursiveSequence = 0;
      while (vm && vm.$options.name !== 'PageBody') {
        if (tree.length > 0) {
          var last = tree[tree.length - 1];
          if (last.constructor === vm.constructor) {
            currentRecursiveSequence++;
            vm = vm.$parent;
            continue
          } else if (currentRecursiveSequence > 0) {
            tree[tree.length - 1] = [last, currentRecursiveSequence];
            currentRecursiveSequence = 0;
          }
        }
        !vm.$options.isReserved && tree.push(vm);
        vm = vm.$parent;
      }
      return '\n\nfound in\n\n' + tree
        .map(function (vm, i) { return ("" + (i === 0 ? '---> ' : repeat(' ', 5 + i * 2)) + (Array.isArray(vm)
            ? ((formatComponentName(vm[0])) + "... (" + (vm[1]) + " recursive calls)")
            : formatComponentName(vm))); })
        .join('\n')
    } else {
      return ("\n\n(found in " + (formatComponentName(vm)) + ")")
    }
  };
}

/*  */

var uid = 0;

/**
 * A dep is an observable that can have multiple
 * directives subscribing to it.
 */
var Dep = function Dep () {
  this.id = uid++;
  this.subs = [];
};

Dep.prototype.addSub = function addSub (sub) {
  this.subs.push(sub);
};

Dep.prototype.removeSub = function removeSub (sub) {
  remove(this.subs, sub);
};

Dep.prototype.depend = function depend () {
  if (Dep.SharedObject.target) {
    Dep.SharedObject.target.addDep(this);
  }
};

Dep.prototype.notify = function notify () {
  // stabilize the subscriber list first
  var subs = this.subs.slice();
  if ( true && !config.async) {
    // subs aren't sorted in scheduler if not running async
    // we need to sort them now to make sure they fire in correct
    // order
    subs.sort(function (a, b) { return a.id - b.id; });
  }
  for (var i = 0, l = subs.length; i < l; i++) {
    subs[i].update();
  }
};

// The current target watcher being evaluated.
// This is globally unique because only one watcher
// can be evaluated at a time.
// fixed by xxxxxx (nvue shared vuex)
/* eslint-disable no-undef */
Dep.SharedObject = {};
Dep.SharedObject.target = null;
Dep.SharedObject.targetStack = [];

function pushTarget (target) {
  Dep.SharedObject.targetStack.push(target);
  Dep.SharedObject.target = target;
  Dep.target = target;
}

function popTarget () {
  Dep.SharedObject.targetStack.pop();
  Dep.SharedObject.target = Dep.SharedObject.targetStack[Dep.SharedObject.targetStack.length - 1];
  Dep.target = Dep.SharedObject.target;
}

/*  */

var VNode = function VNode (
  tag,
  data,
  children,
  text,
  elm,
  context,
  componentOptions,
  asyncFactory
) {
  this.tag = tag;
  this.data = data;
  this.children = children;
  this.text = text;
  this.elm = elm;
  this.ns = undefined;
  this.context = context;
  this.fnContext = undefined;
  this.fnOptions = undefined;
  this.fnScopeId = undefined;
  this.key = data && data.key;
  this.componentOptions = componentOptions;
  this.componentInstance = undefined;
  this.parent = undefined;
  this.raw = false;
  this.isStatic = false;
  this.isRootInsert = true;
  this.isComment = false;
  this.isCloned = false;
  this.isOnce = false;
  this.asyncFactory = asyncFactory;
  this.asyncMeta = undefined;
  this.isAsyncPlaceholder = false;
};

var prototypeAccessors = { child: { configurable: true } };

// DEPRECATED: alias for componentInstance for backwards compat.
/* istanbul ignore next */
prototypeAccessors.child.get = function () {
  return this.componentInstance
};

Object.defineProperties( VNode.prototype, prototypeAccessors );

var createEmptyVNode = function (text) {
  if ( text === void 0 ) text = '';

  var node = new VNode();
  node.text = text;
  node.isComment = true;
  return node
};

function createTextVNode (val) {
  return new VNode(undefined, undefined, undefined, String(val))
}

// optimized shallow clone
// used for static nodes and slot nodes because they may be reused across
// multiple renders, cloning them avoids errors when DOM manipulations rely
// on their elm reference.
function cloneVNode (vnode) {
  var cloned = new VNode(
    vnode.tag,
    vnode.data,
    // #7975
    // clone children array to avoid mutating original in case of cloning
    // a child.
    vnode.children && vnode.children.slice(),
    vnode.text,
    vnode.elm,
    vnode.context,
    vnode.componentOptions,
    vnode.asyncFactory
  );
  cloned.ns = vnode.ns;
  cloned.isStatic = vnode.isStatic;
  cloned.key = vnode.key;
  cloned.isComment = vnode.isComment;
  cloned.fnContext = vnode.fnContext;
  cloned.fnOptions = vnode.fnOptions;
  cloned.fnScopeId = vnode.fnScopeId;
  cloned.asyncMeta = vnode.asyncMeta;
  cloned.isCloned = true;
  return cloned
}

/*
 * not type checking this file because flow doesn't play well with
 * dynamically accessing methods on Array prototype
 */

var arrayProto = Array.prototype;
var arrayMethods = Object.create(arrayProto);

var methodsToPatch = [
  'push',
  'pop',
  'shift',
  'unshift',
  'splice',
  'sort',
  'reverse'
];

/**
 * Intercept mutating methods and emit events
 */
methodsToPatch.forEach(function (method) {
  // cache original method
  var original = arrayProto[method];
  def(arrayMethods, method, function mutator () {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    var result = original.apply(this, args);
    var ob = this.__ob__;
    var inserted;
    switch (method) {
      case 'push':
      case 'unshift':
        inserted = args;
        break
      case 'splice':
        inserted = args.slice(2);
        break
    }
    if (inserted) { ob.observeArray(inserted); }
    // notify change
    ob.dep.notify();
    return result
  });
});

/*  */

var arrayKeys = Object.getOwnPropertyNames(arrayMethods);

/**
 * In some cases we may want to disable observation inside a component's
 * update computation.
 */
var shouldObserve = true;

function toggleObserving (value) {
  shouldObserve = value;
}

/**
 * Observer class that is attached to each observed
 * object. Once attached, the observer converts the target
 * object's property keys into getter/setters that
 * collect dependencies and dispatch updates.
 */
var Observer = function Observer (value) {
  this.value = value;
  this.dep = new Dep();
  this.vmCount = 0;
  def(value, '__ob__', this);
  if (Array.isArray(value)) {
    if (hasProto) {
      {// fixed by xxxxxx 微信小程序使用 plugins 之后，数组方法被直接挂载到了数组对象上，需要执行 copyAugment 逻辑
        if(value.push !== value.__proto__.push){
          copyAugment(value, arrayMethods, arrayKeys);
        } else {
          protoAugment(value, arrayMethods);
        }
      }
    } else {
      copyAugment(value, arrayMethods, arrayKeys);
    }
    this.observeArray(value);
  } else {
    this.walk(value);
  }
};

/**
 * Walk through all properties and convert them into
 * getter/setters. This method should only be called when
 * value type is Object.
 */
Observer.prototype.walk = function walk (obj) {
  var keys = Object.keys(obj);
  for (var i = 0; i < keys.length; i++) {
    defineReactive$$1(obj, keys[i]);
  }
};

/**
 * Observe a list of Array items.
 */
Observer.prototype.observeArray = function observeArray (items) {
  for (var i = 0, l = items.length; i < l; i++) {
    observe(items[i]);
  }
};

// helpers

/**
 * Augment a target Object or Array by intercepting
 * the prototype chain using __proto__
 */
function protoAugment (target, src) {
  /* eslint-disable no-proto */
  target.__proto__ = src;
  /* eslint-enable no-proto */
}

/**
 * Augment a target Object or Array by defining
 * hidden properties.
 */
/* istanbul ignore next */
function copyAugment (target, src, keys) {
  for (var i = 0, l = keys.length; i < l; i++) {
    var key = keys[i];
    def(target, key, src[key]);
  }
}

/**
 * Attempt to create an observer instance for a value,
 * returns the new observer if successfully observed,
 * or the existing observer if the value already has one.
 */
function observe (value, asRootData) {
  if (!isObject(value) || value instanceof VNode) {
    return
  }
  var ob;
  if (hasOwn(value, '__ob__') && value.__ob__ instanceof Observer) {
    ob = value.__ob__;
  } else if (
    shouldObserve &&
    !isServerRendering() &&
    (Array.isArray(value) || isPlainObject(value)) &&
    Object.isExtensible(value) &&
    !value._isVue
  ) {
    ob = new Observer(value);
  }
  if (asRootData && ob) {
    ob.vmCount++;
  }
  return ob
}

/**
 * Define a reactive property on an Object.
 */
function defineReactive$$1 (
  obj,
  key,
  val,
  customSetter,
  shallow
) {
  var dep = new Dep();

  var property = Object.getOwnPropertyDescriptor(obj, key);
  if (property && property.configurable === false) {
    return
  }

  // cater for pre-defined getter/setters
  var getter = property && property.get;
  var setter = property && property.set;
  if ((!getter || setter) && arguments.length === 2) {
    val = obj[key];
  }

  var childOb = !shallow && observe(val);
  Object.defineProperty(obj, key, {
    enumerable: true,
    configurable: true,
    get: function reactiveGetter () {
      var value = getter ? getter.call(obj) : val;
      if (Dep.SharedObject.target) { // fixed by xxxxxx
        dep.depend();
        if (childOb) {
          childOb.dep.depend();
          if (Array.isArray(value)) {
            dependArray(value);
          }
        }
      }
      return value
    },
    set: function reactiveSetter (newVal) {
      var value = getter ? getter.call(obj) : val;
      /* eslint-disable no-self-compare */
      if (newVal === value || (newVal !== newVal && value !== value)) {
        return
      }
      /* eslint-enable no-self-compare */
      if ( true && customSetter) {
        customSetter();
      }
      // #7981: for accessor properties without setter
      if (getter && !setter) { return }
      if (setter) {
        setter.call(obj, newVal);
      } else {
        val = newVal;
      }
      childOb = !shallow && observe(newVal);
      dep.notify();
    }
  });
}

/**
 * Set a property on an object. Adds the new property and
 * triggers change notification if the property doesn't
 * already exist.
 */
function set (target, key, val) {
  if ( true &&
    (isUndef(target) || isPrimitive(target))
  ) {
    warn(("Cannot set reactive property on undefined, null, or primitive value: " + ((target))));
  }
  if (Array.isArray(target) && isValidArrayIndex(key)) {
    target.length = Math.max(target.length, key);
    target.splice(key, 1, val);
    return val
  }
  if (key in target && !(key in Object.prototype)) {
    target[key] = val;
    return val
  }
  var ob = (target).__ob__;
  if (target._isVue || (ob && ob.vmCount)) {
     true && warn(
      'Avoid adding reactive properties to a Vue instance or its root $data ' +
      'at runtime - declare it upfront in the data option.'
    );
    return val
  }
  if (!ob) {
    target[key] = val;
    return val
  }
  defineReactive$$1(ob.value, key, val);
  ob.dep.notify();
  return val
}

/**
 * Delete a property and trigger change if necessary.
 */
function del (target, key) {
  if ( true &&
    (isUndef(target) || isPrimitive(target))
  ) {
    warn(("Cannot delete reactive property on undefined, null, or primitive value: " + ((target))));
  }
  if (Array.isArray(target) && isValidArrayIndex(key)) {
    target.splice(key, 1);
    return
  }
  var ob = (target).__ob__;
  if (target._isVue || (ob && ob.vmCount)) {
     true && warn(
      'Avoid deleting properties on a Vue instance or its root $data ' +
      '- just set it to null.'
    );
    return
  }
  if (!hasOwn(target, key)) {
    return
  }
  delete target[key];
  if (!ob) {
    return
  }
  ob.dep.notify();
}

/**
 * Collect dependencies on array elements when the array is touched, since
 * we cannot intercept array element access like property getters.
 */
function dependArray (value) {
  for (var e = (void 0), i = 0, l = value.length; i < l; i++) {
    e = value[i];
    e && e.__ob__ && e.__ob__.dep.depend();
    if (Array.isArray(e)) {
      dependArray(e);
    }
  }
}

/*  */

/**
 * Option overwriting strategies are functions that handle
 * how to merge a parent option value and a child option
 * value into the final value.
 */
var strats = config.optionMergeStrategies;

/**
 * Options with restrictions
 */
if (true) {
  strats.el = strats.propsData = function (parent, child, vm, key) {
    if (!vm) {
      warn(
        "option \"" + key + "\" can only be used during instance " +
        'creation with the `new` keyword.'
      );
    }
    return defaultStrat(parent, child)
  };
}

/**
 * Helper that recursively merges two data objects together.
 */
function mergeData (to, from) {
  if (!from) { return to }
  var key, toVal, fromVal;

  var keys = hasSymbol
    ? Reflect.ownKeys(from)
    : Object.keys(from);

  for (var i = 0; i < keys.length; i++) {
    key = keys[i];
    // in case the object is already observed...
    if (key === '__ob__') { continue }
    toVal = to[key];
    fromVal = from[key];
    if (!hasOwn(to, key)) {
      set(to, key, fromVal);
    } else if (
      toVal !== fromVal &&
      isPlainObject(toVal) &&
      isPlainObject(fromVal)
    ) {
      mergeData(toVal, fromVal);
    }
  }
  return to
}

/**
 * Data
 */
function mergeDataOrFn (
  parentVal,
  childVal,
  vm
) {
  if (!vm) {
    // in a Vue.extend merge, both should be functions
    if (!childVal) {
      return parentVal
    }
    if (!parentVal) {
      return childVal
    }
    // when parentVal & childVal are both present,
    // we need to return a function that returns the
    // merged result of both functions... no need to
    // check if parentVal is a function here because
    // it has to be a function to pass previous merges.
    return function mergedDataFn () {
      return mergeData(
        typeof childVal === 'function' ? childVal.call(this, this) : childVal,
        typeof parentVal === 'function' ? parentVal.call(this, this) : parentVal
      )
    }
  } else {
    return function mergedInstanceDataFn () {
      // instance merge
      var instanceData = typeof childVal === 'function'
        ? childVal.call(vm, vm)
        : childVal;
      var defaultData = typeof parentVal === 'function'
        ? parentVal.call(vm, vm)
        : parentVal;
      if (instanceData) {
        return mergeData(instanceData, defaultData)
      } else {
        return defaultData
      }
    }
  }
}

strats.data = function (
  parentVal,
  childVal,
  vm
) {
  if (!vm) {
    if (childVal && typeof childVal !== 'function') {
       true && warn(
        'The "data" option should be a function ' +
        'that returns a per-instance value in component ' +
        'definitions.',
        vm
      );

      return parentVal
    }
    return mergeDataOrFn(parentVal, childVal)
  }

  return mergeDataOrFn(parentVal, childVal, vm)
};

/**
 * Hooks and props are merged as arrays.
 */
function mergeHook (
  parentVal,
  childVal
) {
  var res = childVal
    ? parentVal
      ? parentVal.concat(childVal)
      : Array.isArray(childVal)
        ? childVal
        : [childVal]
    : parentVal;
  return res
    ? dedupeHooks(res)
    : res
}

function dedupeHooks (hooks) {
  var res = [];
  for (var i = 0; i < hooks.length; i++) {
    if (res.indexOf(hooks[i]) === -1) {
      res.push(hooks[i]);
    }
  }
  return res
}

LIFECYCLE_HOOKS.forEach(function (hook) {
  strats[hook] = mergeHook;
});

/**
 * Assets
 *
 * When a vm is present (instance creation), we need to do
 * a three-way merge between constructor options, instance
 * options and parent options.
 */
function mergeAssets (
  parentVal,
  childVal,
  vm,
  key
) {
  var res = Object.create(parentVal || null);
  if (childVal) {
     true && assertObjectType(key, childVal, vm);
    return extend(res, childVal)
  } else {
    return res
  }
}

ASSET_TYPES.forEach(function (type) {
  strats[type + 's'] = mergeAssets;
});

/**
 * Watchers.
 *
 * Watchers hashes should not overwrite one
 * another, so we merge them as arrays.
 */
strats.watch = function (
  parentVal,
  childVal,
  vm,
  key
) {
  // work around Firefox's Object.prototype.watch...
  if (parentVal === nativeWatch) { parentVal = undefined; }
  if (childVal === nativeWatch) { childVal = undefined; }
  /* istanbul ignore if */
  if (!childVal) { return Object.create(parentVal || null) }
  if (true) {
    assertObjectType(key, childVal, vm);
  }
  if (!parentVal) { return childVal }
  var ret = {};
  extend(ret, parentVal);
  for (var key$1 in childVal) {
    var parent = ret[key$1];
    var child = childVal[key$1];
    if (parent && !Array.isArray(parent)) {
      parent = [parent];
    }
    ret[key$1] = parent
      ? parent.concat(child)
      : Array.isArray(child) ? child : [child];
  }
  return ret
};

/**
 * Other object hashes.
 */
strats.props =
strats.methods =
strats.inject =
strats.computed = function (
  parentVal,
  childVal,
  vm,
  key
) {
  if (childVal && "development" !== 'production') {
    assertObjectType(key, childVal, vm);
  }
  if (!parentVal) { return childVal }
  var ret = Object.create(null);
  extend(ret, parentVal);
  if (childVal) { extend(ret, childVal); }
  return ret
};
strats.provide = mergeDataOrFn;

/**
 * Default strategy.
 */
var defaultStrat = function (parentVal, childVal) {
  return childVal === undefined
    ? parentVal
    : childVal
};

/**
 * Validate component names
 */
function checkComponents (options) {
  for (var key in options.components) {
    validateComponentName(key);
  }
}

function validateComponentName (name) {
  if (!new RegExp(("^[a-zA-Z][\\-\\.0-9_" + (unicodeRegExp.source) + "]*$")).test(name)) {
    warn(
      'Invalid component name: "' + name + '". Component names ' +
      'should conform to valid custom element name in html5 specification.'
    );
  }
  if (isBuiltInTag(name) || config.isReservedTag(name)) {
    warn(
      'Do not use built-in or reserved HTML elements as component ' +
      'id: ' + name
    );
  }
}

/**
 * Ensure all props option syntax are normalized into the
 * Object-based format.
 */
function normalizeProps (options, vm) {
  var props = options.props;
  if (!props) { return }
  var res = {};
  var i, val, name;
  if (Array.isArray(props)) {
    i = props.length;
    while (i--) {
      val = props[i];
      if (typeof val === 'string') {
        name = camelize(val);
        res[name] = { type: null };
      } else if (true) {
        warn('props must be strings when using array syntax.');
      }
    }
  } else if (isPlainObject(props)) {
    for (var key in props) {
      val = props[key];
      name = camelize(key);
      res[name] = isPlainObject(val)
        ? val
        : { type: val };
    }
  } else if (true) {
    warn(
      "Invalid value for option \"props\": expected an Array or an Object, " +
      "but got " + (toRawType(props)) + ".",
      vm
    );
  }
  options.props = res;
}

/**
 * Normalize all injections into Object-based format
 */
function normalizeInject (options, vm) {
  var inject = options.inject;
  if (!inject) { return }
  var normalized = options.inject = {};
  if (Array.isArray(inject)) {
    for (var i = 0; i < inject.length; i++) {
      normalized[inject[i]] = { from: inject[i] };
    }
  } else if (isPlainObject(inject)) {
    for (var key in inject) {
      var val = inject[key];
      normalized[key] = isPlainObject(val)
        ? extend({ from: key }, val)
        : { from: val };
    }
  } else if (true) {
    warn(
      "Invalid value for option \"inject\": expected an Array or an Object, " +
      "but got " + (toRawType(inject)) + ".",
      vm
    );
  }
}

/**
 * Normalize raw function directives into object format.
 */
function normalizeDirectives (options) {
  var dirs = options.directives;
  if (dirs) {
    for (var key in dirs) {
      var def$$1 = dirs[key];
      if (typeof def$$1 === 'function') {
        dirs[key] = { bind: def$$1, update: def$$1 };
      }
    }
  }
}

function assertObjectType (name, value, vm) {
  if (!isPlainObject(value)) {
    warn(
      "Invalid value for option \"" + name + "\": expected an Object, " +
      "but got " + (toRawType(value)) + ".",
      vm
    );
  }
}

/**
 * Merge two option objects into a new one.
 * Core utility used in both instantiation and inheritance.
 */
function mergeOptions (
  parent,
  child,
  vm
) {
  if (true) {
    checkComponents(child);
  }

  if (typeof child === 'function') {
    child = child.options;
  }

  normalizeProps(child, vm);
  normalizeInject(child, vm);
  normalizeDirectives(child);

  // Apply extends and mixins on the child options,
  // but only if it is a raw options object that isn't
  // the result of another mergeOptions call.
  // Only merged options has the _base property.
  if (!child._base) {
    if (child.extends) {
      parent = mergeOptions(parent, child.extends, vm);
    }
    if (child.mixins) {
      for (var i = 0, l = child.mixins.length; i < l; i++) {
        parent = mergeOptions(parent, child.mixins[i], vm);
      }
    }
  }

  var options = {};
  var key;
  for (key in parent) {
    mergeField(key);
  }
  for (key in child) {
    if (!hasOwn(parent, key)) {
      mergeField(key);
    }
  }
  function mergeField (key) {
    var strat = strats[key] || defaultStrat;
    options[key] = strat(parent[key], child[key], vm, key);
  }
  return options
}

/**
 * Resolve an asset.
 * This function is used because child instances need access
 * to assets defined in its ancestor chain.
 */
function resolveAsset (
  options,
  type,
  id,
  warnMissing
) {
  /* istanbul ignore if */
  if (typeof id !== 'string') {
    return
  }
  var assets = options[type];
  // check local registration variations first
  if (hasOwn(assets, id)) { return assets[id] }
  var camelizedId = camelize(id);
  if (hasOwn(assets, camelizedId)) { return assets[camelizedId] }
  var PascalCaseId = capitalize(camelizedId);
  if (hasOwn(assets, PascalCaseId)) { return assets[PascalCaseId] }
  // fallback to prototype chain
  var res = assets[id] || assets[camelizedId] || assets[PascalCaseId];
  if ( true && warnMissing && !res) {
    warn(
      'Failed to resolve ' + type.slice(0, -1) + ': ' + id,
      options
    );
  }
  return res
}

/*  */



function validateProp (
  key,
  propOptions,
  propsData,
  vm
) {
  var prop = propOptions[key];
  var absent = !hasOwn(propsData, key);
  var value = propsData[key];
  // boolean casting
  var booleanIndex = getTypeIndex(Boolean, prop.type);
  if (booleanIndex > -1) {
    if (absent && !hasOwn(prop, 'default')) {
      value = false;
    } else if (value === '' || value === hyphenate(key)) {
      // only cast empty string / same name to boolean if
      // boolean has higher priority
      var stringIndex = getTypeIndex(String, prop.type);
      if (stringIndex < 0 || booleanIndex < stringIndex) {
        value = true;
      }
    }
  }
  // check default value
  if (value === undefined) {
    value = getPropDefaultValue(vm, prop, key);
    // since the default value is a fresh copy,
    // make sure to observe it.
    var prevShouldObserve = shouldObserve;
    toggleObserving(true);
    observe(value);
    toggleObserving(prevShouldObserve);
  }
  if (
    true
  ) {
    assertProp(prop, key, value, vm, absent);
  }
  return value
}

/**
 * Get the default value of a prop.
 */
function getPropDefaultValue (vm, prop, key) {
  // no default, return undefined
  if (!hasOwn(prop, 'default')) {
    return undefined
  }
  var def = prop.default;
  // warn against non-factory defaults for Object & Array
  if ( true && isObject(def)) {
    warn(
      'Invalid default value for prop "' + key + '": ' +
      'Props with type Object/Array must use a factory function ' +
      'to return the default value.',
      vm
    );
  }
  // the raw prop value was also undefined from previous render,
  // return previous default value to avoid unnecessary watcher trigger
  if (vm && vm.$options.propsData &&
    vm.$options.propsData[key] === undefined &&
    vm._props[key] !== undefined
  ) {
    return vm._props[key]
  }
  // call factory function for non-Function types
  // a value is Function if its prototype is function even across different execution context
  return typeof def === 'function' && getType(prop.type) !== 'Function'
    ? def.call(vm)
    : def
}

/**
 * Assert whether a prop is valid.
 */
function assertProp (
  prop,
  name,
  value,
  vm,
  absent
) {
  if (prop.required && absent) {
    warn(
      'Missing required prop: "' + name + '"',
      vm
    );
    return
  }
  if (value == null && !prop.required) {
    return
  }
  var type = prop.type;
  var valid = !type || type === true;
  var expectedTypes = [];
  if (type) {
    if (!Array.isArray(type)) {
      type = [type];
    }
    for (var i = 0; i < type.length && !valid; i++) {
      var assertedType = assertType(value, type[i]);
      expectedTypes.push(assertedType.expectedType || '');
      valid = assertedType.valid;
    }
  }

  if (!valid) {
    warn(
      getInvalidTypeMessage(name, value, expectedTypes),
      vm
    );
    return
  }
  var validator = prop.validator;
  if (validator) {
    if (!validator(value)) {
      warn(
        'Invalid prop: custom validator check failed for prop "' + name + '".',
        vm
      );
    }
  }
}

var simpleCheckRE = /^(String|Number|Boolean|Function|Symbol)$/;

function assertType (value, type) {
  var valid;
  var expectedType = getType(type);
  if (simpleCheckRE.test(expectedType)) {
    var t = typeof value;
    valid = t === expectedType.toLowerCase();
    // for primitive wrapper objects
    if (!valid && t === 'object') {
      valid = value instanceof type;
    }
  } else if (expectedType === 'Object') {
    valid = isPlainObject(value);
  } else if (expectedType === 'Array') {
    valid = Array.isArray(value);
  } else {
    valid = value instanceof type;
  }
  return {
    valid: valid,
    expectedType: expectedType
  }
}

/**
 * Use function string name to check built-in types,
 * because a simple equality check will fail when running
 * across different vms / iframes.
 */
function getType (fn) {
  var match = fn && fn.toString().match(/^\s*function (\w+)/);
  return match ? match[1] : ''
}

function isSameType (a, b) {
  return getType(a) === getType(b)
}

function getTypeIndex (type, expectedTypes) {
  if (!Array.isArray(expectedTypes)) {
    return isSameType(expectedTypes, type) ? 0 : -1
  }
  for (var i = 0, len = expectedTypes.length; i < len; i++) {
    if (isSameType(expectedTypes[i], type)) {
      return i
    }
  }
  return -1
}

function getInvalidTypeMessage (name, value, expectedTypes) {
  var message = "Invalid prop: type check failed for prop \"" + name + "\"." +
    " Expected " + (expectedTypes.map(capitalize).join(', '));
  var expectedType = expectedTypes[0];
  var receivedType = toRawType(value);
  var expectedValue = styleValue(value, expectedType);
  var receivedValue = styleValue(value, receivedType);
  // check if we need to specify expected value
  if (expectedTypes.length === 1 &&
      isExplicable(expectedType) &&
      !isBoolean(expectedType, receivedType)) {
    message += " with value " + expectedValue;
  }
  message += ", got " + receivedType + " ";
  // check if we need to specify received value
  if (isExplicable(receivedType)) {
    message += "with value " + receivedValue + ".";
  }
  return message
}

function styleValue (value, type) {
  if (type === 'String') {
    return ("\"" + value + "\"")
  } else if (type === 'Number') {
    return ("" + (Number(value)))
  } else {
    return ("" + value)
  }
}

function isExplicable (value) {
  var explicitTypes = ['string', 'number', 'boolean'];
  return explicitTypes.some(function (elem) { return value.toLowerCase() === elem; })
}

function isBoolean () {
  var args = [], len = arguments.length;
  while ( len-- ) args[ len ] = arguments[ len ];

  return args.some(function (elem) { return elem.toLowerCase() === 'boolean'; })
}

/*  */

function handleError (err, vm, info) {
  // Deactivate deps tracking while processing error handler to avoid possible infinite rendering.
  // See: https://github.com/vuejs/vuex/issues/1505
  pushTarget();
  try {
    if (vm) {
      var cur = vm;
      while ((cur = cur.$parent)) {
        var hooks = cur.$options.errorCaptured;
        if (hooks) {
          for (var i = 0; i < hooks.length; i++) {
            try {
              var capture = hooks[i].call(cur, err, vm, info) === false;
              if (capture) { return }
            } catch (e) {
              globalHandleError(e, cur, 'errorCaptured hook');
            }
          }
        }
      }
    }
    globalHandleError(err, vm, info);
  } finally {
    popTarget();
  }
}

function invokeWithErrorHandling (
  handler,
  context,
  args,
  vm,
  info
) {
  var res;
  try {
    res = args ? handler.apply(context, args) : handler.call(context);
    if (res && !res._isVue && isPromise(res) && !res._handled) {
      res.catch(function (e) { return handleError(e, vm, info + " (Promise/async)"); });
      // issue #9511
      // avoid catch triggering multiple times when nested calls
      res._handled = true;
    }
  } catch (e) {
    handleError(e, vm, info);
  }
  return res
}

function globalHandleError (err, vm, info) {
  if (config.errorHandler) {
    try {
      return config.errorHandler.call(null, err, vm, info)
    } catch (e) {
      // if the user intentionally throws the original error in the handler,
      // do not log it twice
      if (e !== err) {
        logError(e, null, 'config.errorHandler');
      }
    }
  }
  logError(err, vm, info);
}

function logError (err, vm, info) {
  if (true) {
    warn(("Error in " + info + ": \"" + (err.toString()) + "\""), vm);
  }
  /* istanbul ignore else */
  if ((inBrowser || inWeex) && typeof console !== 'undefined') {
    console.error(err);
  } else {
    throw err
  }
}

/*  */

var callbacks = [];
var pending = false;

function flushCallbacks () {
  pending = false;
  var copies = callbacks.slice(0);
  callbacks.length = 0;
  for (var i = 0; i < copies.length; i++) {
    copies[i]();
  }
}

// Here we have async deferring wrappers using microtasks.
// In 2.5 we used (macro) tasks (in combination with microtasks).
// However, it has subtle problems when state is changed right before repaint
// (e.g. #6813, out-in transitions).
// Also, using (macro) tasks in event handler would cause some weird behaviors
// that cannot be circumvented (e.g. #7109, #7153, #7546, #7834, #8109).
// So we now use microtasks everywhere, again.
// A major drawback of this tradeoff is that there are some scenarios
// where microtasks have too high a priority and fire in between supposedly
// sequential events (e.g. #4521, #6690, which have workarounds)
// or even between bubbling of the same event (#6566).
var timerFunc;

// The nextTick behavior leverages the microtask queue, which can be accessed
// via either native Promise.then or MutationObserver.
// MutationObserver has wider support, however it is seriously bugged in
// UIWebView in iOS >= 9.3.3 when triggered in touch event handlers. It
// completely stops working after triggering a few times... so, if native
// Promise is available, we will use it:
/* istanbul ignore next, $flow-disable-line */
if (typeof Promise !== 'undefined' && isNative(Promise)) {
  var p = Promise.resolve();
  timerFunc = function () {
    p.then(flushCallbacks);
    // In problematic UIWebViews, Promise.then doesn't completely break, but
    // it can get stuck in a weird state where callbacks are pushed into the
    // microtask queue but the queue isn't being flushed, until the browser
    // needs to do some other work, e.g. handle a timer. Therefore we can
    // "force" the microtask queue to be flushed by adding an empty timer.
    if (isIOS) { setTimeout(noop); }
  };
} else if (!isIE && typeof MutationObserver !== 'undefined' && (
  isNative(MutationObserver) ||
  // PhantomJS and iOS 7.x
  MutationObserver.toString() === '[object MutationObserverConstructor]'
)) {
  // Use MutationObserver where native Promise is not available,
  // e.g. PhantomJS, iOS7, Android 4.4
  // (#6466 MutationObserver is unreliable in IE11)
  var counter = 1;
  var observer = new MutationObserver(flushCallbacks);
  var textNode = document.createTextNode(String(counter));
  observer.observe(textNode, {
    characterData: true
  });
  timerFunc = function () {
    counter = (counter + 1) % 2;
    textNode.data = String(counter);
  };
} else if (typeof setImmediate !== 'undefined' && isNative(setImmediate)) {
  // Fallback to setImmediate.
  // Technically it leverages the (macro) task queue,
  // but it is still a better choice than setTimeout.
  timerFunc = function () {
    setImmediate(flushCallbacks);
  };
} else {
  // Fallback to setTimeout.
  timerFunc = function () {
    setTimeout(flushCallbacks, 0);
  };
}

function nextTick (cb, ctx) {
  var _resolve;
  callbacks.push(function () {
    if (cb) {
      try {
        cb.call(ctx);
      } catch (e) {
        handleError(e, ctx, 'nextTick');
      }
    } else if (_resolve) {
      _resolve(ctx);
    }
  });
  if (!pending) {
    pending = true;
    timerFunc();
  }
  // $flow-disable-line
  if (!cb && typeof Promise !== 'undefined') {
    return new Promise(function (resolve) {
      _resolve = resolve;
    })
  }
}

/*  */

/* not type checking this file because flow doesn't play well with Proxy */

var initProxy;

if (true) {
  var allowedGlobals = makeMap(
    'Infinity,undefined,NaN,isFinite,isNaN,' +
    'parseFloat,parseInt,decodeURI,decodeURIComponent,encodeURI,encodeURIComponent,' +
    'Math,Number,Date,Array,Object,Boolean,String,RegExp,Map,Set,JSON,Intl,' +
    'require' // for Webpack/Browserify
  );

  var warnNonPresent = function (target, key) {
    warn(
      "Property or method \"" + key + "\" is not defined on the instance but " +
      'referenced during render. Make sure that this property is reactive, ' +
      'either in the data option, or for class-based components, by ' +
      'initializing the property. ' +
      'See: https://vuejs.org/v2/guide/reactivity.html#Declaring-Reactive-Properties.',
      target
    );
  };

  var warnReservedPrefix = function (target, key) {
    warn(
      "Property \"" + key + "\" must be accessed with \"$data." + key + "\" because " +
      'properties starting with "$" or "_" are not proxied in the Vue instance to ' +
      'prevent conflicts with Vue internals. ' +
      'See: https://vuejs.org/v2/api/#data',
      target
    );
  };

  var hasProxy =
    typeof Proxy !== 'undefined' && isNative(Proxy);

  if (hasProxy) {
    var isBuiltInModifier = makeMap('stop,prevent,self,ctrl,shift,alt,meta,exact');
    config.keyCodes = new Proxy(config.keyCodes, {
      set: function set (target, key, value) {
        if (isBuiltInModifier(key)) {
          warn(("Avoid overwriting built-in modifier in config.keyCodes: ." + key));
          return false
        } else {
          target[key] = value;
          return true
        }
      }
    });
  }

  var hasHandler = {
    has: function has (target, key) {
      var has = key in target;
      var isAllowed = allowedGlobals(key) ||
        (typeof key === 'string' && key.charAt(0) === '_' && !(key in target.$data));
      if (!has && !isAllowed) {
        if (key in target.$data) { warnReservedPrefix(target, key); }
        else { warnNonPresent(target, key); }
      }
      return has || !isAllowed
    }
  };

  var getHandler = {
    get: function get (target, key) {
      if (typeof key === 'string' && !(key in target)) {
        if (key in target.$data) { warnReservedPrefix(target, key); }
        else { warnNonPresent(target, key); }
      }
      return target[key]
    }
  };

  initProxy = function initProxy (vm) {
    if (hasProxy) {
      // determine which proxy handler to use
      var options = vm.$options;
      var handlers = options.render && options.render._withStripped
        ? getHandler
        : hasHandler;
      vm._renderProxy = new Proxy(vm, handlers);
    } else {
      vm._renderProxy = vm;
    }
  };
}

/*  */

var seenObjects = new _Set();

/**
 * Recursively traverse an object to evoke all converted
 * getters, so that every nested property inside the object
 * is collected as a "deep" dependency.
 */
function traverse (val) {
  _traverse(val, seenObjects);
  seenObjects.clear();
}

function _traverse (val, seen) {
  var i, keys;
  var isA = Array.isArray(val);
  if ((!isA && !isObject(val)) || Object.isFrozen(val) || val instanceof VNode) {
    return
  }
  if (val.__ob__) {
    var depId = val.__ob__.dep.id;
    if (seen.has(depId)) {
      return
    }
    seen.add(depId);
  }
  if (isA) {
    i = val.length;
    while (i--) { _traverse(val[i], seen); }
  } else {
    keys = Object.keys(val);
    i = keys.length;
    while (i--) { _traverse(val[keys[i]], seen); }
  }
}

var mark;
var measure;

if (true) {
  var perf = inBrowser && window.performance;
  /* istanbul ignore if */
  if (
    perf &&
    perf.mark &&
    perf.measure &&
    perf.clearMarks &&
    perf.clearMeasures
  ) {
    mark = function (tag) { return perf.mark(tag); };
    measure = function (name, startTag, endTag) {
      perf.measure(name, startTag, endTag);
      perf.clearMarks(startTag);
      perf.clearMarks(endTag);
      // perf.clearMeasures(name)
    };
  }
}

/*  */

var normalizeEvent = cached(function (name) {
  var passive = name.charAt(0) === '&';
  name = passive ? name.slice(1) : name;
  var once$$1 = name.charAt(0) === '~'; // Prefixed last, checked first
  name = once$$1 ? name.slice(1) : name;
  var capture = name.charAt(0) === '!';
  name = capture ? name.slice(1) : name;
  return {
    name: name,
    once: once$$1,
    capture: capture,
    passive: passive
  }
});

function createFnInvoker (fns, vm) {
  function invoker () {
    var arguments$1 = arguments;

    var fns = invoker.fns;
    if (Array.isArray(fns)) {
      var cloned = fns.slice();
      for (var i = 0; i < cloned.length; i++) {
        invokeWithErrorHandling(cloned[i], null, arguments$1, vm, "v-on handler");
      }
    } else {
      // return handler return value for single handlers
      return invokeWithErrorHandling(fns, null, arguments, vm, "v-on handler")
    }
  }
  invoker.fns = fns;
  return invoker
}

function updateListeners (
  on,
  oldOn,
  add,
  remove$$1,
  createOnceHandler,
  vm
) {
  var name, def$$1, cur, old, event;
  for (name in on) {
    def$$1 = cur = on[name];
    old = oldOn[name];
    event = normalizeEvent(name);
    if (isUndef(cur)) {
       true && warn(
        "Invalid handler for event \"" + (event.name) + "\": got " + String(cur),
        vm
      );
    } else if (isUndef(old)) {
      if (isUndef(cur.fns)) {
        cur = on[name] = createFnInvoker(cur, vm);
      }
      if (isTrue(event.once)) {
        cur = on[name] = createOnceHandler(event.name, cur, event.capture);
      }
      add(event.name, cur, event.capture, event.passive, event.params);
    } else if (cur !== old) {
      old.fns = cur;
      on[name] = old;
    }
  }
  for (name in oldOn) {
    if (isUndef(on[name])) {
      event = normalizeEvent(name);
      remove$$1(event.name, oldOn[name], event.capture);
    }
  }
}

/*  */

/*  */

// fixed by xxxxxx (mp properties)
function extractPropertiesFromVNodeData(data, Ctor, res, context) {
  var propOptions = Ctor.options.mpOptions && Ctor.options.mpOptions.properties;
  if (isUndef(propOptions)) {
    return res
  }
  var externalClasses = Ctor.options.mpOptions.externalClasses || [];
  var attrs = data.attrs;
  var props = data.props;
  if (isDef(attrs) || isDef(props)) {
    for (var key in propOptions) {
      var altKey = hyphenate(key);
      var result = checkProp(res, props, key, altKey, true) ||
          checkProp(res, attrs, key, altKey, false);
      // externalClass
      if (
        result &&
        res[key] &&
        externalClasses.indexOf(altKey) !== -1 &&
        context[camelize(res[key])]
      ) {
        // 赋值 externalClass 真正的值(模板里 externalClass 的值可能是字符串)
        res[key] = context[camelize(res[key])];
      }
    }
  }
  return res
}

function extractPropsFromVNodeData (
  data,
  Ctor,
  tag,
  context// fixed by xxxxxx
) {
  // we are only extracting raw values here.
  // validation and default values are handled in the child
  // component itself.
  var propOptions = Ctor.options.props;
  if (isUndef(propOptions)) {
    // fixed by xxxxxx
    return extractPropertiesFromVNodeData(data, Ctor, {}, context)
  }
  var res = {};
  var attrs = data.attrs;
  var props = data.props;
  if (isDef(attrs) || isDef(props)) {
    for (var key in propOptions) {
      var altKey = hyphenate(key);
      if (true) {
        var keyInLowerCase = key.toLowerCase();
        if (
          key !== keyInLowerCase &&
          attrs && hasOwn(attrs, keyInLowerCase)
        ) {
          tip(
            "Prop \"" + keyInLowerCase + "\" is passed to component " +
            (formatComponentName(tag || Ctor)) + ", but the declared prop name is" +
            " \"" + key + "\". " +
            "Note that HTML attributes are case-insensitive and camelCased " +
            "props need to use their kebab-case equivalents when using in-DOM " +
            "templates. You should probably use \"" + altKey + "\" instead of \"" + key + "\"."
          );
        }
      }
      checkProp(res, props, key, altKey, true) ||
      checkProp(res, attrs, key, altKey, false);
    }
  }
  // fixed by xxxxxx
  return extractPropertiesFromVNodeData(data, Ctor, res, context)
}

function checkProp (
  res,
  hash,
  key,
  altKey,
  preserve
) {
  if (isDef(hash)) {
    if (hasOwn(hash, key)) {
      res[key] = hash[key];
      if (!preserve) {
        delete hash[key];
      }
      return true
    } else if (hasOwn(hash, altKey)) {
      res[key] = hash[altKey];
      if (!preserve) {
        delete hash[altKey];
      }
      return true
    }
  }
  return false
}

/*  */

// The template compiler attempts to minimize the need for normalization by
// statically analyzing the template at compile time.
//
// For plain HTML markup, normalization can be completely skipped because the
// generated render function is guaranteed to return Array<VNode>. There are
// two cases where extra normalization is needed:

// 1. When the children contains components - because a functional component
// may return an Array instead of a single root. In this case, just a simple
// normalization is needed - if any child is an Array, we flatten the whole
// thing with Array.prototype.concat. It is guaranteed to be only 1-level deep
// because functional components already normalize their own children.
function simpleNormalizeChildren (children) {
  for (var i = 0; i < children.length; i++) {
    if (Array.isArray(children[i])) {
      return Array.prototype.concat.apply([], children)
    }
  }
  return children
}

// 2. When the children contains constructs that always generated nested Arrays,
// e.g. <template>, <slot>, v-for, or when the children is provided by user
// with hand-written render functions / JSX. In such cases a full normalization
// is needed to cater to all possible types of children values.
function normalizeChildren (children) {
  return isPrimitive(children)
    ? [createTextVNode(children)]
    : Array.isArray(children)
      ? normalizeArrayChildren(children)
      : undefined
}

function isTextNode (node) {
  return isDef(node) && isDef(node.text) && isFalse(node.isComment)
}

function normalizeArrayChildren (children, nestedIndex) {
  var res = [];
  var i, c, lastIndex, last;
  for (i = 0; i < children.length; i++) {
    c = children[i];
    if (isUndef(c) || typeof c === 'boolean') { continue }
    lastIndex = res.length - 1;
    last = res[lastIndex];
    //  nested
    if (Array.isArray(c)) {
      if (c.length > 0) {
        c = normalizeArrayChildren(c, ((nestedIndex || '') + "_" + i));
        // merge adjacent text nodes
        if (isTextNode(c[0]) && isTextNode(last)) {
          res[lastIndex] = createTextVNode(last.text + (c[0]).text);
          c.shift();
        }
        res.push.apply(res, c);
      }
    } else if (isPrimitive(c)) {
      if (isTextNode(last)) {
        // merge adjacent text nodes
        // this is necessary for SSR hydration because text nodes are
        // essentially merged when rendered to HTML strings
        res[lastIndex] = createTextVNode(last.text + c);
      } else if (c !== '') {
        // convert primitive to vnode
        res.push(createTextVNode(c));
      }
    } else {
      if (isTextNode(c) && isTextNode(last)) {
        // merge adjacent text nodes
        res[lastIndex] = createTextVNode(last.text + c.text);
      } else {
        // default key for nested array children (likely generated by v-for)
        if (isTrue(children._isVList) &&
          isDef(c.tag) &&
          isUndef(c.key) &&
          isDef(nestedIndex)) {
          c.key = "__vlist" + nestedIndex + "_" + i + "__";
        }
        res.push(c);
      }
    }
  }
  return res
}

/*  */

function initProvide (vm) {
  var provide = vm.$options.provide;
  if (provide) {
    vm._provided = typeof provide === 'function'
      ? provide.call(vm)
      : provide;
  }
}

function initInjections (vm) {
  var result = resolveInject(vm.$options.inject, vm);
  if (result) {
    toggleObserving(false);
    Object.keys(result).forEach(function (key) {
      /* istanbul ignore else */
      if (true) {
        defineReactive$$1(vm, key, result[key], function () {
          warn(
            "Avoid mutating an injected value directly since the changes will be " +
            "overwritten whenever the provided component re-renders. " +
            "injection being mutated: \"" + key + "\"",
            vm
          );
        });
      } else {}
    });
    toggleObserving(true);
  }
}

function resolveInject (inject, vm) {
  if (inject) {
    // inject is :any because flow is not smart enough to figure out cached
    var result = Object.create(null);
    var keys = hasSymbol
      ? Reflect.ownKeys(inject)
      : Object.keys(inject);

    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];
      // #6574 in case the inject object is observed...
      if (key === '__ob__') { continue }
      var provideKey = inject[key].from;
      var source = vm;
      while (source) {
        if (source._provided && hasOwn(source._provided, provideKey)) {
          result[key] = source._provided[provideKey];
          break
        }
        source = source.$parent;
      }
      if (!source) {
        if ('default' in inject[key]) {
          var provideDefault = inject[key].default;
          result[key] = typeof provideDefault === 'function'
            ? provideDefault.call(vm)
            : provideDefault;
        } else if (true) {
          warn(("Injection \"" + key + "\" not found"), vm);
        }
      }
    }
    return result
  }
}

/*  */



/**
 * Runtime helper for resolving raw children VNodes into a slot object.
 */
function resolveSlots (
  children,
  context
) {
  if (!children || !children.length) {
    return {}
  }
  var slots = {};
  for (var i = 0, l = children.length; i < l; i++) {
    var child = children[i];
    var data = child.data;
    // remove slot attribute if the node is resolved as a Vue slot node
    if (data && data.attrs && data.attrs.slot) {
      delete data.attrs.slot;
    }
    // named slots should only be respected if the vnode was rendered in the
    // same context.
    if ((child.context === context || child.fnContext === context) &&
      data && data.slot != null
    ) {
      var name = data.slot;
      var slot = (slots[name] || (slots[name] = []));
      if (child.tag === 'template') {
        slot.push.apply(slot, child.children || []);
      } else {
        slot.push(child);
      }
    } else {
      // fixed by xxxxxx 临时 hack 掉 uni-app 中的异步 name slot page
      if(child.asyncMeta && child.asyncMeta.data && child.asyncMeta.data.slot === 'page'){
        (slots['page'] || (slots['page'] = [])).push(child);
      }else{
        (slots.default || (slots.default = [])).push(child);
      }
    }
  }
  // ignore slots that contains only whitespace
  for (var name$1 in slots) {
    if (slots[name$1].every(isWhitespace)) {
      delete slots[name$1];
    }
  }
  return slots
}

function isWhitespace (node) {
  return (node.isComment && !node.asyncFactory) || node.text === ' '
}

/*  */

function normalizeScopedSlots (
  slots,
  normalSlots,
  prevSlots
) {
  var res;
  var hasNormalSlots = Object.keys(normalSlots).length > 0;
  var isStable = slots ? !!slots.$stable : !hasNormalSlots;
  var key = slots && slots.$key;
  if (!slots) {
    res = {};
  } else if (slots._normalized) {
    // fast path 1: child component re-render only, parent did not change
    return slots._normalized
  } else if (
    isStable &&
    prevSlots &&
    prevSlots !== emptyObject &&
    key === prevSlots.$key &&
    !hasNormalSlots &&
    !prevSlots.$hasNormal
  ) {
    // fast path 2: stable scoped slots w/ no normal slots to proxy,
    // only need to normalize once
    return prevSlots
  } else {
    res = {};
    for (var key$1 in slots) {
      if (slots[key$1] && key$1[0] !== '$') {
        res[key$1] = normalizeScopedSlot(normalSlots, key$1, slots[key$1]);
      }
    }
  }
  // expose normal slots on scopedSlots
  for (var key$2 in normalSlots) {
    if (!(key$2 in res)) {
      res[key$2] = proxyNormalSlot(normalSlots, key$2);
    }
  }
  // avoriaz seems to mock a non-extensible $scopedSlots object
  // and when that is passed down this would cause an error
  if (slots && Object.isExtensible(slots)) {
    (slots)._normalized = res;
  }
  def(res, '$stable', isStable);
  def(res, '$key', key);
  def(res, '$hasNormal', hasNormalSlots);
  return res
}

function normalizeScopedSlot(normalSlots, key, fn) {
  var normalized = function () {
    var res = arguments.length ? fn.apply(null, arguments) : fn({});
    res = res && typeof res === 'object' && !Array.isArray(res)
      ? [res] // single vnode
      : normalizeChildren(res);
    return res && (
      res.length === 0 ||
      (res.length === 1 && res[0].isComment) // #9658
    ) ? undefined
      : res
  };
  // this is a slot using the new v-slot syntax without scope. although it is
  // compiled as a scoped slot, render fn users would expect it to be present
  // on this.$slots because the usage is semantically a normal slot.
  if (fn.proxy) {
    Object.defineProperty(normalSlots, key, {
      get: normalized,
      enumerable: true,
      configurable: true
    });
  }
  return normalized
}

function proxyNormalSlot(slots, key) {
  return function () { return slots[key]; }
}

/*  */

/**
 * Runtime helper for rendering v-for lists.
 */
function renderList (
  val,
  render
) {
  var ret, i, l, keys, key;
  if (Array.isArray(val) || typeof val === 'string') {
    ret = new Array(val.length);
    for (i = 0, l = val.length; i < l; i++) {
      ret[i] = render(val[i], i, i, i); // fixed by xxxxxx
    }
  } else if (typeof val === 'number') {
    ret = new Array(val);
    for (i = 0; i < val; i++) {
      ret[i] = render(i + 1, i, i, i); // fixed by xxxxxx
    }
  } else if (isObject(val)) {
    if (hasSymbol && val[Symbol.iterator]) {
      ret = [];
      var iterator = val[Symbol.iterator]();
      var result = iterator.next();
      while (!result.done) {
        ret.push(render(result.value, ret.length, i, i++)); // fixed by xxxxxx
        result = iterator.next();
      }
    } else {
      keys = Object.keys(val);
      ret = new Array(keys.length);
      for (i = 0, l = keys.length; i < l; i++) {
        key = keys[i];
        ret[i] = render(val[key], key, i, i); // fixed by xxxxxx
      }
    }
  }
  if (!isDef(ret)) {
    ret = [];
  }
  (ret)._isVList = true;
  return ret
}

/*  */

/**
 * Runtime helper for rendering <slot>
 */
function renderSlot (
  name,
  fallback,
  props,
  bindObject
) {
  var scopedSlotFn = this.$scopedSlots[name];
  var nodes;
  if (scopedSlotFn) { // scoped slot
    props = props || {};
    if (bindObject) {
      if ( true && !isObject(bindObject)) {
        warn(
          'slot v-bind without argument expects an Object',
          this
        );
      }
      props = extend(extend({}, bindObject), props);
    }
    // fixed by xxxxxx app-plus scopedSlot
    nodes = scopedSlotFn(props, this, props._i) || fallback;
  } else {
    nodes = this.$slots[name] || fallback;
  }

  var target = props && props.slot;
  if (target) {
    return this.$createElement('template', { slot: target }, nodes)
  } else {
    return nodes
  }
}

/*  */

/**
 * Runtime helper for resolving filters
 */
function resolveFilter (id) {
  return resolveAsset(this.$options, 'filters', id, true) || identity
}

/*  */

function isKeyNotMatch (expect, actual) {
  if (Array.isArray(expect)) {
    return expect.indexOf(actual) === -1
  } else {
    return expect !== actual
  }
}

/**
 * Runtime helper for checking keyCodes from config.
 * exposed as Vue.prototype._k
 * passing in eventKeyName as last argument separately for backwards compat
 */
function checkKeyCodes (
  eventKeyCode,
  key,
  builtInKeyCode,
  eventKeyName,
  builtInKeyName
) {
  var mappedKeyCode = config.keyCodes[key] || builtInKeyCode;
  if (builtInKeyName && eventKeyName && !config.keyCodes[key]) {
    return isKeyNotMatch(builtInKeyName, eventKeyName)
  } else if (mappedKeyCode) {
    return isKeyNotMatch(mappedKeyCode, eventKeyCode)
  } else if (eventKeyName) {
    return hyphenate(eventKeyName) !== key
  }
}

/*  */

/**
 * Runtime helper for merging v-bind="object" into a VNode's data.
 */
function bindObjectProps (
  data,
  tag,
  value,
  asProp,
  isSync
) {
  if (value) {
    if (!isObject(value)) {
       true && warn(
        'v-bind without argument expects an Object or Array value',
        this
      );
    } else {
      if (Array.isArray(value)) {
        value = toObject(value);
      }
      var hash;
      var loop = function ( key ) {
        if (
          key === 'class' ||
          key === 'style' ||
          isReservedAttribute(key)
        ) {
          hash = data;
        } else {
          var type = data.attrs && data.attrs.type;
          hash = asProp || config.mustUseProp(tag, type, key)
            ? data.domProps || (data.domProps = {})
            : data.attrs || (data.attrs = {});
        }
        var camelizedKey = camelize(key);
        var hyphenatedKey = hyphenate(key);
        if (!(camelizedKey in hash) && !(hyphenatedKey in hash)) {
          hash[key] = value[key];

          if (isSync) {
            var on = data.on || (data.on = {});
            on[("update:" + key)] = function ($event) {
              value[key] = $event;
            };
          }
        }
      };

      for (var key in value) loop( key );
    }
  }
  return data
}

/*  */

/**
 * Runtime helper for rendering static trees.
 */
function renderStatic (
  index,
  isInFor
) {
  var cached = this._staticTrees || (this._staticTrees = []);
  var tree = cached[index];
  // if has already-rendered static tree and not inside v-for,
  // we can reuse the same tree.
  if (tree && !isInFor) {
    return tree
  }
  // otherwise, render a fresh tree.
  tree = cached[index] = this.$options.staticRenderFns[index].call(
    this._renderProxy,
    null,
    this // for render fns generated for functional component templates
  );
  markStatic(tree, ("__static__" + index), false);
  return tree
}

/**
 * Runtime helper for v-once.
 * Effectively it means marking the node as static with a unique key.
 */
function markOnce (
  tree,
  index,
  key
) {
  markStatic(tree, ("__once__" + index + (key ? ("_" + key) : "")), true);
  return tree
}

function markStatic (
  tree,
  key,
  isOnce
) {
  if (Array.isArray(tree)) {
    for (var i = 0; i < tree.length; i++) {
      if (tree[i] && typeof tree[i] !== 'string') {
        markStaticNode(tree[i], (key + "_" + i), isOnce);
      }
    }
  } else {
    markStaticNode(tree, key, isOnce);
  }
}

function markStaticNode (node, key, isOnce) {
  node.isStatic = true;
  node.key = key;
  node.isOnce = isOnce;
}

/*  */

function bindObjectListeners (data, value) {
  if (value) {
    if (!isPlainObject(value)) {
       true && warn(
        'v-on without argument expects an Object value',
        this
      );
    } else {
      var on = data.on = data.on ? extend({}, data.on) : {};
      for (var key in value) {
        var existing = on[key];
        var ours = value[key];
        on[key] = existing ? [].concat(existing, ours) : ours;
      }
    }
  }
  return data
}

/*  */

function resolveScopedSlots (
  fns, // see flow/vnode
  res,
  // the following are added in 2.6
  hasDynamicKeys,
  contentHashKey
) {
  res = res || { $stable: !hasDynamicKeys };
  for (var i = 0; i < fns.length; i++) {
    var slot = fns[i];
    if (Array.isArray(slot)) {
      resolveScopedSlots(slot, res, hasDynamicKeys);
    } else if (slot) {
      // marker for reverse proxying v-slot without scope on this.$slots
      if (slot.proxy) {
        slot.fn.proxy = true;
      }
      res[slot.key] = slot.fn;
    }
  }
  if (contentHashKey) {
    (res).$key = contentHashKey;
  }
  return res
}

/*  */

function bindDynamicKeys (baseObj, values) {
  for (var i = 0; i < values.length; i += 2) {
    var key = values[i];
    if (typeof key === 'string' && key) {
      baseObj[values[i]] = values[i + 1];
    } else if ( true && key !== '' && key !== null) {
      // null is a special value for explicitly removing a binding
      warn(
        ("Invalid value for dynamic directive argument (expected string or null): " + key),
        this
      );
    }
  }
  return baseObj
}

// helper to dynamically append modifier runtime markers to event names.
// ensure only append when value is already string, otherwise it will be cast
// to string and cause the type check to miss.
function prependModifier (value, symbol) {
  return typeof value === 'string' ? symbol + value : value
}

/*  */

function installRenderHelpers (target) {
  target._o = markOnce;
  target._n = toNumber;
  target._s = toString;
  target._l = renderList;
  target._t = renderSlot;
  target._q = looseEqual;
  target._i = looseIndexOf;
  target._m = renderStatic;
  target._f = resolveFilter;
  target._k = checkKeyCodes;
  target._b = bindObjectProps;
  target._v = createTextVNode;
  target._e = createEmptyVNode;
  target._u = resolveScopedSlots;
  target._g = bindObjectListeners;
  target._d = bindDynamicKeys;
  target._p = prependModifier;
}

/*  */

function FunctionalRenderContext (
  data,
  props,
  children,
  parent,
  Ctor
) {
  var this$1 = this;

  var options = Ctor.options;
  // ensure the createElement function in functional components
  // gets a unique context - this is necessary for correct named slot check
  var contextVm;
  if (hasOwn(parent, '_uid')) {
    contextVm = Object.create(parent);
    // $flow-disable-line
    contextVm._original = parent;
  } else {
    // the context vm passed in is a functional context as well.
    // in this case we want to make sure we are able to get a hold to the
    // real context instance.
    contextVm = parent;
    // $flow-disable-line
    parent = parent._original;
  }
  var isCompiled = isTrue(options._compiled);
  var needNormalization = !isCompiled;

  this.data = data;
  this.props = props;
  this.children = children;
  this.parent = parent;
  this.listeners = data.on || emptyObject;
  this.injections = resolveInject(options.inject, parent);
  this.slots = function () {
    if (!this$1.$slots) {
      normalizeScopedSlots(
        data.scopedSlots,
        this$1.$slots = resolveSlots(children, parent)
      );
    }
    return this$1.$slots
  };

  Object.defineProperty(this, 'scopedSlots', ({
    enumerable: true,
    get: function get () {
      return normalizeScopedSlots(data.scopedSlots, this.slots())
    }
  }));

  // support for compiled functional template
  if (isCompiled) {
    // exposing $options for renderStatic()
    this.$options = options;
    // pre-resolve slots for renderSlot()
    this.$slots = this.slots();
    this.$scopedSlots = normalizeScopedSlots(data.scopedSlots, this.$slots);
  }

  if (options._scopeId) {
    this._c = function (a, b, c, d) {
      var vnode = createElement(contextVm, a, b, c, d, needNormalization);
      if (vnode && !Array.isArray(vnode)) {
        vnode.fnScopeId = options._scopeId;
        vnode.fnContext = parent;
      }
      return vnode
    };
  } else {
    this._c = function (a, b, c, d) { return createElement(contextVm, a, b, c, d, needNormalization); };
  }
}

installRenderHelpers(FunctionalRenderContext.prototype);

function createFunctionalComponent (
  Ctor,
  propsData,
  data,
  contextVm,
  children
) {
  var options = Ctor.options;
  var props = {};
  var propOptions = options.props;
  if (isDef(propOptions)) {
    for (var key in propOptions) {
      props[key] = validateProp(key, propOptions, propsData || emptyObject);
    }
  } else {
    if (isDef(data.attrs)) { mergeProps(props, data.attrs); }
    if (isDef(data.props)) { mergeProps(props, data.props); }
  }

  var renderContext = new FunctionalRenderContext(
    data,
    props,
    children,
    contextVm,
    Ctor
  );

  var vnode = options.render.call(null, renderContext._c, renderContext);

  if (vnode instanceof VNode) {
    return cloneAndMarkFunctionalResult(vnode, data, renderContext.parent, options, renderContext)
  } else if (Array.isArray(vnode)) {
    var vnodes = normalizeChildren(vnode) || [];
    var res = new Array(vnodes.length);
    for (var i = 0; i < vnodes.length; i++) {
      res[i] = cloneAndMarkFunctionalResult(vnodes[i], data, renderContext.parent, options, renderContext);
    }
    return res
  }
}

function cloneAndMarkFunctionalResult (vnode, data, contextVm, options, renderContext) {
  // #7817 clone node before setting fnContext, otherwise if the node is reused
  // (e.g. it was from a cached normal slot) the fnContext causes named slots
  // that should not be matched to match.
  var clone = cloneVNode(vnode);
  clone.fnContext = contextVm;
  clone.fnOptions = options;
  if (true) {
    (clone.devtoolsMeta = clone.devtoolsMeta || {}).renderContext = renderContext;
  }
  if (data.slot) {
    (clone.data || (clone.data = {})).slot = data.slot;
  }
  return clone
}

function mergeProps (to, from) {
  for (var key in from) {
    to[camelize(key)] = from[key];
  }
}

/*  */

/*  */

/*  */

/*  */

// inline hooks to be invoked on component VNodes during patch
var componentVNodeHooks = {
  init: function init (vnode, hydrating) {
    if (
      vnode.componentInstance &&
      !vnode.componentInstance._isDestroyed &&
      vnode.data.keepAlive
    ) {
      // kept-alive components, treat as a patch
      var mountedNode = vnode; // work around flow
      componentVNodeHooks.prepatch(mountedNode, mountedNode);
    } else {
      var child = vnode.componentInstance = createComponentInstanceForVnode(
        vnode,
        activeInstance
      );
      child.$mount(hydrating ? vnode.elm : undefined, hydrating);
    }
  },

  prepatch: function prepatch (oldVnode, vnode) {
    var options = vnode.componentOptions;
    var child = vnode.componentInstance = oldVnode.componentInstance;
    updateChildComponent(
      child,
      options.propsData, // updated props
      options.listeners, // updated listeners
      vnode, // new parent vnode
      options.children // new children
    );
  },

  insert: function insert (vnode) {
    var context = vnode.context;
    var componentInstance = vnode.componentInstance;
    if (!componentInstance._isMounted) {
      callHook(componentInstance, 'onServiceCreated');
      callHook(componentInstance, 'onServiceAttached');
      componentInstance._isMounted = true;
      callHook(componentInstance, 'mounted');
    }
    if (vnode.data.keepAlive) {
      if (context._isMounted) {
        // vue-router#1212
        // During updates, a kept-alive component's child components may
        // change, so directly walking the tree here may call activated hooks
        // on incorrect children. Instead we push them into a queue which will
        // be processed after the whole patch process ended.
        queueActivatedComponent(componentInstance);
      } else {
        activateChildComponent(componentInstance, true /* direct */);
      }
    }
  },

  destroy: function destroy (vnode) {
    var componentInstance = vnode.componentInstance;
    if (!componentInstance._isDestroyed) {
      if (!vnode.data.keepAlive) {
        componentInstance.$destroy();
      } else {
        deactivateChildComponent(componentInstance, true /* direct */);
      }
    }
  }
};

var hooksToMerge = Object.keys(componentVNodeHooks);

function createComponent (
  Ctor,
  data,
  context,
  children,
  tag
) {
  if (isUndef(Ctor)) {
    return
  }

  var baseCtor = context.$options._base;

  // plain options object: turn it into a constructor
  if (isObject(Ctor)) {
    Ctor = baseCtor.extend(Ctor);
  }

  // if at this stage it's not a constructor or an async component factory,
  // reject.
  if (typeof Ctor !== 'function') {
    if (true) {
      warn(("Invalid Component definition: " + (String(Ctor))), context);
    }
    return
  }

  // async component
  var asyncFactory;
  if (isUndef(Ctor.cid)) {
    asyncFactory = Ctor;
    Ctor = resolveAsyncComponent(asyncFactory, baseCtor);
    if (Ctor === undefined) {
      // return a placeholder node for async component, which is rendered
      // as a comment node but preserves all the raw information for the node.
      // the information will be used for async server-rendering and hydration.
      return createAsyncPlaceholder(
        asyncFactory,
        data,
        context,
        children,
        tag
      )
    }
  }

  data = data || {};

  // resolve constructor options in case global mixins are applied after
  // component constructor creation
  resolveConstructorOptions(Ctor);

  // transform component v-model data into props & events
  if (isDef(data.model)) {
    transformModel(Ctor.options, data);
  }

  // extract props
  var propsData = extractPropsFromVNodeData(data, Ctor, tag, context); // fixed by xxxxxx

  // functional component
  if (isTrue(Ctor.options.functional)) {
    return createFunctionalComponent(Ctor, propsData, data, context, children)
  }

  // extract listeners, since these needs to be treated as
  // child component listeners instead of DOM listeners
  var listeners = data.on;
  // replace with listeners with .native modifier
  // so it gets processed during parent component patch.
  data.on = data.nativeOn;

  if (isTrue(Ctor.options.abstract)) {
    // abstract components do not keep anything
    // other than props & listeners & slot

    // work around flow
    var slot = data.slot;
    data = {};
    if (slot) {
      data.slot = slot;
    }
  }

  // install component management hooks onto the placeholder node
  installComponentHooks(data);

  // return a placeholder vnode
  var name = Ctor.options.name || tag;
  var vnode = new VNode(
    ("vue-component-" + (Ctor.cid) + (name ? ("-" + name) : '')),
    data, undefined, undefined, undefined, context,
    { Ctor: Ctor, propsData: propsData, listeners: listeners, tag: tag, children: children },
    asyncFactory
  );

  return vnode
}

function createComponentInstanceForVnode (
  vnode, // we know it's MountedComponentVNode but flow doesn't
  parent // activeInstance in lifecycle state
) {
  var options = {
    _isComponent: true,
    _parentVnode: vnode,
    parent: parent
  };
  // check inline-template render functions
  var inlineTemplate = vnode.data.inlineTemplate;
  if (isDef(inlineTemplate)) {
    options.render = inlineTemplate.render;
    options.staticRenderFns = inlineTemplate.staticRenderFns;
  }
  return new vnode.componentOptions.Ctor(options)
}

function installComponentHooks (data) {
  var hooks = data.hook || (data.hook = {});
  for (var i = 0; i < hooksToMerge.length; i++) {
    var key = hooksToMerge[i];
    var existing = hooks[key];
    var toMerge = componentVNodeHooks[key];
    if (existing !== toMerge && !(existing && existing._merged)) {
      hooks[key] = existing ? mergeHook$1(toMerge, existing) : toMerge;
    }
  }
}

function mergeHook$1 (f1, f2) {
  var merged = function (a, b) {
    // flow complains about extra args which is why we use any
    f1(a, b);
    f2(a, b);
  };
  merged._merged = true;
  return merged
}

// transform component v-model info (value and callback) into
// prop and event handler respectively.
function transformModel (options, data) {
  var prop = (options.model && options.model.prop) || 'value';
  var event = (options.model && options.model.event) || 'input'
  ;(data.attrs || (data.attrs = {}))[prop] = data.model.value;
  var on = data.on || (data.on = {});
  var existing = on[event];
  var callback = data.model.callback;
  if (isDef(existing)) {
    if (
      Array.isArray(existing)
        ? existing.indexOf(callback) === -1
        : existing !== callback
    ) {
      on[event] = [callback].concat(existing);
    }
  } else {
    on[event] = callback;
  }
}

/*  */

var SIMPLE_NORMALIZE = 1;
var ALWAYS_NORMALIZE = 2;

// wrapper function for providing a more flexible interface
// without getting yelled at by flow
function createElement (
  context,
  tag,
  data,
  children,
  normalizationType,
  alwaysNormalize
) {
  if (Array.isArray(data) || isPrimitive(data)) {
    normalizationType = children;
    children = data;
    data = undefined;
  }
  if (isTrue(alwaysNormalize)) {
    normalizationType = ALWAYS_NORMALIZE;
  }
  return _createElement(context, tag, data, children, normalizationType)
}

function _createElement (
  context,
  tag,
  data,
  children,
  normalizationType
) {
  if (isDef(data) && isDef((data).__ob__)) {
     true && warn(
      "Avoid using observed data object as vnode data: " + (JSON.stringify(data)) + "\n" +
      'Always create fresh vnode data objects in each render!',
      context
    );
    return createEmptyVNode()
  }
  // object syntax in v-bind
  if (isDef(data) && isDef(data.is)) {
    tag = data.is;
  }
  if (!tag) {
    // in case of component :is set to falsy value
    return createEmptyVNode()
  }
  // warn against non-primitive key
  if ( true &&
    isDef(data) && isDef(data.key) && !isPrimitive(data.key)
  ) {
    {
      warn(
        'Avoid using non-primitive value as key, ' +
        'use string/number value instead.',
        context
      );
    }
  }
  // support single function children as default scoped slot
  if (Array.isArray(children) &&
    typeof children[0] === 'function'
  ) {
    data = data || {};
    data.scopedSlots = { default: children[0] };
    children.length = 0;
  }
  if (normalizationType === ALWAYS_NORMALIZE) {
    children = normalizeChildren(children);
  } else if (normalizationType === SIMPLE_NORMALIZE) {
    children = simpleNormalizeChildren(children);
  }
  var vnode, ns;
  if (typeof tag === 'string') {
    var Ctor;
    ns = (context.$vnode && context.$vnode.ns) || config.getTagNamespace(tag);
    if (config.isReservedTag(tag)) {
      // platform built-in elements
      if ( true && isDef(data) && isDef(data.nativeOn)) {
        warn(
          ("The .native modifier for v-on is only valid on components but it was used on <" + tag + ">."),
          context
        );
      }
      vnode = new VNode(
        config.parsePlatformTagName(tag), data, children,
        undefined, undefined, context
      );
    } else if ((!data || !data.pre) && isDef(Ctor = resolveAsset(context.$options, 'components', tag))) {
      // component
      vnode = createComponent(Ctor, data, context, children, tag);
    } else {
      // unknown or unlisted namespaced elements
      // check at runtime because it may get assigned a namespace when its
      // parent normalizes children
      vnode = new VNode(
        tag, data, children,
        undefined, undefined, context
      );
    }
  } else {
    // direct component options / constructor
    vnode = createComponent(tag, data, context, children);
  }
  if (Array.isArray(vnode)) {
    return vnode
  } else if (isDef(vnode)) {
    if (isDef(ns)) { applyNS(vnode, ns); }
    if (isDef(data)) { registerDeepBindings(data); }
    return vnode
  } else {
    return createEmptyVNode()
  }
}

function applyNS (vnode, ns, force) {
  vnode.ns = ns;
  if (vnode.tag === 'foreignObject') {
    // use default namespace inside foreignObject
    ns = undefined;
    force = true;
  }
  if (isDef(vnode.children)) {
    for (var i = 0, l = vnode.children.length; i < l; i++) {
      var child = vnode.children[i];
      if (isDef(child.tag) && (
        isUndef(child.ns) || (isTrue(force) && child.tag !== 'svg'))) {
        applyNS(child, ns, force);
      }
    }
  }
}

// ref #5318
// necessary to ensure parent re-render when deep bindings like :style and
// :class are used on slot nodes
function registerDeepBindings (data) {
  if (isObject(data.style)) {
    traverse(data.style);
  }
  if (isObject(data.class)) {
    traverse(data.class);
  }
}

/*  */

function initRender (vm) {
  vm._vnode = null; // the root of the child tree
  vm._staticTrees = null; // v-once cached trees
  var options = vm.$options;
  var parentVnode = vm.$vnode = options._parentVnode; // the placeholder node in parent tree
  var renderContext = parentVnode && parentVnode.context;
  vm.$slots = resolveSlots(options._renderChildren, renderContext);
  vm.$scopedSlots = emptyObject;
  // bind the createElement fn to this instance
  // so that we get proper render context inside it.
  // args order: tag, data, children, normalizationType, alwaysNormalize
  // internal version is used by render functions compiled from templates
  vm._c = function (a, b, c, d) { return createElement(vm, a, b, c, d, false); };
  // normalization is always applied for the public version, used in
  // user-written render functions.
  vm.$createElement = function (a, b, c, d) { return createElement(vm, a, b, c, d, true); };

  // $attrs & $listeners are exposed for easier HOC creation.
  // they need to be reactive so that HOCs using them are always updated
  var parentData = parentVnode && parentVnode.data;

  /* istanbul ignore else */
  if (true) {
    defineReactive$$1(vm, '$attrs', parentData && parentData.attrs || emptyObject, function () {
      !isUpdatingChildComponent && warn("$attrs is readonly.", vm);
    }, true);
    defineReactive$$1(vm, '$listeners', options._parentListeners || emptyObject, function () {
      !isUpdatingChildComponent && warn("$listeners is readonly.", vm);
    }, true);
  } else {}
}

var currentRenderingInstance = null;

function renderMixin (Vue) {
  // install runtime convenience helpers
  installRenderHelpers(Vue.prototype);

  Vue.prototype.$nextTick = function (fn) {
    return nextTick(fn, this)
  };

  Vue.prototype._render = function () {
    var vm = this;
    var ref = vm.$options;
    var render = ref.render;
    var _parentVnode = ref._parentVnode;

    if (_parentVnode) {
      vm.$scopedSlots = normalizeScopedSlots(
        _parentVnode.data.scopedSlots,
        vm.$slots,
        vm.$scopedSlots
      );
    }

    // set parent vnode. this allows render functions to have access
    // to the data on the placeholder node.
    vm.$vnode = _parentVnode;
    // render self
    var vnode;
    try {
      // There's no need to maintain a stack because all render fns are called
      // separately from one another. Nested component's render fns are called
      // when parent component is patched.
      currentRenderingInstance = vm;
      vnode = render.call(vm._renderProxy, vm.$createElement);
    } catch (e) {
      handleError(e, vm, "render");
      // return error render result,
      // or previous vnode to prevent render error causing blank component
      /* istanbul ignore else */
      if ( true && vm.$options.renderError) {
        try {
          vnode = vm.$options.renderError.call(vm._renderProxy, vm.$createElement, e);
        } catch (e) {
          handleError(e, vm, "renderError");
          vnode = vm._vnode;
        }
      } else {
        vnode = vm._vnode;
      }
    } finally {
      currentRenderingInstance = null;
    }
    // if the returned array contains only a single node, allow it
    if (Array.isArray(vnode) && vnode.length === 1) {
      vnode = vnode[0];
    }
    // return empty vnode in case the render function errored out
    if (!(vnode instanceof VNode)) {
      if ( true && Array.isArray(vnode)) {
        warn(
          'Multiple root nodes returned from render function. Render function ' +
          'should return a single root node.',
          vm
        );
      }
      vnode = createEmptyVNode();
    }
    // set parent
    vnode.parent = _parentVnode;
    return vnode
  };
}

/*  */

function ensureCtor (comp, base) {
  if (
    comp.__esModule ||
    (hasSymbol && comp[Symbol.toStringTag] === 'Module')
  ) {
    comp = comp.default;
  }
  return isObject(comp)
    ? base.extend(comp)
    : comp
}

function createAsyncPlaceholder (
  factory,
  data,
  context,
  children,
  tag
) {
  var node = createEmptyVNode();
  node.asyncFactory = factory;
  node.asyncMeta = { data: data, context: context, children: children, tag: tag };
  return node
}

function resolveAsyncComponent (
  factory,
  baseCtor
) {
  if (isTrue(factory.error) && isDef(factory.errorComp)) {
    return factory.errorComp
  }

  if (isDef(factory.resolved)) {
    return factory.resolved
  }

  var owner = currentRenderingInstance;
  if (owner && isDef(factory.owners) && factory.owners.indexOf(owner) === -1) {
    // already pending
    factory.owners.push(owner);
  }

  if (isTrue(factory.loading) && isDef(factory.loadingComp)) {
    return factory.loadingComp
  }

  if (owner && !isDef(factory.owners)) {
    var owners = factory.owners = [owner];
    var sync = true;
    var timerLoading = null;
    var timerTimeout = null

    ;(owner).$on('hook:destroyed', function () { return remove(owners, owner); });

    var forceRender = function (renderCompleted) {
      for (var i = 0, l = owners.length; i < l; i++) {
        (owners[i]).$forceUpdate();
      }

      if (renderCompleted) {
        owners.length = 0;
        if (timerLoading !== null) {
          clearTimeout(timerLoading);
          timerLoading = null;
        }
        if (timerTimeout !== null) {
          clearTimeout(timerTimeout);
          timerTimeout = null;
        }
      }
    };

    var resolve = once(function (res) {
      // cache resolved
      factory.resolved = ensureCtor(res, baseCtor);
      // invoke callbacks only if this is not a synchronous resolve
      // (async resolves are shimmed as synchronous during SSR)
      if (!sync) {
        forceRender(true);
      } else {
        owners.length = 0;
      }
    });

    var reject = once(function (reason) {
       true && warn(
        "Failed to resolve async component: " + (String(factory)) +
        (reason ? ("\nReason: " + reason) : '')
      );
      if (isDef(factory.errorComp)) {
        factory.error = true;
        forceRender(true);
      }
    });

    var res = factory(resolve, reject);

    if (isObject(res)) {
      if (isPromise(res)) {
        // () => Promise
        if (isUndef(factory.resolved)) {
          res.then(resolve, reject);
        }
      } else if (isPromise(res.component)) {
        res.component.then(resolve, reject);

        if (isDef(res.error)) {
          factory.errorComp = ensureCtor(res.error, baseCtor);
        }

        if (isDef(res.loading)) {
          factory.loadingComp = ensureCtor(res.loading, baseCtor);
          if (res.delay === 0) {
            factory.loading = true;
          } else {
            timerLoading = setTimeout(function () {
              timerLoading = null;
              if (isUndef(factory.resolved) && isUndef(factory.error)) {
                factory.loading = true;
                forceRender(false);
              }
            }, res.delay || 200);
          }
        }

        if (isDef(res.timeout)) {
          timerTimeout = setTimeout(function () {
            timerTimeout = null;
            if (isUndef(factory.resolved)) {
              reject(
                 true
                  ? ("timeout (" + (res.timeout) + "ms)")
                  : undefined
              );
            }
          }, res.timeout);
        }
      }
    }

    sync = false;
    // return in case resolved synchronously
    return factory.loading
      ? factory.loadingComp
      : factory.resolved
  }
}

/*  */

function isAsyncPlaceholder (node) {
  return node.isComment && node.asyncFactory
}

/*  */

function getFirstComponentChild (children) {
  if (Array.isArray(children)) {
    for (var i = 0; i < children.length; i++) {
      var c = children[i];
      if (isDef(c) && (isDef(c.componentOptions) || isAsyncPlaceholder(c))) {
        return c
      }
    }
  }
}

/*  */

/*  */

function initEvents (vm) {
  vm._events = Object.create(null);
  vm._hasHookEvent = false;
  // init parent attached events
  var listeners = vm.$options._parentListeners;
  if (listeners) {
    updateComponentListeners(vm, listeners);
  }
}

var target;

function add (event, fn) {
  target.$on(event, fn);
}

function remove$1 (event, fn) {
  target.$off(event, fn);
}

function createOnceHandler (event, fn) {
  var _target = target;
  return function onceHandler () {
    var res = fn.apply(null, arguments);
    if (res !== null) {
      _target.$off(event, onceHandler);
    }
  }
}

function updateComponentListeners (
  vm,
  listeners,
  oldListeners
) {
  target = vm;
  updateListeners(listeners, oldListeners || {}, add, remove$1, createOnceHandler, vm);
  target = undefined;
}

function eventsMixin (Vue) {
  var hookRE = /^hook:/;
  Vue.prototype.$on = function (event, fn) {
    var vm = this;
    if (Array.isArray(event)) {
      for (var i = 0, l = event.length; i < l; i++) {
        vm.$on(event[i], fn);
      }
    } else {
      (vm._events[event] || (vm._events[event] = [])).push(fn);
      // optimize hook:event cost by using a boolean flag marked at registration
      // instead of a hash lookup
      if (hookRE.test(event)) {
        vm._hasHookEvent = true;
      }
    }
    return vm
  };

  Vue.prototype.$once = function (event, fn) {
    var vm = this;
    function on () {
      vm.$off(event, on);
      fn.apply(vm, arguments);
    }
    on.fn = fn;
    vm.$on(event, on);
    return vm
  };

  Vue.prototype.$off = function (event, fn) {
    var vm = this;
    // all
    if (!arguments.length) {
      vm._events = Object.create(null);
      return vm
    }
    // array of events
    if (Array.isArray(event)) {
      for (var i$1 = 0, l = event.length; i$1 < l; i$1++) {
        vm.$off(event[i$1], fn);
      }
      return vm
    }
    // specific event
    var cbs = vm._events[event];
    if (!cbs) {
      return vm
    }
    if (!fn) {
      vm._events[event] = null;
      return vm
    }
    // specific handler
    var cb;
    var i = cbs.length;
    while (i--) {
      cb = cbs[i];
      if (cb === fn || cb.fn === fn) {
        cbs.splice(i, 1);
        break
      }
    }
    return vm
  };

  Vue.prototype.$emit = function (event) {
    var vm = this;
    if (true) {
      var lowerCaseEvent = event.toLowerCase();
      if (lowerCaseEvent !== event && vm._events[lowerCaseEvent]) {
        tip(
          "Event \"" + lowerCaseEvent + "\" is emitted in component " +
          (formatComponentName(vm)) + " but the handler is registered for \"" + event + "\". " +
          "Note that HTML attributes are case-insensitive and you cannot use " +
          "v-on to listen to camelCase events when using in-DOM templates. " +
          "You should probably use \"" + (hyphenate(event)) + "\" instead of \"" + event + "\"."
        );
      }
    }
    var cbs = vm._events[event];
    if (cbs) {
      cbs = cbs.length > 1 ? toArray(cbs) : cbs;
      var args = toArray(arguments, 1);
      var info = "event handler for \"" + event + "\"";
      for (var i = 0, l = cbs.length; i < l; i++) {
        invokeWithErrorHandling(cbs[i], vm, args, vm, info);
      }
    }
    return vm
  };
}

/*  */

var activeInstance = null;
var isUpdatingChildComponent = false;

function setActiveInstance(vm) {
  var prevActiveInstance = activeInstance;
  activeInstance = vm;
  return function () {
    activeInstance = prevActiveInstance;
  }
}

function initLifecycle (vm) {
  var options = vm.$options;

  // locate first non-abstract parent
  var parent = options.parent;
  if (parent && !options.abstract) {
    while (parent.$options.abstract && parent.$parent) {
      parent = parent.$parent;
    }
    parent.$children.push(vm);
  }

  vm.$parent = parent;
  vm.$root = parent ? parent.$root : vm;

  vm.$children = [];
  vm.$refs = {};

  vm._watcher = null;
  vm._inactive = null;
  vm._directInactive = false;
  vm._isMounted = false;
  vm._isDestroyed = false;
  vm._isBeingDestroyed = false;
}

function lifecycleMixin (Vue) {
  Vue.prototype._update = function (vnode, hydrating) {
    var vm = this;
    var prevEl = vm.$el;
    var prevVnode = vm._vnode;
    var restoreActiveInstance = setActiveInstance(vm);
    vm._vnode = vnode;
    // Vue.prototype.__patch__ is injected in entry points
    // based on the rendering backend used.
    if (!prevVnode) {
      // initial render
      vm.$el = vm.__patch__(vm.$el, vnode, hydrating, false /* removeOnly */);
    } else {
      // updates
      vm.$el = vm.__patch__(prevVnode, vnode);
    }
    restoreActiveInstance();
    // update __vue__ reference
    if (prevEl) {
      prevEl.__vue__ = null;
    }
    if (vm.$el) {
      vm.$el.__vue__ = vm;
    }
    // if parent is an HOC, update its $el as well
    if (vm.$vnode && vm.$parent && vm.$vnode === vm.$parent._vnode) {
      vm.$parent.$el = vm.$el;
    }
    // updated hook is called by the scheduler to ensure that children are
    // updated in a parent's updated hook.
  };

  Vue.prototype.$forceUpdate = function () {
    var vm = this;
    if (vm._watcher) {
      vm._watcher.update();
    }
  };

  Vue.prototype.$destroy = function () {
    var vm = this;
    if (vm._isBeingDestroyed) {
      return
    }
    callHook(vm, 'beforeDestroy');
    vm._isBeingDestroyed = true;
    // remove self from parent
    var parent = vm.$parent;
    if (parent && !parent._isBeingDestroyed && !vm.$options.abstract) {
      remove(parent.$children, vm);
    }
    // teardown watchers
    if (vm._watcher) {
      vm._watcher.teardown();
    }
    var i = vm._watchers.length;
    while (i--) {
      vm._watchers[i].teardown();
    }
    // remove reference from data ob
    // frozen object may not have observer.
    if (vm._data.__ob__) {
      vm._data.__ob__.vmCount--;
    }
    // call the last hook...
    vm._isDestroyed = true;
    // invoke destroy hooks on current rendered tree
    vm.__patch__(vm._vnode, null);
    // fire destroyed hook
    callHook(vm, 'destroyed');
    // turn off all instance listeners.
    vm.$off();
    // remove __vue__ reference
    if (vm.$el) {
      vm.$el.__vue__ = null;
    }
    // release circular reference (#6759)
    if (vm.$vnode) {
      vm.$vnode.parent = null;
    }
  };
}

function updateChildComponent (
  vm,
  propsData,
  listeners,
  parentVnode,
  renderChildren
) {
  if (true) {
    isUpdatingChildComponent = true;
  }

  // determine whether component has slot children
  // we need to do this before overwriting $options._renderChildren.

  // check if there are dynamic scopedSlots (hand-written or compiled but with
  // dynamic slot names). Static scoped slots compiled from template has the
  // "$stable" marker.
  var newScopedSlots = parentVnode.data.scopedSlots;
  var oldScopedSlots = vm.$scopedSlots;
  var hasDynamicScopedSlot = !!(
    (newScopedSlots && !newScopedSlots.$stable) ||
    (oldScopedSlots !== emptyObject && !oldScopedSlots.$stable) ||
    (newScopedSlots && vm.$scopedSlots.$key !== newScopedSlots.$key)
  );

  // Any static slot children from the parent may have changed during parent's
  // update. Dynamic scoped slots may also have changed. In such cases, a forced
  // update is necessary to ensure correctness.
  var needsForceUpdate = !!(
    renderChildren ||               // has new static slots
    vm.$options._renderChildren ||  // has old static slots
    hasDynamicScopedSlot
  );

  vm.$options._parentVnode = parentVnode;
  vm.$vnode = parentVnode; // update vm's placeholder node without re-render

  if (vm._vnode) { // update child tree's parent
    vm._vnode.parent = parentVnode;
  }
  vm.$options._renderChildren = renderChildren;

  // update $attrs and $listeners hash
  // these are also reactive so they may trigger child update if the child
  // used them during render
  vm.$attrs = parentVnode.data.attrs || emptyObject;
  vm.$listeners = listeners || emptyObject;

  // update props
  if (propsData && vm.$options.props) {
    toggleObserving(false);
    var props = vm._props;
    var propKeys = vm.$options._propKeys || [];
    for (var i = 0; i < propKeys.length; i++) {
      var key = propKeys[i];
      var propOptions = vm.$options.props; // wtf flow?
      props[key] = validateProp(key, propOptions, propsData, vm);
    }
    toggleObserving(true);
    // keep a copy of raw propsData
    vm.$options.propsData = propsData;
  }
  
  // fixed by xxxxxx update properties(mp runtime)
  vm._$updateProperties && vm._$updateProperties(vm);
  
  // update listeners
  listeners = listeners || emptyObject;
  var oldListeners = vm.$options._parentListeners;
  vm.$options._parentListeners = listeners;
  updateComponentListeners(vm, listeners, oldListeners);

  // resolve slots + force update if has children
  if (needsForceUpdate) {
    vm.$slots = resolveSlots(renderChildren, parentVnode.context);
    vm.$forceUpdate();
  }

  if (true) {
    isUpdatingChildComponent = false;
  }
}

function isInInactiveTree (vm) {
  while (vm && (vm = vm.$parent)) {
    if (vm._inactive) { return true }
  }
  return false
}

function activateChildComponent (vm, direct) {
  if (direct) {
    vm._directInactive = false;
    if (isInInactiveTree(vm)) {
      return
    }
  } else if (vm._directInactive) {
    return
  }
  if (vm._inactive || vm._inactive === null) {
    vm._inactive = false;
    for (var i = 0; i < vm.$children.length; i++) {
      activateChildComponent(vm.$children[i]);
    }
    callHook(vm, 'activated');
  }
}

function deactivateChildComponent (vm, direct) {
  if (direct) {
    vm._directInactive = true;
    if (isInInactiveTree(vm)) {
      return
    }
  }
  if (!vm._inactive) {
    vm._inactive = true;
    for (var i = 0; i < vm.$children.length; i++) {
      deactivateChildComponent(vm.$children[i]);
    }
    callHook(vm, 'deactivated');
  }
}

function callHook (vm, hook) {
  // #7573 disable dep collection when invoking lifecycle hooks
  pushTarget();
  var handlers = vm.$options[hook];
  var info = hook + " hook";
  if (handlers) {
    for (var i = 0, j = handlers.length; i < j; i++) {
      invokeWithErrorHandling(handlers[i], vm, null, vm, info);
    }
  }
  if (vm._hasHookEvent) {
    vm.$emit('hook:' + hook);
  }
  popTarget();
}

/*  */

var MAX_UPDATE_COUNT = 100;

var queue = [];
var activatedChildren = [];
var has = {};
var circular = {};
var waiting = false;
var flushing = false;
var index = 0;

/**
 * Reset the scheduler's state.
 */
function resetSchedulerState () {
  index = queue.length = activatedChildren.length = 0;
  has = {};
  if (true) {
    circular = {};
  }
  waiting = flushing = false;
}

// Async edge case #6566 requires saving the timestamp when event listeners are
// attached. However, calling performance.now() has a perf overhead especially
// if the page has thousands of event listeners. Instead, we take a timestamp
// every time the scheduler flushes and use that for all event listeners
// attached during that flush.
var currentFlushTimestamp = 0;

// Async edge case fix requires storing an event listener's attach timestamp.
var getNow = Date.now;

// Determine what event timestamp the browser is using. Annoyingly, the
// timestamp can either be hi-res (relative to page load) or low-res
// (relative to UNIX epoch), so in order to compare time we have to use the
// same timestamp type when saving the flush timestamp.
// All IE versions use low-res event timestamps, and have problematic clock
// implementations (#9632)
if (inBrowser && !isIE) {
  var performance = window.performance;
  if (
    performance &&
    typeof performance.now === 'function' &&
    getNow() > document.createEvent('Event').timeStamp
  ) {
    // if the event timestamp, although evaluated AFTER the Date.now(), is
    // smaller than it, it means the event is using a hi-res timestamp,
    // and we need to use the hi-res version for event listener timestamps as
    // well.
    getNow = function () { return performance.now(); };
  }
}

/**
 * Flush both queues and run the watchers.
 */
function flushSchedulerQueue () {
  currentFlushTimestamp = getNow();
  flushing = true;
  var watcher, id;

  // Sort queue before flush.
  // This ensures that:
  // 1. Components are updated from parent to child. (because parent is always
  //    created before the child)
  // 2. A component's user watchers are run before its render watcher (because
  //    user watchers are created before the render watcher)
  // 3. If a component is destroyed during a parent component's watcher run,
  //    its watchers can be skipped.
  queue.sort(function (a, b) { return a.id - b.id; });

  // do not cache length because more watchers might be pushed
  // as we run existing watchers
  for (index = 0; index < queue.length; index++) {
    watcher = queue[index];
    if (watcher.before) {
      watcher.before();
    }
    id = watcher.id;
    has[id] = null;
    watcher.run();
    // in dev build, check and stop circular updates.
    if ( true && has[id] != null) {
      circular[id] = (circular[id] || 0) + 1;
      if (circular[id] > MAX_UPDATE_COUNT) {
        warn(
          'You may have an infinite update loop ' + (
            watcher.user
              ? ("in watcher with expression \"" + (watcher.expression) + "\"")
              : "in a component render function."
          ),
          watcher.vm
        );
        break
      }
    }
  }

  // keep copies of post queues before resetting state
  var activatedQueue = activatedChildren.slice();
  var updatedQueue = queue.slice();

  resetSchedulerState();

  // call component updated and activated hooks
  callActivatedHooks(activatedQueue);
  callUpdatedHooks(updatedQueue);

  // devtool hook
  /* istanbul ignore if */
  if (devtools && config.devtools) {
    devtools.emit('flush');
  }
}

function callUpdatedHooks (queue) {
  var i = queue.length;
  while (i--) {
    var watcher = queue[i];
    var vm = watcher.vm;
    if (vm._watcher === watcher && vm._isMounted && !vm._isDestroyed) {
      callHook(vm, 'updated');
    }
  }
}

/**
 * Queue a kept-alive component that was activated during patch.
 * The queue will be processed after the entire tree has been patched.
 */
function queueActivatedComponent (vm) {
  // setting _inactive to false here so that a render function can
  // rely on checking whether it's in an inactive tree (e.g. router-view)
  vm._inactive = false;
  activatedChildren.push(vm);
}

function callActivatedHooks (queue) {
  for (var i = 0; i < queue.length; i++) {
    queue[i]._inactive = true;
    activateChildComponent(queue[i], true /* true */);
  }
}

/**
 * Push a watcher into the watcher queue.
 * Jobs with duplicate IDs will be skipped unless it's
 * pushed when the queue is being flushed.
 */
function queueWatcher (watcher) {
  var id = watcher.id;
  if (has[id] == null) {
    has[id] = true;
    if (!flushing) {
      queue.push(watcher);
    } else {
      // if already flushing, splice the watcher based on its id
      // if already past its id, it will be run next immediately.
      var i = queue.length - 1;
      while (i > index && queue[i].id > watcher.id) {
        i--;
      }
      queue.splice(i + 1, 0, watcher);
    }
    // queue the flush
    if (!waiting) {
      waiting = true;

      if ( true && !config.async) {
        flushSchedulerQueue();
        return
      }
      nextTick(flushSchedulerQueue);
    }
  }
}

/*  */



var uid$2 = 0;

/**
 * A watcher parses an expression, collects dependencies,
 * and fires callback when the expression value changes.
 * This is used for both the $watch() api and directives.
 */
var Watcher = function Watcher (
  vm,
  expOrFn,
  cb,
  options,
  isRenderWatcher
) {
  this.vm = vm;
  if (isRenderWatcher) {
    vm._watcher = this;
  }
  vm._watchers.push(this);
  // options
  if (options) {
    this.deep = !!options.deep;
    this.user = !!options.user;
    this.lazy = !!options.lazy;
    this.sync = !!options.sync;
    this.before = options.before;
  } else {
    this.deep = this.user = this.lazy = this.sync = false;
  }
  this.cb = cb;
  this.id = ++uid$2; // uid for batching
  this.active = true;
  this.dirty = this.lazy; // for lazy watchers
  this.deps = [];
  this.newDeps = [];
  this.depIds = new _Set();
  this.newDepIds = new _Set();
  this.expression =  true
    ? expOrFn.toString()
    : undefined;
  // parse expression for getter
  if (typeof expOrFn === 'function') {
    this.getter = expOrFn;
  } else {
    this.getter = parsePath(expOrFn);
    if (!this.getter) {
      this.getter = noop;
       true && warn(
        "Failed watching path: \"" + expOrFn + "\" " +
        'Watcher only accepts simple dot-delimited paths. ' +
        'For full control, use a function instead.',
        vm
      );
    }
  }
  this.value = this.lazy
    ? undefined
    : this.get();
};

/**
 * Evaluate the getter, and re-collect dependencies.
 */
Watcher.prototype.get = function get () {
  pushTarget(this);
  var value;
  var vm = this.vm;
  try {
    value = this.getter.call(vm, vm);
  } catch (e) {
    if (this.user) {
      handleError(e, vm, ("getter for watcher \"" + (this.expression) + "\""));
    } else {
      throw e
    }
  } finally {
    // "touch" every property so they are all tracked as
    // dependencies for deep watching
    if (this.deep) {
      traverse(value);
    }
    popTarget();
    this.cleanupDeps();
  }
  return value
};

/**
 * Add a dependency to this directive.
 */
Watcher.prototype.addDep = function addDep (dep) {
  var id = dep.id;
  if (!this.newDepIds.has(id)) {
    this.newDepIds.add(id);
    this.newDeps.push(dep);
    if (!this.depIds.has(id)) {
      dep.addSub(this);
    }
  }
};

/**
 * Clean up for dependency collection.
 */
Watcher.prototype.cleanupDeps = function cleanupDeps () {
  var i = this.deps.length;
  while (i--) {
    var dep = this.deps[i];
    if (!this.newDepIds.has(dep.id)) {
      dep.removeSub(this);
    }
  }
  var tmp = this.depIds;
  this.depIds = this.newDepIds;
  this.newDepIds = tmp;
  this.newDepIds.clear();
  tmp = this.deps;
  this.deps = this.newDeps;
  this.newDeps = tmp;
  this.newDeps.length = 0;
};

/**
 * Subscriber interface.
 * Will be called when a dependency changes.
 */
Watcher.prototype.update = function update () {
  /* istanbul ignore else */
  if (this.lazy) {
    this.dirty = true;
  } else if (this.sync) {
    this.run();
  } else {
    queueWatcher(this);
  }
};

/**
 * Scheduler job interface.
 * Will be called by the scheduler.
 */
Watcher.prototype.run = function run () {
  if (this.active) {
    var value = this.get();
    if (
      value !== this.value ||
      // Deep watchers and watchers on Object/Arrays should fire even
      // when the value is the same, because the value may
      // have mutated.
      isObject(value) ||
      this.deep
    ) {
      // set new value
      var oldValue = this.value;
      this.value = value;
      if (this.user) {
        try {
          this.cb.call(this.vm, value, oldValue);
        } catch (e) {
          handleError(e, this.vm, ("callback for watcher \"" + (this.expression) + "\""));
        }
      } else {
        this.cb.call(this.vm, value, oldValue);
      }
    }
  }
};

/**
 * Evaluate the value of the watcher.
 * This only gets called for lazy watchers.
 */
Watcher.prototype.evaluate = function evaluate () {
  this.value = this.get();
  this.dirty = false;
};

/**
 * Depend on all deps collected by this watcher.
 */
Watcher.prototype.depend = function depend () {
  var i = this.deps.length;
  while (i--) {
    this.deps[i].depend();
  }
};

/**
 * Remove self from all dependencies' subscriber list.
 */
Watcher.prototype.teardown = function teardown () {
  if (this.active) {
    // remove self from vm's watcher list
    // this is a somewhat expensive operation so we skip it
    // if the vm is being destroyed.
    if (!this.vm._isBeingDestroyed) {
      remove(this.vm._watchers, this);
    }
    var i = this.deps.length;
    while (i--) {
      this.deps[i].removeSub(this);
    }
    this.active = false;
  }
};

/*  */

var sharedPropertyDefinition = {
  enumerable: true,
  configurable: true,
  get: noop,
  set: noop
};

function proxy (target, sourceKey, key) {
  sharedPropertyDefinition.get = function proxyGetter () {
    return this[sourceKey][key]
  };
  sharedPropertyDefinition.set = function proxySetter (val) {
    this[sourceKey][key] = val;
  };
  Object.defineProperty(target, key, sharedPropertyDefinition);
}

function initState (vm) {
  vm._watchers = [];
  var opts = vm.$options;
  if (opts.props) { initProps(vm, opts.props); }
  if (opts.methods) { initMethods(vm, opts.methods); }
  if (opts.data) {
    initData(vm);
  } else {
    observe(vm._data = {}, true /* asRootData */);
  }
  if (opts.computed) { initComputed(vm, opts.computed); }
  if (opts.watch && opts.watch !== nativeWatch) {
    initWatch(vm, opts.watch);
  }
}

function initProps (vm, propsOptions) {
  var propsData = vm.$options.propsData || {};
  var props = vm._props = {};
  // cache prop keys so that future props updates can iterate using Array
  // instead of dynamic object key enumeration.
  var keys = vm.$options._propKeys = [];
  var isRoot = !vm.$parent;
  // root instance props should be converted
  if (!isRoot) {
    toggleObserving(false);
  }
  var loop = function ( key ) {
    keys.push(key);
    var value = validateProp(key, propsOptions, propsData, vm);
    /* istanbul ignore else */
    if (true) {
      var hyphenatedKey = hyphenate(key);
      if (isReservedAttribute(hyphenatedKey) ||
          config.isReservedAttr(hyphenatedKey)) {
        warn(
          ("\"" + hyphenatedKey + "\" is a reserved attribute and cannot be used as component prop."),
          vm
        );
      }
      defineReactive$$1(props, key, value, function () {
        if (!isRoot && !isUpdatingChildComponent) {
          {
            if(vm.mpHost === 'mp-baidu' || vm.mpHost === 'mp-kuaishou' || vm.mpHost === 'mp-xhs'){//百度、快手、小红书 observer 在 setData callback 之后触发，直接忽略该 warn
                return
            }
            //fixed by xxxxxx __next_tick_pending,uni://form-field 时不告警
            if(
                key === 'value' && 
                Array.isArray(vm.$options.behaviors) &&
                vm.$options.behaviors.indexOf('uni://form-field') !== -1
              ){
              return
            }
            if(vm._getFormData){
              return
            }
            var $parent = vm.$parent;
            while($parent){
              if($parent.__next_tick_pending){
                return  
              }
              $parent = $parent.$parent;
            }
          }
          warn(
            "Avoid mutating a prop directly since the value will be " +
            "overwritten whenever the parent component re-renders. " +
            "Instead, use a data or computed property based on the prop's " +
            "value. Prop being mutated: \"" + key + "\"",
            vm
          );
        }
      });
    } else {}
    // static props are already proxied on the component's prototype
    // during Vue.extend(). We only need to proxy props defined at
    // instantiation here.
    if (!(key in vm)) {
      proxy(vm, "_props", key);
    }
  };

  for (var key in propsOptions) loop( key );
  toggleObserving(true);
}

function initData (vm) {
  var data = vm.$options.data;
  data = vm._data = typeof data === 'function'
    ? getData(data, vm)
    : data || {};
  if (!isPlainObject(data)) {
    data = {};
     true && warn(
      'data functions should return an object:\n' +
      'https://vuejs.org/v2/guide/components.html#data-Must-Be-a-Function',
      vm
    );
  }
  // proxy data on instance
  var keys = Object.keys(data);
  var props = vm.$options.props;
  var methods = vm.$options.methods;
  var i = keys.length;
  while (i--) {
    var key = keys[i];
    if (true) {
      if (methods && hasOwn(methods, key)) {
        warn(
          ("Method \"" + key + "\" has already been defined as a data property."),
          vm
        );
      }
    }
    if (props && hasOwn(props, key)) {
       true && warn(
        "The data property \"" + key + "\" is already declared as a prop. " +
        "Use prop default value instead.",
        vm
      );
    } else if (!isReserved(key)) {
      proxy(vm, "_data", key);
    }
  }
  // observe data
  observe(data, true /* asRootData */);
}

function getData (data, vm) {
  // #7573 disable dep collection when invoking data getters
  pushTarget();
  try {
    return data.call(vm, vm)
  } catch (e) {
    handleError(e, vm, "data()");
    return {}
  } finally {
    popTarget();
  }
}

var computedWatcherOptions = { lazy: true };

function initComputed (vm, computed) {
  // $flow-disable-line
  var watchers = vm._computedWatchers = Object.create(null);
  // computed properties are just getters during SSR
  var isSSR = isServerRendering();

  for (var key in computed) {
    var userDef = computed[key];
    var getter = typeof userDef === 'function' ? userDef : userDef.get;
    if ( true && getter == null) {
      warn(
        ("Getter is missing for computed property \"" + key + "\"."),
        vm
      );
    }

    if (!isSSR) {
      // create internal watcher for the computed property.
      watchers[key] = new Watcher(
        vm,
        getter || noop,
        noop,
        computedWatcherOptions
      );
    }

    // component-defined computed properties are already defined on the
    // component prototype. We only need to define computed properties defined
    // at instantiation here.
    if (!(key in vm)) {
      defineComputed(vm, key, userDef);
    } else if (true) {
      if (key in vm.$data) {
        warn(("The computed property \"" + key + "\" is already defined in data."), vm);
      } else if (vm.$options.props && key in vm.$options.props) {
        warn(("The computed property \"" + key + "\" is already defined as a prop."), vm);
      }
    }
  }
}

function defineComputed (
  target,
  key,
  userDef
) {
  var shouldCache = !isServerRendering();
  if (typeof userDef === 'function') {
    sharedPropertyDefinition.get = shouldCache
      ? createComputedGetter(key)
      : createGetterInvoker(userDef);
    sharedPropertyDefinition.set = noop;
  } else {
    sharedPropertyDefinition.get = userDef.get
      ? shouldCache && userDef.cache !== false
        ? createComputedGetter(key)
        : createGetterInvoker(userDef.get)
      : noop;
    sharedPropertyDefinition.set = userDef.set || noop;
  }
  if ( true &&
      sharedPropertyDefinition.set === noop) {
    sharedPropertyDefinition.set = function () {
      warn(
        ("Computed property \"" + key + "\" was assigned to but it has no setter."),
        this
      );
    };
  }
  Object.defineProperty(target, key, sharedPropertyDefinition);
}

function createComputedGetter (key) {
  return function computedGetter () {
    var watcher = this._computedWatchers && this._computedWatchers[key];
    if (watcher) {
      if (watcher.dirty) {
        watcher.evaluate();
      }
      if (Dep.SharedObject.target) {// fixed by xxxxxx
        watcher.depend();
      }
      return watcher.value
    }
  }
}

function createGetterInvoker(fn) {
  return function computedGetter () {
    return fn.call(this, this)
  }
}

function initMethods (vm, methods) {
  var props = vm.$options.props;
  for (var key in methods) {
    if (true) {
      if (typeof methods[key] !== 'function') {
        warn(
          "Method \"" + key + "\" has type \"" + (typeof methods[key]) + "\" in the component definition. " +
          "Did you reference the function correctly?",
          vm
        );
      }
      if (props && hasOwn(props, key)) {
        warn(
          ("Method \"" + key + "\" has already been defined as a prop."),
          vm
        );
      }
      if ((key in vm) && isReserved(key)) {
        warn(
          "Method \"" + key + "\" conflicts with an existing Vue instance method. " +
          "Avoid defining component methods that start with _ or $."
        );
      }
    }
    vm[key] = typeof methods[key] !== 'function' ? noop : bind(methods[key], vm);
  }
}

function initWatch (vm, watch) {
  for (var key in watch) {
    var handler = watch[key];
    if (Array.isArray(handler)) {
      for (var i = 0; i < handler.length; i++) {
        createWatcher(vm, key, handler[i]);
      }
    } else {
      createWatcher(vm, key, handler);
    }
  }
}

function createWatcher (
  vm,
  expOrFn,
  handler,
  options
) {
  if (isPlainObject(handler)) {
    options = handler;
    handler = handler.handler;
  }
  if (typeof handler === 'string') {
    handler = vm[handler];
  }
  return vm.$watch(expOrFn, handler, options)
}

function stateMixin (Vue) {
  // flow somehow has problems with directly declared definition object
  // when using Object.defineProperty, so we have to procedurally build up
  // the object here.
  var dataDef = {};
  dataDef.get = function () { return this._data };
  var propsDef = {};
  propsDef.get = function () { return this._props };
  if (true) {
    dataDef.set = function () {
      warn(
        'Avoid replacing instance root $data. ' +
        'Use nested data properties instead.',
        this
      );
    };
    propsDef.set = function () {
      warn("$props is readonly.", this);
    };
  }
  Object.defineProperty(Vue.prototype, '$data', dataDef);
  Object.defineProperty(Vue.prototype, '$props', propsDef);

  Vue.prototype.$set = set;
  Vue.prototype.$delete = del;

  Vue.prototype.$watch = function (
    expOrFn,
    cb,
    options
  ) {
    var vm = this;
    if (isPlainObject(cb)) {
      return createWatcher(vm, expOrFn, cb, options)
    }
    options = options || {};
    options.user = true;
    var watcher = new Watcher(vm, expOrFn, cb, options);
    if (options.immediate) {
      try {
        cb.call(vm, watcher.value);
      } catch (error) {
        handleError(error, vm, ("callback for immediate watcher \"" + (watcher.expression) + "\""));
      }
    }
    return function unwatchFn () {
      watcher.teardown();
    }
  };
}

/*  */

var uid$3 = 0;

function initMixin (Vue) {
  Vue.prototype._init = function (options) {
    var vm = this;
    // a uid
    vm._uid = uid$3++;

    var startTag, endTag;
    /* istanbul ignore if */
    if ( true && config.performance && mark) {
      startTag = "vue-perf-start:" + (vm._uid);
      endTag = "vue-perf-end:" + (vm._uid);
      mark(startTag);
    }

    // a flag to avoid this being observed
    vm._isVue = true;
    // merge options
    if (options && options._isComponent) {
      // optimize internal component instantiation
      // since dynamic options merging is pretty slow, and none of the
      // internal component options needs special treatment.
      initInternalComponent(vm, options);
    } else {
      vm.$options = mergeOptions(
        resolveConstructorOptions(vm.constructor),
        options || {},
        vm
      );
    }
    /* istanbul ignore else */
    if (true) {
      initProxy(vm);
    } else {}
    // expose real self
    vm._self = vm;
    initLifecycle(vm);
    initEvents(vm);
    initRender(vm);
    callHook(vm, 'beforeCreate');
    !vm._$fallback && initInjections(vm); // resolve injections before data/props  
    initState(vm);
    !vm._$fallback && initProvide(vm); // resolve provide after data/props
    !vm._$fallback && callHook(vm, 'created');      

    /* istanbul ignore if */
    if ( true && config.performance && mark) {
      vm._name = formatComponentName(vm, false);
      mark(endTag);
      measure(("vue " + (vm._name) + " init"), startTag, endTag);
    }

    if (vm.$options.el) {
      vm.$mount(vm.$options.el);
    }
  };
}

function initInternalComponent (vm, options) {
  var opts = vm.$options = Object.create(vm.constructor.options);
  // doing this because it's faster than dynamic enumeration.
  var parentVnode = options._parentVnode;
  opts.parent = options.parent;
  opts._parentVnode = parentVnode;

  var vnodeComponentOptions = parentVnode.componentOptions;
  opts.propsData = vnodeComponentOptions.propsData;
  opts._parentListeners = vnodeComponentOptions.listeners;
  opts._renderChildren = vnodeComponentOptions.children;
  opts._componentTag = vnodeComponentOptions.tag;

  if (options.render) {
    opts.render = options.render;
    opts.staticRenderFns = options.staticRenderFns;
  }
}

function resolveConstructorOptions (Ctor) {
  var options = Ctor.options;
  if (Ctor.super) {
    var superOptions = resolveConstructorOptions(Ctor.super);
    var cachedSuperOptions = Ctor.superOptions;
    if (superOptions !== cachedSuperOptions) {
      // super option changed,
      // need to resolve new options.
      Ctor.superOptions = superOptions;
      // check if there are any late-modified/attached options (#4976)
      var modifiedOptions = resolveModifiedOptions(Ctor);
      // update base extend options
      if (modifiedOptions) {
        extend(Ctor.extendOptions, modifiedOptions);
      }
      options = Ctor.options = mergeOptions(superOptions, Ctor.extendOptions);
      if (options.name) {
        options.components[options.name] = Ctor;
      }
    }
  }
  return options
}

function resolveModifiedOptions (Ctor) {
  var modified;
  var latest = Ctor.options;
  var sealed = Ctor.sealedOptions;
  for (var key in latest) {
    if (latest[key] !== sealed[key]) {
      if (!modified) { modified = {}; }
      modified[key] = latest[key];
    }
  }
  return modified
}

function Vue (options) {
  if ( true &&
    !(this instanceof Vue)
  ) {
    warn('Vue is a constructor and should be called with the `new` keyword');
  }
  this._init(options);
}

initMixin(Vue);
stateMixin(Vue);
eventsMixin(Vue);
lifecycleMixin(Vue);
renderMixin(Vue);

/*  */

function initUse (Vue) {
  Vue.use = function (plugin) {
    var installedPlugins = (this._installedPlugins || (this._installedPlugins = []));
    if (installedPlugins.indexOf(plugin) > -1) {
      return this
    }

    // additional parameters
    var args = toArray(arguments, 1);
    args.unshift(this);
    if (typeof plugin.install === 'function') {
      plugin.install.apply(plugin, args);
    } else if (typeof plugin === 'function') {
      plugin.apply(null, args);
    }
    installedPlugins.push(plugin);
    return this
  };
}

/*  */

function initMixin$1 (Vue) {
  Vue.mixin = function (mixin) {
    this.options = mergeOptions(this.options, mixin);
    return this
  };
}

/*  */

function initExtend (Vue) {
  /**
   * Each instance constructor, including Vue, has a unique
   * cid. This enables us to create wrapped "child
   * constructors" for prototypal inheritance and cache them.
   */
  Vue.cid = 0;
  var cid = 1;

  /**
   * Class inheritance
   */
  Vue.extend = function (extendOptions) {
    extendOptions = extendOptions || {};
    var Super = this;
    var SuperId = Super.cid;
    var cachedCtors = extendOptions._Ctor || (extendOptions._Ctor = {});
    if (cachedCtors[SuperId]) {
      return cachedCtors[SuperId]
    }

    var name = extendOptions.name || Super.options.name;
    if ( true && name) {
      validateComponentName(name);
    }

    var Sub = function VueComponent (options) {
      this._init(options);
    };
    Sub.prototype = Object.create(Super.prototype);
    Sub.prototype.constructor = Sub;
    Sub.cid = cid++;
    Sub.options = mergeOptions(
      Super.options,
      extendOptions
    );
    Sub['super'] = Super;

    // For props and computed properties, we define the proxy getters on
    // the Vue instances at extension time, on the extended prototype. This
    // avoids Object.defineProperty calls for each instance created.
    if (Sub.options.props) {
      initProps$1(Sub);
    }
    if (Sub.options.computed) {
      initComputed$1(Sub);
    }

    // allow further extension/mixin/plugin usage
    Sub.extend = Super.extend;
    Sub.mixin = Super.mixin;
    Sub.use = Super.use;

    // create asset registers, so extended classes
    // can have their private assets too.
    ASSET_TYPES.forEach(function (type) {
      Sub[type] = Super[type];
    });
    // enable recursive self-lookup
    if (name) {
      Sub.options.components[name] = Sub;
    }

    // keep a reference to the super options at extension time.
    // later at instantiation we can check if Super's options have
    // been updated.
    Sub.superOptions = Super.options;
    Sub.extendOptions = extendOptions;
    Sub.sealedOptions = extend({}, Sub.options);

    // cache constructor
    cachedCtors[SuperId] = Sub;
    return Sub
  };
}

function initProps$1 (Comp) {
  var props = Comp.options.props;
  for (var key in props) {
    proxy(Comp.prototype, "_props", key);
  }
}

function initComputed$1 (Comp) {
  var computed = Comp.options.computed;
  for (var key in computed) {
    defineComputed(Comp.prototype, key, computed[key]);
  }
}

/*  */

function initAssetRegisters (Vue) {
  /**
   * Create asset registration methods.
   */
  ASSET_TYPES.forEach(function (type) {
    Vue[type] = function (
      id,
      definition
    ) {
      if (!definition) {
        return this.options[type + 's'][id]
      } else {
        /* istanbul ignore if */
        if ( true && type === 'component') {
          validateComponentName(id);
        }
        if (type === 'component' && isPlainObject(definition)) {
          definition.name = definition.name || id;
          definition = this.options._base.extend(definition);
        }
        if (type === 'directive' && typeof definition === 'function') {
          definition = { bind: definition, update: definition };
        }
        this.options[type + 's'][id] = definition;
        return definition
      }
    };
  });
}

/*  */



function getComponentName (opts) {
  return opts && (opts.Ctor.options.name || opts.tag)
}

function matches (pattern, name) {
  if (Array.isArray(pattern)) {
    return pattern.indexOf(name) > -1
  } else if (typeof pattern === 'string') {
    return pattern.split(',').indexOf(name) > -1
  } else if (isRegExp(pattern)) {
    return pattern.test(name)
  }
  /* istanbul ignore next */
  return false
}

function pruneCache (keepAliveInstance, filter) {
  var cache = keepAliveInstance.cache;
  var keys = keepAliveInstance.keys;
  var _vnode = keepAliveInstance._vnode;
  for (var key in cache) {
    var cachedNode = cache[key];
    if (cachedNode) {
      var name = getComponentName(cachedNode.componentOptions);
      if (name && !filter(name)) {
        pruneCacheEntry(cache, key, keys, _vnode);
      }
    }
  }
}

function pruneCacheEntry (
  cache,
  key,
  keys,
  current
) {
  var cached$$1 = cache[key];
  if (cached$$1 && (!current || cached$$1.tag !== current.tag)) {
    cached$$1.componentInstance.$destroy();
  }
  cache[key] = null;
  remove(keys, key);
}

var patternTypes = [String, RegExp, Array];

var KeepAlive = {
  name: 'keep-alive',
  abstract: true,

  props: {
    include: patternTypes,
    exclude: patternTypes,
    max: [String, Number]
  },

  created: function created () {
    this.cache = Object.create(null);
    this.keys = [];
  },

  destroyed: function destroyed () {
    for (var key in this.cache) {
      pruneCacheEntry(this.cache, key, this.keys);
    }
  },

  mounted: function mounted () {
    var this$1 = this;

    this.$watch('include', function (val) {
      pruneCache(this$1, function (name) { return matches(val, name); });
    });
    this.$watch('exclude', function (val) {
      pruneCache(this$1, function (name) { return !matches(val, name); });
    });
  },

  render: function render () {
    var slot = this.$slots.default;
    var vnode = getFirstComponentChild(slot);
    var componentOptions = vnode && vnode.componentOptions;
    if (componentOptions) {
      // check pattern
      var name = getComponentName(componentOptions);
      var ref = this;
      var include = ref.include;
      var exclude = ref.exclude;
      if (
        // not included
        (include && (!name || !matches(include, name))) ||
        // excluded
        (exclude && name && matches(exclude, name))
      ) {
        return vnode
      }

      var ref$1 = this;
      var cache = ref$1.cache;
      var keys = ref$1.keys;
      var key = vnode.key == null
        // same constructor may get registered as different local components
        // so cid alone is not enough (#3269)
        ? componentOptions.Ctor.cid + (componentOptions.tag ? ("::" + (componentOptions.tag)) : '')
        : vnode.key;
      if (cache[key]) {
        vnode.componentInstance = cache[key].componentInstance;
        // make current key freshest
        remove(keys, key);
        keys.push(key);
      } else {
        cache[key] = vnode;
        keys.push(key);
        // prune oldest entry
        if (this.max && keys.length > parseInt(this.max)) {
          pruneCacheEntry(cache, keys[0], keys, this._vnode);
        }
      }

      vnode.data.keepAlive = true;
    }
    return vnode || (slot && slot[0])
  }
};

var builtInComponents = {
  KeepAlive: KeepAlive
};

/*  */

function initGlobalAPI (Vue) {
  // config
  var configDef = {};
  configDef.get = function () { return config; };
  if (true) {
    configDef.set = function () {
      warn(
        'Do not replace the Vue.config object, set individual fields instead.'
      );
    };
  }
  Object.defineProperty(Vue, 'config', configDef);

  // exposed util methods.
  // NOTE: these are not considered part of the public API - avoid relying on
  // them unless you are aware of the risk.
  Vue.util = {
    warn: warn,
    extend: extend,
    mergeOptions: mergeOptions,
    defineReactive: defineReactive$$1
  };

  Vue.set = set;
  Vue.delete = del;
  Vue.nextTick = nextTick;

  // 2.6 explicit observable API
  Vue.observable = function (obj) {
    observe(obj);
    return obj
  };

  Vue.options = Object.create(null);
  ASSET_TYPES.forEach(function (type) {
    Vue.options[type + 's'] = Object.create(null);
  });

  // this is used to identify the "base" constructor to extend all plain-object
  // components with in Weex's multi-instance scenarios.
  Vue.options._base = Vue;

  extend(Vue.options.components, builtInComponents);

  initUse(Vue);
  initMixin$1(Vue);
  initExtend(Vue);
  initAssetRegisters(Vue);
}

initGlobalAPI(Vue);

Object.defineProperty(Vue.prototype, '$isServer', {
  get: isServerRendering
});

Object.defineProperty(Vue.prototype, '$ssrContext', {
  get: function get () {
    /* istanbul ignore next */
    return this.$vnode && this.$vnode.ssrContext
  }
});

// expose FunctionalRenderContext for ssr runtime helper installation
Object.defineProperty(Vue, 'FunctionalRenderContext', {
  value: FunctionalRenderContext
});

Vue.version = '2.6.11';

/**
 * https://raw.githubusercontent.com/Tencent/westore/master/packages/westore/utils/diff.js
 */
var ARRAYTYPE = '[object Array]';
var OBJECTTYPE = '[object Object]';
// const FUNCTIONTYPE = '[object Function]'

function diff(current, pre) {
    var result = {};
    syncKeys(current, pre);
    _diff(current, pre, '', result);
    return result
}

function syncKeys(current, pre) {
    if (current === pre) { return }
    var rootCurrentType = type(current);
    var rootPreType = type(pre);
    if (rootCurrentType == OBJECTTYPE && rootPreType == OBJECTTYPE) {
        if(Object.keys(current).length >= Object.keys(pre).length){
            for (var key in pre) {
                var currentValue = current[key];
                if (currentValue === undefined) {
                    current[key] = null;
                } else {
                    syncKeys(currentValue, pre[key]);
                }
            }
        }
    } else if (rootCurrentType == ARRAYTYPE && rootPreType == ARRAYTYPE) {
        if (current.length >= pre.length) {
            pre.forEach(function (item, index) {
                syncKeys(current[index], item);
            });
        }
    }
}

function _diff(current, pre, path, result) {
    if (current === pre) { return }
    var rootCurrentType = type(current);
    var rootPreType = type(pre);
    if (rootCurrentType == OBJECTTYPE) {
        if (rootPreType != OBJECTTYPE || Object.keys(current).length < Object.keys(pre).length) {
            setResult(result, path, current);
        } else {
            var loop = function ( key ) {
                var currentValue = current[key];
                var preValue = pre[key];
                var currentType = type(currentValue);
                var preType = type(preValue);
                if (currentType != ARRAYTYPE && currentType != OBJECTTYPE) {
                    if (currentValue !== pre[key]) {
                        setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                    }
                } else if (currentType == ARRAYTYPE) {
                    if (preType != ARRAYTYPE) {
                        setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                    } else {
                        if (currentValue.length < preValue.length) {
                            setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                        } else {
                            currentValue.forEach(function (item, index) {
                                _diff(item, preValue[index], (path == '' ? '' : path + ".") + key + '[' + index + ']', result);
                            });
                        }
                    }
                } else if (currentType == OBJECTTYPE) {
                    if (preType != OBJECTTYPE || Object.keys(currentValue).length < Object.keys(preValue).length) {
                        setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                    } else {
                        for (var subKey in currentValue) {
                            _diff(currentValue[subKey], preValue[subKey], (path == '' ? '' : path + ".") + key + '.' + subKey, result);
                        }
                    }
                }
            };

            for (var key in current) loop( key );
        }
    } else if (rootCurrentType == ARRAYTYPE) {
        if (rootPreType != ARRAYTYPE) {
            setResult(result, path, current);
        } else {
            if (current.length < pre.length) {
                setResult(result, path, current);
            } else {
                current.forEach(function (item, index) {
                    _diff(item, pre[index], path + '[' + index + ']', result);
                });
            }
        }
    } else {
        setResult(result, path, current);
    }
}

function setResult(result, k, v) {
    // if (type(v) != FUNCTIONTYPE) {
        result[k] = v;
    // }
}

function type(obj) {
    return Object.prototype.toString.call(obj)
}

/*  */

function flushCallbacks$1(vm) {
    if (vm.__next_tick_callbacks && vm.__next_tick_callbacks.length) {
        if (Object({"NODE_ENV":"development","VUE_APP_NAME":"wyy","VUE_APP_PLATFORM":"mp-weixin","BASE_URL":"/"}).VUE_APP_DEBUG) {
            var mpInstance = vm.$scope;
            console.log('[' + (+new Date) + '][' + (mpInstance.is || mpInstance.route) + '][' + vm._uid +
                ']:flushCallbacks[' + vm.__next_tick_callbacks.length + ']');
        }
        var copies = vm.__next_tick_callbacks.slice(0);
        vm.__next_tick_callbacks.length = 0;
        for (var i = 0; i < copies.length; i++) {
            copies[i]();
        }
    }
}

function hasRenderWatcher(vm) {
    return queue.find(function (watcher) { return vm._watcher === watcher; })
}

function nextTick$1(vm, cb) {
    //1.nextTick 之前 已 setData 且 setData 还未回调完成
    //2.nextTick 之前存在 render watcher
    if (!vm.__next_tick_pending && !hasRenderWatcher(vm)) {
        if(Object({"NODE_ENV":"development","VUE_APP_NAME":"wyy","VUE_APP_PLATFORM":"mp-weixin","BASE_URL":"/"}).VUE_APP_DEBUG){
            var mpInstance = vm.$scope;
            console.log('[' + (+new Date) + '][' + (mpInstance.is || mpInstance.route) + '][' + vm._uid +
                ']:nextVueTick');
        }
        return nextTick(cb, vm)
    }else{
        if(Object({"NODE_ENV":"development","VUE_APP_NAME":"wyy","VUE_APP_PLATFORM":"mp-weixin","BASE_URL":"/"}).VUE_APP_DEBUG){
            var mpInstance$1 = vm.$scope;
            console.log('[' + (+new Date) + '][' + (mpInstance$1.is || mpInstance$1.route) + '][' + vm._uid +
                ']:nextMPTick');
        }
    }
    var _resolve;
    if (!vm.__next_tick_callbacks) {
        vm.__next_tick_callbacks = [];
    }
    vm.__next_tick_callbacks.push(function () {
        if (cb) {
            try {
                cb.call(vm);
            } catch (e) {
                handleError(e, vm, 'nextTick');
            }
        } else if (_resolve) {
            _resolve(vm);
        }
    });
    // $flow-disable-line
    if (!cb && typeof Promise !== 'undefined') {
        return new Promise(function (resolve) {
            _resolve = resolve;
        })
    }
}

/*  */

function cloneWithData(vm) {
  // 确保当前 vm 所有数据被同步
  var ret = Object.create(null);
  var dataKeys = [].concat(
    Object.keys(vm._data || {}),
    Object.keys(vm._computedWatchers || {}));

  dataKeys.reduce(function(ret, key) {
    ret[key] = vm[key];
    return ret
  }, ret);

  // vue-composition-api
  var compositionApiState = vm.__composition_api_state__ || vm.__secret_vfa_state__;
  var rawBindings = compositionApiState && compositionApiState.rawBindings;
  if (rawBindings) {
    Object.keys(rawBindings).forEach(function (key) {
      ret[key] = vm[key];
    });
  }

  //TODO 需要把无用数据处理掉，比如 list=>l0 则 list 需要移除，否则多传输一份数据
  Object.assign(ret, vm.$mp.data || {});
  if (
    Array.isArray(vm.$options.behaviors) &&
    vm.$options.behaviors.indexOf('uni://form-field') !== -1
  ) { //form-field
    ret['name'] = vm.name;
    ret['value'] = vm.value;
  }

  return JSON.parse(JSON.stringify(ret))
}

var patch = function(oldVnode, vnode) {
  var this$1 = this;

  if (vnode === null) { //destroy
    return
  }
  if (this.mpType === 'page' || this.mpType === 'component') {
    var mpInstance = this.$scope;
    var data = Object.create(null);
    try {
      data = cloneWithData(this);
    } catch (err) {
      console.error(err);
    }
    data.__webviewId__ = mpInstance.data.__webviewId__;
    var mpData = Object.create(null);
    Object.keys(data).forEach(function (key) { //仅同步 data 中有的数据
      mpData[key] = mpInstance.data[key];
    });
    var diffData = this.$shouldDiffData === false ? data : diff(data, mpData);
    if (Object.keys(diffData).length) {
      if (Object({"NODE_ENV":"development","VUE_APP_NAME":"wyy","VUE_APP_PLATFORM":"mp-weixin","BASE_URL":"/"}).VUE_APP_DEBUG) {
        console.log('[' + (+new Date) + '][' + (mpInstance.is || mpInstance.route) + '][' + this._uid +
          ']差量更新',
          JSON.stringify(diffData));
      }
      this.__next_tick_pending = true;
      mpInstance.setData(diffData, function () {
        this$1.__next_tick_pending = false;
        flushCallbacks$1(this$1);
      });
    } else {
      flushCallbacks$1(this);
    }
  }
};

/*  */

function createEmptyRender() {

}

function mountComponent$1(
  vm,
  el,
  hydrating
) {
  if (!vm.mpType) {//main.js 中的 new Vue
    return vm
  }
  if (vm.mpType === 'app') {
    vm.$options.render = createEmptyRender;
  }
  if (!vm.$options.render) {
    vm.$options.render = createEmptyRender;
    if (true) {
      /* istanbul ignore if */
      if ((vm.$options.template && vm.$options.template.charAt(0) !== '#') ||
        vm.$options.el || el) {
        warn(
          'You are using the runtime-only build of Vue where the template ' +
          'compiler is not available. Either pre-compile the templates into ' +
          'render functions, or use the compiler-included build.',
          vm
        );
      } else {
        warn(
          'Failed to mount component: template or render function not defined.',
          vm
        );
      }
    }
  }
  
  !vm._$fallback && callHook(vm, 'beforeMount');

  var updateComponent = function () {
    vm._update(vm._render(), hydrating);
  };

  // we set this to vm._watcher inside the watcher's constructor
  // since the watcher's initial patch may call $forceUpdate (e.g. inside child
  // component's mounted hook), which relies on vm._watcher being already defined
  new Watcher(vm, updateComponent, noop, {
    before: function before() {
      if (vm._isMounted && !vm._isDestroyed) {
        callHook(vm, 'beforeUpdate');
      }
    }
  }, true /* isRenderWatcher */);
  hydrating = false;
  return vm
}

/*  */

function renderClass (
  staticClass,
  dynamicClass
) {
  if (isDef(staticClass) || isDef(dynamicClass)) {
    return concat(staticClass, stringifyClass(dynamicClass))
  }
  /* istanbul ignore next */
  return ''
}

function concat (a, b) {
  return a ? b ? (a + ' ' + b) : a : (b || '')
}

function stringifyClass (value) {
  if (Array.isArray(value)) {
    return stringifyArray(value)
  }
  if (isObject(value)) {
    return stringifyObject(value)
  }
  if (typeof value === 'string') {
    return value
  }
  /* istanbul ignore next */
  return ''
}

function stringifyArray (value) {
  var res = '';
  var stringified;
  for (var i = 0, l = value.length; i < l; i++) {
    if (isDef(stringified = stringifyClass(value[i])) && stringified !== '') {
      if (res) { res += ' '; }
      res += stringified;
    }
  }
  return res
}

function stringifyObject (value) {
  var res = '';
  for (var key in value) {
    if (value[key]) {
      if (res) { res += ' '; }
      res += key;
    }
  }
  return res
}

/*  */

var parseStyleText = cached(function (cssText) {
  var res = {};
  var listDelimiter = /;(?![^(]*\))/g;
  var propertyDelimiter = /:(.+)/;
  cssText.split(listDelimiter).forEach(function (item) {
    if (item) {
      var tmp = item.split(propertyDelimiter);
      tmp.length > 1 && (res[tmp[0].trim()] = tmp[1].trim());
    }
  });
  return res
});

// normalize possible array / string values into Object
function normalizeStyleBinding (bindingStyle) {
  if (Array.isArray(bindingStyle)) {
    return toObject(bindingStyle)
  }
  if (typeof bindingStyle === 'string') {
    return parseStyleText(bindingStyle)
  }
  return bindingStyle
}

/*  */

var MP_METHODS = ['createSelectorQuery', 'createIntersectionObserver', 'selectAllComponents', 'selectComponent'];

function getTarget(obj, path) {
  var parts = path.split('.');
  var key = parts[0];
  if (key.indexOf('__$n') === 0) { //number index
    key = parseInt(key.replace('__$n', ''));
  }
  if (parts.length === 1) {
    return obj[key]
  }
  return getTarget(obj[key], parts.slice(1).join('.'))
}

function internalMixin(Vue) {

  Vue.config.errorHandler = function(err, vm, info) {
    Vue.util.warn(("Error in " + info + ": \"" + (err.toString()) + "\""), vm);
    console.error(err);
    /* eslint-disable no-undef */
    var app = typeof getApp === 'function' && getApp();
    if (app && app.onError) {
      app.onError(err);
    }
  };

  var oldEmit = Vue.prototype.$emit;

  Vue.prototype.$emit = function(event) {
    if (this.$scope && event) {
      (this.$scope['_triggerEvent'] || this.$scope['triggerEvent']).call(this.$scope, event, {
        __args__: toArray(arguments, 1)
      });
    }
    return oldEmit.apply(this, arguments)
  };

  Vue.prototype.$nextTick = function(fn) {
    return nextTick$1(this, fn)
  };

  MP_METHODS.forEach(function (method) {
    Vue.prototype[method] = function(args) {
      if (this.$scope && this.$scope[method]) {
        return this.$scope[method](args)
      }
      // mp-alipay
      if (typeof my === 'undefined') {
        return
      }
      if (method === 'createSelectorQuery') {
        /* eslint-disable no-undef */
        return my.createSelectorQuery(args)
      } else if (method === 'createIntersectionObserver') {
        /* eslint-disable no-undef */
        return my.createIntersectionObserver(args)
      }
      // TODO mp-alipay 暂不支持 selectAllComponents,selectComponent
    };
  });

  Vue.prototype.__init_provide = initProvide;

  Vue.prototype.__init_injections = initInjections;

  Vue.prototype.__call_hook = function(hook, args) {
    var vm = this;
    // #7573 disable dep collection when invoking lifecycle hooks
    pushTarget();
    var handlers = vm.$options[hook];
    var info = hook + " hook";
    var ret;
    if (handlers) {
      for (var i = 0, j = handlers.length; i < j; i++) {
        ret = invokeWithErrorHandling(handlers[i], vm, args ? [args] : null, vm, info);
      }
    }
    if (vm._hasHookEvent) {
      vm.$emit('hook:' + hook, args);
    }
    popTarget();
    return ret
  };

  Vue.prototype.__set_model = function(target, key, value, modifiers) {
    if (Array.isArray(modifiers)) {
      if (modifiers.indexOf('trim') !== -1) {
        value = value.trim();
      }
      if (modifiers.indexOf('number') !== -1) {
        value = this._n(value);
      }
    }
    if (!target) {
      target = this;
    }
    // 解决动态属性添加
    Vue.set(target, key, value);
  };

  Vue.prototype.__set_sync = function(target, key, value) {
    if (!target) {
      target = this;
    }
    // 解决动态属性添加
    Vue.set(target, key, value);
  };

  Vue.prototype.__get_orig = function(item) {
    if (isPlainObject(item)) {
      return item['$orig'] || item
    }
    return item
  };

  Vue.prototype.__get_value = function(dataPath, target) {
    return getTarget(target || this, dataPath)
  };


  Vue.prototype.__get_class = function(dynamicClass, staticClass) {
    return renderClass(staticClass, dynamicClass)
  };

  Vue.prototype.__get_style = function(dynamicStyle, staticStyle) {
    if (!dynamicStyle && !staticStyle) {
      return ''
    }
    var dynamicStyleObj = normalizeStyleBinding(dynamicStyle);
    var styleObj = staticStyle ? extend(staticStyle, dynamicStyleObj) : dynamicStyleObj;
    return Object.keys(styleObj).map(function (name) { return ((hyphenate(name)) + ":" + (styleObj[name])); }).join(';')
  };

  Vue.prototype.__map = function(val, iteratee) {
    //TODO 暂不考虑 string
    var ret, i, l, keys, key;
    if (Array.isArray(val)) {
      ret = new Array(val.length);
      for (i = 0, l = val.length; i < l; i++) {
        ret[i] = iteratee(val[i], i);
      }
      return ret
    } else if (isObject(val)) {
      keys = Object.keys(val);
      ret = Object.create(null);
      for (i = 0, l = keys.length; i < l; i++) {
        key = keys[i];
        ret[key] = iteratee(val[key], key, i);
      }
      return ret
    } else if (typeof val === 'number') {
      ret = new Array(val);
      for (i = 0, l = val; i < l; i++) {
        // 第一个参数暂时仍和小程序一致
        ret[i] = iteratee(i, i);
      }
      return ret
    }
    return []
  };

}

/*  */

var LIFECYCLE_HOOKS$1 = [
    //App
    'onLaunch',
    'onShow',
    'onHide',
    'onUniNViewMessage',
    'onPageNotFound',
    'onThemeChange',
    'onError',
    'onUnhandledRejection',
    //Page
    'onInit',
    'onLoad',
    // 'onShow',
    'onReady',
    // 'onHide',
    'onUnload',
    'onPullDownRefresh',
    'onReachBottom',
    'onTabItemTap',
    'onAddToFavorites',
    'onShareTimeline',
    'onShareAppMessage',
    'onResize',
    'onPageScroll',
    'onNavigationBarButtonTap',
    'onBackPress',
    'onNavigationBarSearchInputChanged',
    'onNavigationBarSearchInputConfirmed',
    'onNavigationBarSearchInputClicked',
    //Component
    // 'onReady', // 兼容旧版本，应该移除该事件
    'onPageShow',
    'onPageHide',
    'onPageResize'
];
function lifecycleMixin$1(Vue) {

    //fixed vue-class-component
    var oldExtend = Vue.extend;
    Vue.extend = function(extendOptions) {
        extendOptions = extendOptions || {};

        var methods = extendOptions.methods;
        if (methods) {
            Object.keys(methods).forEach(function (methodName) {
                if (LIFECYCLE_HOOKS$1.indexOf(methodName)!==-1) {
                    extendOptions[methodName] = methods[methodName];
                    delete methods[methodName];
                }
            });
        }

        return oldExtend.call(this, extendOptions)
    };

    var strategies = Vue.config.optionMergeStrategies;
    var mergeHook = strategies.created;
    LIFECYCLE_HOOKS$1.forEach(function (hook) {
        strategies[hook] = mergeHook;
    });

    Vue.prototype.__lifecycle_hooks__ = LIFECYCLE_HOOKS$1;
}

/*  */

// install platform patch function
Vue.prototype.__patch__ = patch;

// public mount method
Vue.prototype.$mount = function(
    el ,
    hydrating 
) {
    return mountComponent$1(this, el, hydrating)
};

lifecycleMixin$1(Vue);
internalMixin(Vue);

/*  */

/* harmony default export */ __webpack_exports__["default"] = (Vue);

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../webpack/buildin/global.js */ 2)))

/***/ }),

/***/ 5:
/*!*******************************************!*\
  !*** C:/Users/王佳豪/Desktop/wyy/pages.json ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ 58:
/*!************************************************!*\
  !*** C:/Users/王佳豪/Desktop/wyy/static/logo.png ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/static/logo.png";

/***/ }),

/***/ 61:
/*!**********************************************!*\
  !*** C:/Users/王佳豪/Desktop/wyy/common/api.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(uni) {Object.defineProperty(exports, "__esModule", { value: true });exports.toplist = toplist;var _config = __webpack_require__(/*! ./config */ 19);

function toplist() {
  uni.request({
    url: "".concat(_config.baseURL, "/toplist/detail"),
    method: "GET",
    data: {},
    success: function success(res) {
      console.log(res.data);
    } });

}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js */ 1)["default"]))

/***/ }),

/***/ 76:
/*!*********************************************!*\
  !*** C:/Users/王佳豪/Desktop/wyy/static/3.png ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPsAAAGZCAMAAAB457dxAAAC91BMVEUAAAAAAAD///8CAgIBAQEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADi4uIAAAAAAAD09PT+/v4qKir////+/v7+/v5TU1NjY2PW1tYXFxe2trYCAgIAAAD8/PwBAQGVlZU8PDxISEj////+/v7V1dX8/Pz+/v6IiIj////+/v78/PwMDAxwcHB/f3/z8/P29vb7+/v7+/sAAAD8/Pzy8vICAgL9/f34+Pj39/f////29vb+/v77+/v7+/vR0dEEBAT6+vr6+vr8/Pz9/f36+vrf39+YmJj9/f0pKSn09PT8/Pzw8PD09PT4+Pj19fXw8PDw8PDr6+v8/Pzu7u6wsLCnp6f5+fkAAABzc3P4+Pjw8PDx8fHt7e3////5+fn9/f3r6+vk5OTi4uLX19f9/f37+/vm5ua3t7f4+Pi4uLj8/PyhoaHx8fH9/f36+vrh4eH4+Pj5+fn9/f3w8PD09PT29vb4+Pj9/f3R0dFiYmKwsLDz8/P19fVXV1fm5ub////09PTl5eXv7+/8/Pzq6urp6en9/f35+fns7Oy4uLjU1NT9/f309PTV1dXw8PBISEiMjIyysrL19fXz8/Pp6enr6+v7+/vy8vLz8/P9/f3t7e37+/vQ0NDGxsbk5OT39/fCwsLe3t7o6OiBgYG8vLx1dXX5+fmbm5shISH////j4+Pj4+Px8fHa2trq6uq/v7/u7u7r6+v19fXNzc3Hx8f////CwsKLi4vk5ORERETc3Ny5ubnIyMipqalHR0ddXV1tbW0yMjJUVFT////s7Oz6+vr39/fv7+/y8vLm5ub09PTr6+v8/Pz9/f35+fn29vbj4+Pw8PDo6Oju7u7q6urd3d3l5eXx8fHp6enZ2dng4ODf39/i4uKzs7Pc3Nzb29tERES2trZKSkpGRkbW1ta6urq+vr7KysrS0tLOzs7GxsbDw8NDQ0Onp6eRkZGHh4dOTk6vr6+cnJx6enqdNwHaAAAAzHRSTlMAGv4DBSsJJwcOLR4pGwwiJBAXFRMl/SAYAv0eBPoFHBoIHAszQOk9EB8e+/cK9uQU8++xMBgWE/zz7jrZ10MyDuzr4NzX0Y84HBfCvLuiWFE1K/z8+vbcz8nCop1mYF82Kyb16+fRzsW8saqWkYuEbUdHQD0xH/v65uHg3dPLl31fQ0JAIiD0yLKsop6TiYR0cnJua2ZkUTszBvLu2s7Kwri1sKyJgX18enNiTUtGODcvC7y1p5+bmZWPhYRdV1JSPjz6iVdSTUo9KxKUELBKAAAYg0lEQVR42uTaz0sbQRQH8J0FEWoXvdQy7Lo/RZLY4CokUOrFUrWX3nvwEnrIUZCcKhUkUBDRiyB4LD0IgmApbWlBhNKf8Ew0akpdPOQ/qb+oeZN1Mx7f+Dns/ct783ZmdrVk3LK9IHUu8GyLa7cEt7yU66RN3/cZO32YacdNebcgP7eCXMZkLcxMLlA7PrdDBwVH8Z3QVjY9t/Npliid99RMb4U4eXz60NKUwwPHZxJ8J1Ct9FZoMkmmYqX3XJ9J811PUwbPZtiNZLLK9H2AokuFV2TR8yDNbiytRHieRdFlvVYhvHdtwxdKMzOlwrVtT3/g2Q6LM/OlOALnRopLMyyOY2u08Xxc8KkhQAYWN1mrHO2u5ymTibYWdGi1UGIiP0U6vJ1pWeRTgxBrcKqg1JLnOSYoz8K1hsoqdX0gdvz8MCQYXmWYGWhUWS7DpnVIpC8zzCV7rBHLvgFtbShSeC6UfbMP2hrcUaPwHi772gBIeLyFC09z1PMQpTDmQEqRIXmSo95yUPR5kISHvUOy6dGkM/xRkPQELRWf5LQLUdk3QNo6+aZHU94wZkHaEPlJb6ebs5ehVb16cFCtQyt0pk0TPMri5b4MoqO92rm9IxAtUV/wKb85+wQI/kS1S1EVBOP4JKuRk2+OvgaCSq1JBQRbaNhp1PBc4sv9sDn7oZ70infJDXo85qcBq9YQseunaWdHuzrjE2B7OPseYFO0d3Y4+2JLyyOHgH0gnj3TnP0tYA2cvQHYArq1o5c9se67atcdneI+A3as9HrHlzbrt2rO4+vpbUhq+l0QzNPOjo+wBV3czUdX0SNxR6+v0d7XCfv5ORBUTv4P+QoIitT3856JN3Yifb9xXvT9OoiWqV9To/M7K0GMSrVagRhl6ud3POiNcZA2yoiPOvG+bhWkraDsoUZQFi14owiSJhT4KmU5KPsOSPquwP08x03f9QakPGNISHG5i285o/ASJMwWVPgeJ1xbGV2lEWhruKzInxdZE2Xv2tahDX2eIWZWI4q7OHv3ygNI1LfCGP2X+4Xm30lPs9/v/jEACQY2mSplx38WGsZp9p7frxJe7GuMqfDx/ZKdwU3f0/lu8iHEqo+ZBsMyBLfyV3hgCtnvdnz9GLPq9f3ohSFkN2n/V6nx0G/O3n2vs+NO/6/J5zoKXj1uRNGukN0n3fFnLBcNu56z7P1PH/389n6sfvEhev+kt/dvLYoaJYP8h/d/5JtLy01RGMfpded1d5CBS+RSSrnEgEguE0RuKVEYuEwMpORSijKUidzKwMBQJmRiTKuz1t6r7L0u9lqds+Nj+K+1z2E7mw9wnrO+wa//8/yeZ632bnxEXpcdij6yr12zZpdPdJJ3f/z88b0Ddu8v/xX8wyHd6P5ebR8ONPyW1ecjO9OB/fuPH9+7kf3i+DiR74hrU37DP9jXrtl1Qfuk6HzH6ZaB/XiN/eFQXl3/lfzyAdmdPwj2NS+Re1F2wd4pC29tTXYbhnipacA3ZAf2D97rBOyo+bJIrLVP+uzLaRR8T3hbV/1DdtKj6Fuh4VuFBvvpKvhVW4d6p/nXzyPNhj9uvQY7DkTvray2m/Xrhn+4DcCvXDGzITtrddXwGPDWyiC7VSso/gK/dN36QdlZ65OyU+VupWQzxzcM+R773/Ns2/o5f8nOSq/zVjcOOS+lPLzp2QSiZ2xs0tb1U2uyY6Ho824Xy03hGZOnxsYmED1gnzbl68MvW1b3ZSeZx5TrdsOAl4ydIM4+e/rCBcveHT0I9F0XpLRJ3gF7K9eMMUWcfcr0eQvmjy+eO+vdltWv0ONo+JB7nkjA3yDLPmFsUhX8/Ikz4ftZk98C1xZlB7lX7I+IBx/Zq0E/wzA0fF6xW2bYC/Ls8xZMRNEH9uOQe5K3Op1WidyNoS+7eQuWjM+M7BfBrguwd8qQu+HE2SG7hfPHZ86ZOnXy5DMGDR+Dx5BjhvNzZNkbsjtrDJM9ds0Af5d48HXZKVMVfQsDPuT+nDx7TXaGM12ULbAnEuwpcfYpddlxHhs+sjPO1UbK7IDvyW4RZPeec4OGL8sWRI/c+UfCwQ/I7rPisehLLLWegX0vYfZB2ble0Zd5EdlHY7upZHcCRQ/2Mg9DzigliLPXZXeJV0Wf53nCuFKO7lWuIburgd0H28Uh59RrwsEPyO6pUzysdjhgV0rtIczef7vpy04obnrs1nDltpNn/yO75wrBJ0VeDTnnRkl2Ox3YdQi+0IHdPabM/rfsDjinmI3sQfRO3CEefE12M0QMvggNH2v+JH12yK5q+O1OKZZEdm+cEKMkuxdOKGOLSvRKCLGbNnuQXWj4wP5KOGdsCL7QRiH4e2SDH2u8Wwnh4pQrguiR+y2y7M2rXArbQXZFkQT20Wj4/rvVdiFi0SN3yV2atkeA/fe7VWCXMXhrwJ5ep81e326uoM7DalckiWVOpOkhwsEPyO5+KoQyPkHuPrLTf6j+I7sMtmM9dg72jDx77d0qTQWXf9hH6ZF+Z2BH8FH0AvCfyMI3rnJvwB7ucrCdjbnvI8vevMplaeo42JMgO7CP1FUuNnyffVS2m95VLja8DvCSp1mWHaPNXr/KXUbWCrLDQcMDfjPZ4BtXuSNZCD6wa2sC+wg8VP/ebtqx4XVkd2k7u0me/c9V7mSWCYWG12GjR+7UZVffbm5nWeqYRe46bHbt9gg9VF8De1X0nqk0a7f3Ew6+LrvY8GCPpg9bLdh3EGYf3G5uAr7HbkLNU2/4+nazp91OnUHDB9mB/dsD2uy/2LualpuiKFxHR2JkJnWnJl5ESq8YoMhHPnoHvmJgIBEpI0nmMpBSMkaUmPgbZx+Hs+8+3XtPOf/EWuvs7yPmazF7h0/7efZ6nmftc1l3Q9iv1XXXQHFlL/q6fsf24K272W4FX5YXDJAecuzyx9Diude72GL/g7tB0oPglwNcdvCHKHcD2JtVv1zai94IwO7dDQgeJ/ywHMDVmrp+yRt7soZH0n9vh2Ggi94YSe7G0JQblkuLfS9j7BN3Q6QHykNXiwNehuCtu4GDx7Z2vOiNUYK6m2/GIPYeBL9qOqMU++5me9hMKVNjjoUhR4JXktyNAdKTre0hxgLnN9hjj9yNUiT48bJTSjHHHnc3VwA7WnoSPGL/yBl72t08NMpghveXHefNVO5utDEdWPp+AMEj9jXG2CfuhgTf9iR4wK7ZYw/u5rIVfN+330nwj3hj9+4Gy1qA2y2qduhbEDz8cZbtwW+aCF4ptHZ9D9jp3AWUtd7dbBD2dqDLTpjgHyuacn2Pzk4B9kO8scfu5h5g735VLWCny05fZ3vwkzjzRis1ljcWu6Q4s9PZ2raCCK/0jD32EGcOK20wx/bQV3aAfSdz7LHgr2nAvlgB6VuIsVprQXHm81wryPBw8BRjtT7C+OAncUaT4K270VpSnFkH8D8XDvt8zl3w8XbmKmDvfgHpW3B2qigKQZ8R3JzPtQHBty0IXs2L4g7jg88Fv7OYKxA8YTe6KPYzxp4Lfm9RAOkJe1MD9oI99hBnnhaFrhs6+KZGwbP+CYi0v3gI2A2Q3gue73dD0/6iiASvgPQH2GJP4ox4wR/1gl+Ngj/NG3ss+FdB8AsSPPvHZn8WPGE/zhb7vwW/mz32vwhezs8glJ9ywb/gjD0pLKeCl1RYxoLHOCMjw+eWfjW6m7e8sQfBJ5Z+0SHp73PG/g/B72NP+lTwdSR4CRsKJ/grTvDVSkSciS39zf+Clyn4dRR8YwUvzNJf84IXNuFB8F+Q9LHgn3HGnmX43Yh9IcrS+5Z+Fwl+VQm09OVznHIkeJvhz/DGjoJ3a7lLKPiktOOMPRP8DHOsF7yIlt4L/oSdcrJa+m0k+PfO1lZ2Lcf/oyncw//Z1p5ji5328NPiqrHYpezhXY4VLPgLgF3Fgr/IGTu9tIsen2S2lvOHQ3mOPRxs7U9hxVV5D6ecIMHHOfZgkmMB+wPO2DNbuzGuZyqJOdbtY2XtomHKhfVMyLG3eGOf2tpFyLFn2WLPbe10yol4T26n3F1hU44En0+5qvpFU07CByRuyq3ZKSdwPVPaRwgi21rKcrKmHNjakOWwsYR/45S7wxl7NuWOjaQXNOW2+yl3iqydxCkXGstqnHICdpLRlFu31k7Qu2KX5cpvsbUTM+UwywVrV7nGku9/jT/NcmtEeoBuSf+EM/bM2l21U07gyxv3+gTPfST9V97YE2s3ozwTrN1Jztgz0rvWrhJT04cC40JMei3jk7EdPs8E0neC8gxiL09EpFdSSG+t3Q3AHkjP/8mVzzM2xHvS866qXWsnkfSb/k165j9u5vKMI72cm37LZrGkd1POkf5mYm/4v7VLSD/L7M1t3tg96XNPz3tHMSX954T0/H/oKSH9hq0sXZDdxxu7J71rb4ws0nt7cykmPfs3V6mnL/cS6b/7ynI/b+wJ6c8j6ePbbg9X8FNPv7XwG9mmUwXzMBdITwd/LPa1Mm47X1k+zEf8a67Yp5VlOctG/AHe2F1PH0a8PXjD/LaLSR9GPN529PaGfYqPfW25Ht92rN9gTDay5Qcx3i4mfXrbeW+3xhV7RPrkthORZH+zd8YuTkRBGF92UXA5tNDGhVja5EzjgRiUK05JZY4jxaGQ9GpaD66Tw0JslKvsBMEDQSytLMTeREkCIWyRP8V9O29vZt6+B6m/vf0Pfsy8b76ZN3lRvpZ3MGh8sy7HN11cdlXiDfxtNb5BXrOsl/iRKHNL6BGGR+06MvDQtxR1tRu7Ze4BNLtSu0vmLp4DD/yUZVL3dubNHxX4R6DsnvFNGsvAI2+guDNLuqVgf1P+WBKUndWOypzt5nTg34DCe8rcngj8Enlim9TL3EcZ+OkCuJ1TaucGPidXH+Oy1wNPUs/t3GNceDfwh7bG09RygfsKiCfwOxR4UecyTHZf4J/ads7KHa6tPy9zHPg0o3bOZj2w1nsC3y8HODLr411IeF/gO7bOcU/TvhkBfokn8EOWO7K2qPbOI/Xpnsj6tTnyoD+gUoG3U8u0LbN+OVmg3sj7At/jrCe9A/2tbBX4KzLwxybrJTzolXzBLvp42kFJs9LarnPSO9xKR4Gne4oq60/j6sjPC4tDYt++F8F9NvBa7p6dZ/28EHuCb0V4n5A7zvquhadn6mcT0DIv5I6yno/8kuCp0kHOLr1ZP6Qjv8oJfjrB7OnKwKus3yoC3zfwU9I7W+kQH/Et2Cnrpdanh3TkJTzi3445WW/hO6x3XOZfo8GLrBdH/oD0TsIjPoqRsMMRR34/Lv0dwecrM8NC3MSRWc9VvmfFniqdNXjZboT1cdarKv/cwhd6xwYvPoqwPsp6PvIET4MMqnRs8DI0a1+w6yNfwKdpep8rHRu81q0I6+Mqr+AHXOkYfjvC+pK63hn4k9gH3wET+4DejWIu85W1x/t5gaN3FfyZCw/Z1BXsPrHvC48zt9YebvEy0XpH5pbL/FrA4+2gsd5psX8oDR7B47XzrHca/tjC5wIe7p0AhleVLu1Kg2fh4R4+Y73T8B2GFx3tJ1T467LM7wwceMQpFuldvcz/ztjaC/gnoPA3FPx3P/xdSHhT5qXH+RU3B/4qeRyGH2Wyr/mzgoXnMs8G71Q1dXkj4K9V8F+Nx6nDf24E/E8HHlTt2eNId9trAHyiDR7Dj5sM37+AZ3hEexuEf+mBR9vCk9Zewb/zw79tBPwLFx6wn98U/u8McIy1OTzgAHMTeOrqAEfX3NEGBY/h0S4tPPBbDO/282DPhAThuc7z9Bbtri4I3/fAo22fVh1t7cz/8MNvI/28KAg/rsHTZsadCOcLwvd4gKngkXZygvBfCH7mwGdHQIc+CH/mh49fwcPT9JbgcwNvd3LAnssIwg8zgl/lYiEJ7MfzFv5yDX4UW3i5jQW2eByE3w/Af2gC/Lcy7cVOzozg34PBc0sr4E9aBfxCwJvFY7BfWgThDwa0k/NPwoNd1v0v7zxCpAajOP7trIgFu8YCI3jxoigWsKCoiwoWsGBfQVDXgh1RVEQ9KAp2UDzZDx48CF4EBUG8Ts3UTGYzvWxx1bWXg+/lSyZfZier13n+QNDVy8//N8l7Ly+7jvIzp9jl0yBPbS+l9uiaL13blhDTBId4PeVHGfJzK+un/E0LgnMsZ/mTljyfXdObZtjkxc2MARttPW0y5aXX0DvLbxLlv3b+aOfy7oWMDKK8uZPD51h7LfmfX9raOn9y+UkXGBl6yA8x5feZbd27zi8g3/YV5YFnbNnKiU2XKLxi5iy/35D/1Namy/+CIgc5Nw3/wn2d1T92+QYuz5dyuHzwd5tO5zcevBu/HI1GDrD6R5THJUSryjnMG/oEl+/s+IbBTzLUI5FmVv84y69C+Ugs+B3VOzs6vvtAnKsDvhms/rHk+4+sbut4cf+tE9VBfhyqA6ju860gcL/n8v1qyK+ZzO91v3T1H15BHVlK4NVKB3mEdza+r6iu2NSRJQSKHUte7Gl1+4lcuNjRUa5Wj8Vip5azuqeGPN7rkI0una6PdnXu7l+0ndU9jQDIW20d1reQPfzazOVrqvv9cwmUeI09OhuMHhg19LHlXq2O8kdZ/VMtD+deZ8jAq46pIxtWs/qnqr7F6IHRAwc2bHVIHYnHZ29j9Y+9xAN5nUENY+b1ph4Pzb7B6h+7/JiGhkGDBjU0DB8+jasDtdRDodmvWP1jyev2w8eMGTN8+IiRk3tLHQgGz7L6xyY/cgQycuSwJfbUgSr1YIJOW2fYDxsG4sP693/jrq0OGOqJwENW/1jyYK8zeHDfG267OkdUDyQfUGnr8NyjPgK/6Xd78t/Uvd5NBPayGs3oTfr169dn9RRH9TRXD7eeJrCX1WhEXwH+0Hh0Dro7px4Ot6oty1j9w+0tGoEFc2uqpyvqGVVuIdDWgTxiiiOMbZ/4N3U5dfEYI0CjDYYsPyWqAz3UFeXieUYBuzmycIlz6lxdKZy4w2gyfregDnB1uMS3onoK1CXpxFNGk7EruDrgoJ7Ln7jJgGXXHzW/ZrSYYVdP9lDPa6VrjN2Z6ItEPSsJDHJFmv+mrpVKL45s8PtRfheBQa7IgdlWTWOqy6J6OZtfH4rHUd61mNg3BT0320k9x9W1NPyDOE9+6lpGilvrRXW1Wr2UDCTS6VDI74/Bg+smAlNskW3rHNVL2Ww4mQwEEsFgKO7HzcTpFKbYAnd2OKurrWGvN+lNcnlIfjKFKbbA+Z26eqanuqyqaiacam+X0ijvA3k3hSm2wLGd4bBdPa+rF+ArsqzmisViKc2Tj7gm3WKkuNBSUS9Y6rmCUlCUlKx2g3zWkI/CjhIjxfLTFXXJVNfgPyEnFVD+I8i/M+WjHgrLSQIL76K6IqSu5UulkpaH8GU58w7kP1aOvaeZwCxTYPw9UBdTl7RytlxGeUw+214sdieCoRDKRz0zCKyoCIzdYqlDJZtC92y2rGlcvgTyHxKYvB+TX0lgkCvyWCji5VROK2W5vCShvAbynwIgz5O/RGCQK7JVMtVb5ZSUg+DF5KWuYvFzIBiEY0+xrXtpqHvBXZHyPPmSpuV1+QLIv/cGQ7zK8VBr656gugbr5hlZUSD5ki351HuQb+XJ++i1dbffQteK7irK5w15uNdJigL9Lch3qSgPyUc8Tc8YKZ6ewCkO1Pdq5di/Q3lMHuQ/F4vtKfPYu5ooPLyxmHUyFEonkl5VweQLIF8G+azGk1fVTyCv8LYOkn/EKNEc84eCIN9V7MbkCzlDHo89RK9mPoB8DuUx+emMEOPdPn8c5JNYyqC8hPLWZz4lo3xRM+pbUj/dZUE0wuUlLGXE5FE+p2Bx3w1X+wBv6BczQiycFIVHtCAfVFBe1pPPY/KVYw+dTdfHZCIdhPEtrV5+j8eQT8tYx8l68nl7iYeTrADW9hTetxC4MN3j8vn8fpjbZ7COUwR5TN6Q9ybh1D+g9GlHFjR5XBEuH8ZSRpFlaPCg1q2SD6TPPGfUWDvVAxe8GMp7Ub6AyUvmsQd5bOsg+PnEOjmd44sx+Rhe7pNYx+Ux+ZzZ2eglHrifJtbHGSzf5XFFuXwA6zgN51m5SnEP93k500LofVobCy9Vkk9jHVdWIXmzyoHgFbmFWA8nMGGlx2PKd4N8VjZKPAwe3C++YXQZO8OS1yfUVokHY0wiW0iOXMFjz+tbfUJtFPcI2R2kCgetz3wZ5Lt1eQmfV1DdQBI45/JEefJBzWzrkJvsP+DWJJcpj22dqsrIIfZf8Nqt3+dxmpH6/AGfU2cyV9l/wuppRnGPQ7ykF7jM/huOTvdEeWeTBvlkYC+tp5B/beu4PG7cJsh1rb2znbd18RDYE+xae2eZ0db54xspdq29M2sXJu/zn6L1JOLfeD7D5YpE7hN79vivHL1+YHXtK/wf40KShXeUY0YAAAAASUVORK5CYII="

/***/ }),

/***/ 77:
/*!*********************************************!*\
  !*** C:/Users/王佳豪/Desktop/wyy/static/2.png ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAABmJLR0QA/wD/AP+gvaeTAAB/ZElEQVR42uyOwQoAIAhDN/v/T851yECioENHHzx0HoZE8QVJuYZhA2Cxr0lJlnN4Qkkn6ZE93fuW5wO8VRbPABgAAAD//xoNRTIALDOAaEZISmSFYnDC//fvH8xQRiS1sMSLxGT4j2Y7jI8eL2A+NNHD2MhiYH1MTCDrwZkFhH9D8WimIRcwMDAAAAAA///slVEKwCAMQ+PY/U+8LvKgYNn86P8WECUEFHkl/481VCCHS1rhZNEEeLBJxvadA2A9JmED/Us13oDZKzrqge1Ij9bh0ZFNc9kOvOYd35akCQAA///sllEKwCAMQ9+Xu/9xtx9HIClVPIIBEbQ0IE3ifaEDtu+SbHkAj8SQdJhGF4QGzoJJzdJcDu/UidsXdJaBtdiWO/FqmaP6ZG+1pcsmmCA8KniBz2lTPBcNwA8AAP//7FbRCsAgCDTK3vr/Py0qY+NEI8b+YBNCK04lLs7/RcwOUoJLbArhKqHqYMxzdQgv2E1++Bjjzg9y994p50ytNaq1hvNTrLVojKFxSkmX3wELXCnlmnNqXmbWc8QiovWxP/t4+MtHQlMYrw+VEVMXHcnOvj5tRHQDAAD//+SXQQoDIQxFNV20cSG6yf2P5gWEQREcqKZEdKAyNxghiyi4kPf8+uhT2G54ofnDzO/ehwuXEDzfVtvfYxeBlzS11lExRl1K0SGEl8B7HIf0IzpSSpBzhtaaXnAvMGUOAFj2l0LEZq3tIo0xpnvvB8RE1ImInXMdEWXtL4lW6tyJM0W4EgYAYPan1vpk5u8u2eOGUuoHAAD//+SXzQqFIBBGZW7+QILkRuz9X60W0SI3RnXVy3RVpFdImI2CMMLxzPe6zh9QYJZgKSWax6cKRsnATeCuMGDhT44mwJrnGaZpgm3bYFkWWNe1c8513nvY9/1zXRec53mDcRwHFN7wbjxrbJSeYxqlNCIsZV8IEcjfMpFzHhljse/7oJT6aq2DMSYMwxDHcYzW2iSlTEIIwjm/QUFTtT3lUa/apYCSswsaBbWGhqlv8JpFCPkBAAD//+SYuw6DMAxFycNJW4kJiYWZ//8iFhYGhBBLmzp1qC5NJMQv1FJ2O/LRPcnfTHsBA0B4wJFynVxcnZUJKlOWAgq0rquapkkNw2DGcTTLsph5nt22bRRCAAyHy0B7mNmEECjGSMx8ExHPzI+UElLqLiJILMr90OkhvxftwZJqrYMx5qW1fltrn0QU8oneewYkzjk0rKy1AELquv60bcuApus66fteAE3TNDtgKTMB9JJeF1jAylEYHamSf8V+l/QPoFRV9QUAAP//3JexasQwEEQtrS+SwhUXAhHYVRr//9e4TeWzHWKuMMFSkl2FMVK4Kh9gwfYa2Jk3e3iVd8bQuUL90UJE9jQu9alUJ1QZ7AWWfJomNc+z6vu+HseRhmE4LcvysK5rHWOEATQoEGOsQwhu27ZLCOGVmRtm9imlF2Z+FpEnETmnlB7zP0AuytXuv4cbgXOaB6UUjpdPIrpprTHvGCK6WmvfnHMfxpjNGAPzMMxjrQVlQJivtm2/m6b56boOxhHv/U4YaIfe+6pXrn2tNWWwFKqEEiiHNkpVVb8AAAD//9yYwQqDMBBE02RtVchB6EFsodaT//813kJpSyjipUKwTSRlwECgfoELc84u7JJ5s8npVlIoJFAQPL8NTMEWn46CbcJRwIL0fQ9u4F3XCaVUorXeD8NwMMYQlghHMY6jNMYcp2k6WWuvzrnLPM9Q5b0/Y/mD9w+MEfcXEqi47b9BVqLhsJCBf2JxzjXn/ElEdyHEjYhUmqaPLMteUsp3nueWiHZgmqIovmVZfuq6tm3buqZpPJgGPyYKc4b3YsAXQiQL2ONItp2CMcZ+AAAA///cmTELwjAQhQNJK0mG6u7Wgktx9f/P4t6Cq90sBblDYi7IlbSNurhqfsCFhHx599793YkSQ8rdAbczKj5QegeDoWC14DUMg2iahqFQbdvmXdet+r7P2UMgogQAjYhrANg75w5EtPPeb4loM8W/Ewxx/9E7JBDMs70EkK/uP51bxPrz0FEsidpLlCsW6G9Zll2klGel1MlaezTGXK21YIzxWutQFAWryr2qKlfXtS/LcvQvXIM/DIYlVZXoVSZVeYQQMCrdR3z900sI8QQAAP//3FkxCgIxEAwmGovzXiAkIHhFKv//CvGa2KRIYSkkgQueG2XPLJw+wOKKgXQhYYaZnV2MQH6jVCllU4n7nLdQJAxESol571d933Nr7do5t0WnSCmJYRhECGEXYzzknM04jicAOALAvpTy5Q60kEPC1Dv+wpIZaclBXrVoIOFwPFIbhuCc34QQKJazlPLSNM21bds7igVjGDqL1jp3XfcwxoBSamrNaMCfPvizzyGhiPp2nFEQy4lejLE3AAAA///Umr8KwjAQh4NpFcNBCZ0K4iPkGdx8ZWffoHTo6JQ5eKVRuJTIQVKqg4NL8SBz/sDH8f0uf3+LDIZzTmitVYyRZXhapFFzPJvdYhxH0bat7Lqu6Pt+Z63dI2LBYCAiDMNw8N6fiOhMRMcQwjYDkeJXNvv5H9XnYG+t+jI4jFIyK3GTzp1dayrLkoG5KqUuAHCrquoOAIFX0zQMytMYQ8aYqa7rOQlbdMG3jpI86bHYf/V3+bmEEC8AAAD//xqyrkdLkKD+BajWYERuSsHUwCbTQPMSZ86cYb527RrrjRs3uF69egXuV3z8+JHzw4cPcl+/frX8/fu3ObS24IH1G2CLBGHLShiQ+i6DBWDp08ABbHgamqj/Q5wPdj94iBlUaLCwsPxkZma+xsbGdpKLi+soHx/fHUFBwc+gJpiIiAio+fVdT0/vl76+PrhzD5q4hGUUtBoFlFHAfZRnz579kpKSgoXhoAovogADAwMAAAD//xpyrsbSAQclZGZYrYFeY4ASAmgU6ty5c8xXrlxhvXr1Kve7d+9AzSimt2/fCn769En527dvfr9///b8/fs3F9KE2j9oogJlOkZY8wIp08Bp5FIbtiyE3gA9AaJnFvSJQlj/C1rTMsBWDEAzy29mZuajnJycq3h5eW+IiIi84uHhAXfsVVVVvxkZGf3S1dX9Iy8v/x9kBmioGC2jMDEzM4MyCqin/3XIduQZGBgAAAAA//8aUq5FWzTICZ1PAIG/yEO10IgHZ4wTJ06wXLp0ie369evcb9++5QDNZL99+1by06dPDj9//nT+8+cPqLZgBSVqUMkHaoqAIhg6n4ExCw0bPULPBLhK78EAYG5GHk2DZWrY6B3I3yBh6PA3eOgZFB4sLCw32dnZ93Jzc+8TERG5y83NDeqT/FRTU/uir6//09zc/I+srCw8oyB35qG1CQiAZua/D7lFkgwMDAAAAAD//9RaMQoDIRD0PCKBNDZWeUHeIOQP+eg9xELwAakDYrepRDDMsYbFIm2SLS1kYR1ndnb/ItN5JaT3DtbYLdvZlUKxiWgJIawxxkNK6VRKORLRmnM+E9G11nprrV1AOqgXHgLr52XMAwZjSFCMPCRo5vgkdb4V0tmSjf2QSIoZRQBm71tYsuIMA8iHMWYDUJxzdzhf1lq4XgBK9d43DCFxB4Ay3Czuf4zWGp/Yk7eK37n8dCilXgAAAP//GvQuREtssFnov+BeMlJzCtQmBs1yX7lyhenkyZNsly9f5nz+/Dn3hw8fQBN7Ep8+fQoDdbr//v0rDRvpgSZ8JlhiAWH0RYbItQjyZCLyylsYGIyZgwGLu2B+RM4syGu2YJOl0CYmbE0aeBUKExPTO1ZW1oPc3NzLREVF7/Dx8YFm7L9pa2t/NTEx+WVoaPhXQkICvKgSuSMPrUlANQpopAs02YkxoDDoAAMDAwAAAP//GtSuQxu6Bfc1/vz5g1FrgNTdu3eP6ciRIywXLlzgvHPnDi9o6cfbt28F3r9/7/br16/wf//+qYAyBmy9E8hMWAmK3oSCTRjCVsnCANroEMay9MEKkOcm0BdcIq8cQC8okIeHQcGNtNoZNGH4nJWVdT0fH99mUVHR56ClLfLy8p/19fW/WVpa/gENEYMKLVAYItcmLCwsrKDahIGBAVSbDO4hYQYGBgAAAAD//xqULkNLdJz///8HrVv6g1xrgAIVNOsLGt7dv38/69mzZ9muXbvGC2pOffjwgf3Vq1fuv379SgHNcENHn/7CNhMxQDMXKCHAEgDyngu08X4wGzlD4MoUhDrKtALoGRffIAF6cxHmR+SmFnJmQe+rQNM5yALwLCEzM/NrVlbWlcLCwsuFhIS+CAkJgfonn01NTX+Aml1ycnLgdWlIe1nAtQkzMzNo/gQ0wQiakccafgMOGBgYAAAAAP//GnQuQp55hjap2ECZAzprDGsTg9u5oAm+EydOsJ46dYrnyZMn3J8+fWJ9+fKl3rdv3+L//PljBWqOwWZ4Ybv+0AF0eTlGEwPWBIEtuUDXCyt9YRGPww90BdgSGHIzCn1ZPaz2QO+TQDvtcP8hd+6RMgqs6QUe1mVmZr7IwcGxRFRU9LCgoOAPCQmJbwYGBp8tLS1/GRkZ/eXm5kauTcBxCcokTExMoOUqoIwy+DrwDAwMAAAAAP//GlSuQUpsoMTMBy3N/6D3NUBrpU6ePMl86NAhjuvXr/O9efOG/c2bN0Lv379P+v37dxho1Sx0vRB8+BIdwBIFrBmFPFfAgNT3gDU3sG0iwlebDBYAWzUAHaWDZwTk4Wr0DV4weeSaAzmDwdTBWrrQsABVDP9ZWVn38vHxTRQXF38kICDwW1lZ+bOdnd030GiXgoICRt8EmklA/I+wlcSDZrkKAwMDAAAA//8aFBkEufML3Z/B/ffvX/jwLfLOuVu3bjEdOHCA7fTp0zyPHz8GdcLZX7x44fLz58/EP3/+qEPH4UGZgxlXAv6PtmUVFiHo+yWQO7KwZhd6Zx0ZoHc8sdlNC4CtE47MR+5vIC/fR070sAyB3B9BHxLGlnCRCg7w5CN0DuQJGxvbMhERkU3CwsKfJCQkvoNqEycnp586Ojrg2gQ20gUbDgZNm4BWKYMWQKIPhAwYYGBgAAAAAP//GnAXYOlvcP358+cXrBoGyYNqDdDykNOnTzPv3buX48qVK6C+BvuzZ88kP378mPjr168IaPfkH6wTid4OR25moPc9YG1kZLWwZgZ0UxNGYkcfyRpMnXbkjIqcWZD7U7CaBdkPyLUpcvggj+Qhj/Ch2wk7egg6Ow8aGt7Py8s7RUpK6raQkBBo1fAnOzu773Z2dqBJx/+g/TVI+kDqQSuFwf2SQZFJGBgYAAAAAP//GlDbYaU1KDBYWVnBQ7jQ/gYYwCLkyZMnjAcPHmQ9fvw4182bN/nfv3/P8eLFC+sfP35k/fnzRw3WAUfvZ+AryZFrBeQNQzA9yGuO0GfK0UtnbGbjkqMFwGUfcuKCZXhYJoDVnLBECEv46JkBl7+x+ROtNgHV5KCa4TU7O/ssERGRbSIiIp9BI10WFhZfHBwcfquqqsKaZ8gz/KAmF3goGCY3YE0uBgYGAAAAAP//xFzBCUMhDM3BTy/11h26Q/boGl2gg3QiB3AHD4JeioIQFEqgKfm2/cffHHOQgA99L3lqPjI7he6iAIBlW7q0cLXe8N5z+3ZxztkQgk0pHXPOFyK68rCQBaJ8vzPbKeALaCQEKGK50CflrEtADQe3OkRbYNojNFWca9C64mUnWeX1oFWcCBKacv6ibpp68qaIjWWMcWqt3WKMZyK68+OyWutSSnkgIiEic+n3WowBY8yB1+i9V6nlLzcJADwBAAD//xoQW9FGqvhAhxQgd8ZhJdvp06eZ9uzZw3Hu3Dm+58+fc7548ULm48ePdX/+/DEHBT408JiQhyOJbeqgq0duZmGbREPbQARnM2Dp0wwkgLkJVmMwIB3egDyKBcsMyJkKueZAHw5Gr0XR4hFDDMl+0DAvKGCZQRu4uLm5G6SlpS8ICwv/0tPT++jk5PTd1tb2L2gbMHK/BFSTMDMzg/R9HrARLgYGBgAAAAD//6J7DYKcOf7//8+LvGQENtICCqiDBw+ygEapzp07JwDao/H06VPL79+/5/39+1cDulqUCRapDEilJ/rEHwOWWoQRx3ILWE0Ca37BahLkYU+YPeiTjOj24LKbFgBbjYk82ACrFZCHtGGFAgPSSBeszwUzAz1ske1A74Mh911gZkBHAEF9C9D+mX9//vxR/PLlS+eDBw+m//z5cxtoy/K3b98+fPz48buzs/Mffn5+8JwJqP8CbWqzMjMz88IyCd1rEgYGBgAAAAD//7xcsQ0DIQy0SBkp6VIxCgtkqBRZJGuwATMwABI9KUAClOgU/LIQX+ZdQoXk84F9x6EAEcmiBnNA77OBA1UEchHn3Mlae/beXzH4izHeSykP+LhHVVGzjoiETZR2EnReY6ec7PnLxBcmo21vnq7TxCqLs/49ZEHgZJZvKdaWMTPg6krivcGgkczBsSpAkllWVyzZLub4iXsVQHLLOT9DCLrW+mqtqZRS6r2/jTFNa/3B/2EDJLBHg3ku+CnpcJAQ0RcAAP//olsGQWtW8UAzB8owLmh+Y9u2bayHDh3ivnPnjsCbN2+4Xr58CeqIx0Aj7h+0xsHasWTAUZoyYMkcDEg1BvL5UcgZBqYelsBgNRxyYmFgQB0hoxUgpvmG7AaYO2F9DWR/wDILA9LEH3IYwPSghzFMDFvNgZ6pYPYjxw20bwJefv3r16/U58+fK//69asNlElAJ8C8evXqs5+f309QJoHGBXh/z9+/f0GdffrXJAwMDAAAAAD//7xcOwoDIRAdg3YhG3IOz2CRNtfJDfZe4kUCsoWVSGqjJMyiYXYw5cbGRkYZ5s3H+fwFIAwcUyllAw60HMuyCOecstYevfenEMIlpXSvtd6aS4Ua5dCzsdy/7tqTB9IjYNBFBZ8G629SlwRN4FDz9ruoBftFeycebtZIYIH8wgHp/UA+93P9/bj3wkReszUSQu5OcVAiHV74SXMtSilMeqwzAkop1xjjOec8CyEeODtMSvk0xry01giiFSSt1wctyQQA34Ti7iABgA8AAAD//6K5Deh9DuhJgn9hy0ZAie727dtMhw8fZtm/fz/fvXv3+F+8eCH54cOHmv///1tBj7wBZQx4TYM+No9coiPXJMSU7OglIHqmgDserQmGHDn0HtbFBZCGSlEyMXKTEXn+4z/afhdstSMywDWsi15TIxdU2PhItRY44TMxMd3j5uauk5SUvCYjI/PF1tb2g729PWj3Iqj6gM+VQFcDg4aPQc0tlLCnCWBgYAAAAAD//6JpDQILONAxnBwcHKClI0zIfQ5Q5nj27Bnjvn37QM0qvsePH3M9ffpU7tOnT50MDAwa0MzBDCvlYbO8sHY1ck2CvI4IthqXmJoEOfKwrDfCmF1Hn1HGVYtgSzzYEh0xAFsi/I9lxv4/2mJK5AlBWPigZwj0/gI+N+Ia1kXPcKC4gdVKsPBDHgQAyUPDmBmUFP7//6/0+fPniX/+/KlkYWE5cfDgQcGfP39+4OTk/KmpqfkPqU8CciRodAvWJ6HtEDADAwMAAAD//6KZyWgBDao5mGFDubBm1c2bN0HDuKBmlcDz58+5Hj16pPP169dGBgYGFVjmQB5pgZXisABG7j8wIK1URRbDlTHQhzDRmw3omQKW2P5hOe+WAa0Ti24vvkRHCcCWCZHtRQ4XWGZA728wIq0agAF8o1fY7IHFD3Jhg15Ygfiwvg2yPGzPCajwZGRkfM/Ozt4sJye3X1RU9IeNjc17V1fXnyYmJn9hs+5IQ8CghAFaMo+yPoyqgIGBAQAAAP//tFxBCsQgDFRLD2EPfUN78aP7M118iA/oSSgFbUnZwNRKb/oAEU2cZCZJFwQ57urzp5Qy5px3TMhjjNo5N3rvWeNg5FhSSl+t9TwMV4v5q3PI5EM07L/Q9PhV3h5WVXE8Gg/Su4gyWKpSI0TL6XqtFjEBRnerFEAmC5Gl1j9auYXcT0MQfOyBeYicT2YDMH2PyAKVxZyXsJMwcjDDtRljfiGEiYhWIjqstVe4JRQwT+DnuWddm6+UUicAAAD//6xdOw7DIAx9DEgZunKAHiBH6OGy9kjchqozk6cypHpRn+RaYYslVoMsbD9/MJerXbCgx7txVcjlOThdpNZKz0FYdWutrWb2BHDnKBo+jBKfmL9Xsx2FjcmFiIErJtY2kg9qtfBLB0vxuL/Pcs0q7Cm0rpyQP8g+WX8sIwv/RYLIQ0IpAs8c5SRsrwsr8vFXNDJncEry8vUPn/2KxcfI33ka9qUQbi1jjIeZvXPOr947jfiHUx9LKYLWR3YrpcSubc0xxrYRfFxIAL4AAAD//8IIdEoBUgIEnYHLC8ocDEhtxS9fvjBu2rQJlDl4b9++zf/06VOFT58+dTMyMqqCdprBjuxE7zgjj6+j1yzok2LIpRyumgQbQG9jI58oiOwe2IHP6B189JIXafHefwbU69WYsCV4AuA/0l2EDLBFmUg0PLHB3IAcjsidc1xNLGy1BoyNXlthq2WQMwlyIQLLFLBlI7D+I/ogCDR+wc2tf//+feDg4CiTk5M7LScnB+q4fwwMDPwJOvER5m6Ql1lYWNige93Bl6tQtRZhYGAAAAAA//+iqmlICQO0Ngo0nPuHASniQO3IAwcOsKxdu5b3zp07/I8ePZL98OFDDwMDgyao5oD1OWABjG2kCj1zIM92MyJt8MHWBmbAM7qFremFnAFgpS56omNAapYhZYZ/0EOp8YXvX+huuh9YbnuC3WrFATrzC0rjq+1Bdv5Fvr0AFobIS2jQMwtyRkKeKMQGYHrRZ+dhCR4mht7nQB96h5mD3FRGjgtorQhqToEyyRsuLq5SJSWlM5KSkt8DAgLeu7m5gQ7hBs+4w+KMBXTgMGQ/CXW38DIwMAAAAAD//6xdOwqDQBB9u2wXMEhaQypPkd7as+QMOZKlt7ANwRAQUggpJI0bNzw/cVi0CdlOix3ZmVln3htm/raTB+duB+h6gHMnY83z3GRZtimKIqyqalfX9RnAkc5BA5DG7+ZShe+h+8iVdCaI3/lSvIwVJ1h6B5HE+1CpHyp0QweIDvNUW7mItJTE+AFclFI3AHcADyakWuvGWvtiHypr7btt217B5ApYnsGxCcYYVjmTJAuVUuwWv3fOHQhkEP1hWEoQROphnEnoRvZa+/VWksjzgQ1Z4bt2VvLZh9gl9zHlajI5lzJ8vUz7jGdM1p0XbRkEwSmKomscx88kSZo0TTnyYZLXVw2PMnq2felbf1oAPgAAAP//rF07CsMwDJVdMoQOvUX3bl1ziF6kS7feqLdIIVuvkSWQKRCw5fKCBEJ1twwZHBKDLev3bD/tkqRXknKyXFUYzDAMegPwNE0TOKru/5SDjFu2C91brWI4sDxNj0dxyCXRNU9SQ4DYcPkaGFNLJ0TzkBzRhiK8Y4wvZv60bTvj8hf+XZblZ+5IQjbf1ncppdl/r5WkNsKpdYXiXJj5Vkq5hhDO4nHUsrPwWx222nLC3o7+1QCp7OwpXo/qWRlbGaknsOGubj6qp7Coo/bn598uaFEiGAmANQjBn+M4PpqmyX3fgwKVu65LEtJt+2o5ZyBbRyBbuwEkRPQFAAD//6RdMQ7CMAy0hSKiPqBDVz7A0gVVYmPjH7wB8a4OKAMjf2BmRNmYoEZXbGRF2Zg6ValV3SXn+Oy/AVIR5cGKD40NULKeUlpq4SG8HAcR2SNgYMLfuk7O5urz93779gKzvDGmQmyXOsaLTw8qH48/1hlTqe54a7w26RYoujHzmZkvzHztuu7e972M4zibvNCj6/spc28pso7veGI5Y9mp0qDOwKlmMDNaMI6q2KGxU4jIo2ma1LZtwkSrnDN6fW5EZEtEOyJawUqgadKXiuEFgOLB4JmdHJnUgGLZMMRTK4r0wME6ZS2YJ7wSJD75oU5D/KR1zvkYQjjhHTSvizE+h2H4zXdxma2ooxn+BwkRfQAAAP//ojiLIVXBoJEnPli/AzYRCBrO3bx5M9vevXsFQQcrPHjwwOfPnz/1LJA6EX44GXp7FlYtY2t2IfdRsPVJGJDaw//RNgAht7sZiGxi/YXs/2WAXgENAh8ZGRkPMTExLYqKilo7f/580FVn4NPioTvjmGFn4CJfgQC7pJNSAFtsCHUjI2w/DKiZBvIK6EIc0OSsiooK8+PHjwP///+f9O/fPyvQMh9oP+kfNIEzYxvdw1W7IvdDkId0sTWnYH0fWD8EtpQdfb4FOU7Qm8tI6sHNLQYGhtkyMjKzpaWlv1hbW78LDAz8AZpIBIUrrJUA7bSDal1Y5iE/tBkYGAAAAAD//6JIN1qHlg/RJ0d0ytesWcO2Y8cO/gcPHvDcv38ftAtwAisrK+ikvf+w5SPIgYqtJgHJI89vIFfl6KttkUdH0GsfBqTmDHqnHh0gZQxG6BE3IPVHGRkZZ7OwsOz8+/fvC9hNVaB4gcqDLqD5B7MblEjpAUAZAsl/TEg7K//A7htkZmaW+vPnjwcDA0Pa////zWEja6AjR6ElNYZLsTWD0JuryMPZyKNVsLCFzaojZ0LkjIXc2YbZiVywIQ0c/AP5jZmZuUtRUXGZlJTUd1dX13fBwcG/QLdkQY89BVkA3qz158+fT6D0B9r/TnYmYWBgAAAAAP//vF0xDoMwDAwJA6kZeED/0JWt4g9dOnfsxIcq9S9d+EdUKvECkoKqQ0llWVRql0byAgIpMcZnX+yspyy+GCILRJG7eJMBmDjaf6JM1jlX9n2/HcfxHC18TofHcFgkA7x0X8KPtYWeWdMBqSxeW875DfnedC2EgIB54WPgvY0x8BaHuq73bdteq6p6hBCW49OIKMO5GnmeP733M2AV5F/GoeJWHsQ3EAT6KMYrigKSxcwgIP3dWnshokZrfdRa32K2yKT58p/JJ8OQvIZc+6R7rh8ZC67xJdLj82ejjhaIOk3TyTm3G4bBdl1XolkgPDfzpvi2ACE3MA6ZrfxpKKVeAAAA//8iO2shWQpav8yLPhl46dIlptWrV3MeO3ZM5NmzZ/wvX77sZmJisoS2KeH9DtiMOANSMwp5tAp90gt9yBZ5LwOWtT4ozTJs67SQm1igxAXb5ANt2q5hZGRs/fXr1wXoOU6wmy1BCQo8Hg+6fhl00+1gBDC3gfzMysoKaqczQ7fBgj3Pzs5u8P///+b////7wE6dBPkRpBY5McPiBnmZCiwssc2nIK+dQx9kQR56Rp/NR58ngfUxkZaSgLdX//nz5wk/P3+GgoLCI11d3XdBQUFfHR0d/0LXbIGbMNATHEHL48GJi6xahIGBAQAAAP//zF0xCsMwDLRijCmldMnYj/QRpaUv6GNDly6BbIG8wHjyYpKQlkstEKJTpmoJhhAMjiTr7NNtyiDi1Bgeu5eIFfM60Jqn67oDGiyEEO5EdC5kJyt/cLkA/G1S16Tl+Bc0qJ2DhfhlJuK9sOZmFycDSWcu+uSwp7X2Ok0TuO8t4FLnnK3rekEvWu/9m+f9r85hxNwYDnXOQXINT5SAVc65HcfxQkQ3InpVX2UgSFXPCAAyU2jwhMdcrMM08UqurQ5mupbhQCnRNN4y87vlnAcZ/ZRSesQYd33fH5um8cMwMA9+jWwFUMFVlNU2ZRFjzAcAAP//zJ0xDsIwDEVjqWrEiVh6DMSExBm4GtwhR2k31GZKIhTQq5LKCiww0SlSh1ZObH9/O/bXCtJ8aFeynluCBhhDUzf6VjGHYxzHIed8Ke31t1OtoVQrMC2gugFtTGJUkAdTUpkT1rr2p6WGs7r7wTqEALaFJoImvIvIKaU0xBhvBLF93wNRaPn/WJaFMhnjvX+Ty78/wLCiME9mqkOVWmvJHxCvXEVkLyLnUkreoSAxxtWjagOk90DHEBXKVsaq9TDVaH2KU1rIppVQK1B5xz9z3g7TNB2ZR0/PAudcN88zXr4a1hVqAf9/VhJjzAsAAP//vF1LCgMhDPWDQl276gG6KPRUXfRSsyiFnkQ8ia7EEwhTyhsSCNJuBlq3wswQoy95vmR2IQid3kACL+vJ4ZxoB5pSOoCxqrUexxg3XHQSrannj5zjUun8PP+aFLvf9D2s05KbxU4FPLyQgGOclNQREEH6Yow5a62fQAvvvYkxriGErdFZa+2vecWvBuL13jvoZySwK1CR6vsh8Xk4507W2jt3i4GNYCvYm+3LIbHUxckQatalzTf1n4qpNocSRIGkuuXgfITecy2lXJCP5JwD+qbxM5n6pf/P79McKqXeAAAA///UXTEKwzAMNILSoRTyjL6k0Ed06FDoV/0KkzmNIFOMae1yrhSEwUPHespq+2SdTor0k4EIT1dAHkSL38DKzGgJukMTaVCrZVmuRHQCtdIxA84E2jaoNjxzs3TLQe2r36pXenl6oLZqtPUa7ks90LTsJV5jJqJLKeWec57lzzW48TcaY2P6bS/B988Le8L+mLlm3rFnqFkxxmdK6UZEZ5nFjhwKjKTSSjtDXh80PedW+lVc2DjDVmK3bMCKARWcTb2WYkMaQeCOhnVdH9M0DSGEo/d+P45jDeYFQ8gz4QPVCF3Fsruccx8AAAD//9xdMQ7DIAy0VCOW9hX9Rrc+o31a+xSUlZFfMLEysFAdsivLSoau9Ua2mMR3tg/zM4JIVMYdHSzd5K9QDpNIcs5nTCGptd7nnE9mnsptyYjcyCXIJzc4wa7JUDLrdC9itD+LRjYL79iU3vs6Ey0Dyl7MfCWiTUq1Sx6jST4i578bSqHwF951jIFLPbHmEMIWYwSavOErMBb4zgcfT8F0X21VUJ9ZJa9HEtvwVduTtph8BJUq5L631toDdL6UckkpBaCkfGcrF0HzGg3EI53ZoRHRBwAA//8iWgcsUa9cuZIBepPsH+SdgaBO0pkzZ9iePn3K/erVK6Hfv3/HQkuH/39Rb0VFMRc5AGEZALlWQQ9A9JoBfdEi8hIHmHoQBiWAHz9+/IXOaYCGZfN//vyZ+vXr10+g2g1aSv6Dnc8EnfQbEQDmV15eXgbQBZ3q6uqgTjrT58+f333//j2FgYGhBHQfOiicfv78Cd68hDyTjpxJsM2+I3e2kYfvYTU+cnMKefYb2+QtLJ1A4xu2vz30xYsXym/evOE8d+4c5+XLl8Frs9CaWpAN+aT0RRgYGAAAAAD//+ScTQoCMQyFpzJaEF14Du8geIrewpu48U6lx+gBpjCL0m2RTxoN2ejerAqBaWknzc97zU8Goj8YQtgP/kvXoVVKaSa0qrVu13XFRZ9HMrXpH3ry2w2La7Vz6Ge12pNoqoItPVqqiegl7CI5xQAGIAZ58NJ7f3jvZ0qhJOHMyw/yT4ZhhbxkWRYahKOBzgEqj2e9O+eutBFlA9lL8jFNLZEz1uds8RNd7UK0AWlMRHR2bC/TgY0QDZxaa7dSyjHnfIgx7mhXK0YyXiyysBdHTdb3VaZpegIAAP//5F09CkMhDE5FhBbv0q1bu/QMPXOh8HDRMziJB7Bo+R7JQ0Ip7VxHdZCQLz9fIvnV5xget7zRunh8SskAueizyjkfxxg3tuQ7Ui6XlLf4dDYXEeewSid874Qv4ODQ4cnfNJfe+4WIHrgCytZ7P2R2BRTk3xdkAJmxvFdqGHbLOXc3xpystQvyktbaChJhEUXpdAFRt6NotkqUX4NLgCF7GngCGHg1NsTnUsq11upijIcQwtYAO9G+exnj/dUiohcAAAD//+xdMQ7CMAw0ShQR8aIOfIQPMvGPPAOJKUvWSEEK6IotHYYB9npply5WnOvdWfa/CGJL+h/2O4OEYicg1p611va991MIIUOvVqT5MIv41vDO+DeeIYQOLPsx5xDXpCgqbaI44ivOOedljHHTQRB3EHBItnhu8R7qyq85xFBEdA6klK5zzmOM8YKMWpGwDGvk2xcJI7134JmX8pljHurPir2zAIP5abVWGNOHUgqmce6s2VSXkuLjFUV+QhAReQIAAP//5F1BCgIxDAxmEQ+99BH+SS8+zoOP6RP6h0KOHhRlJAMhdVnwas+FXpLOpJlmNhMk0CNQpT29yHlIa22BYSbcnWB7Bij2gNVcdO1W/nGLzEMPJAnWXsm2IIrhJIkanVZB6/Xwpti1lHIZY6BRBhR8UkD3D4X4r4tI4miM2gOjQu+11pOq3pgkoLCZTq1JR/LYpG8vVzH4Y2HO/bwkQx/lgyKqejSzs5kdeu9IkoVKAqIIYthBZXpCnpaIvAEAAP//7F1BCoQwDKwFQdhf+Kk9CPol8WX9Qe/toQ8IVBmJZQhF9gGbc7HQpMnETJrXC0JvEkE+6t1b9EgpYUD/iGmymPAkIqv+Haqn4fw7A5+8ecrSwiw+MI4s/D1bfX/yF3ChoDjtNNtFZEORD4xb7Y34X4wf5eF5qY5QExpKKVDA4r0/4IDgiGwksTCYIwLbVSevaPbQa1uAMMoge7pzjlrrN+c8A+6HEKYYI1gQLYro+rt4yHt0xTl3AQAA///snT0OgzAMhRMGT+VS3KAnQT1K194sl8gUj5Tqi4zkGoTETsYskSw/+8k/eacAccgebFd88QqzDCOiQ46ibK11AsEgGUR/w35B4I1/72S30RZBlVxWiaPQR8YnYkADRQQxlndr7UXTz3S61w1A97l2sJnZHAbRV2JVdc45f0SkgwTbO/noXeaImcX7RCzARH/xvbMYJM35aUbTzxlV9QndL6U8yCJb2dc+KwQRUJx+cVr6TSn9AAAA///CL4sA7Mg5EOQoUO1x6dIlNtDQ2uvXryX+/v0bgjwCxYi2ewx5txq6B9E9jzy8y4A2IvIfy15zmBmg4cf/////ge5zWPj3798CDg4OZuipfOBJzdGOOPkAFHbQYV3wMnnQUpU/f/6kMzAwLATV1tC+CkpHHASQMwVyoQmTQ58oRq9VkAtbZDPQm1rQDjtI3OP169eanz59Aq0H5Hjw4AF4OBjWAoL2ReDDvjgBAwMDAAAA///CmUGQSnBQ7cEOqz1gE3AXL15kvnr1Ks+XL1+YP378GMrMzKwJPbIHXnsgN6f+oR2+wIA2ro0+goGNj57xkEsk6NbOP9AJv+VcXFypv3//Bg8D/vr1C7zJfyiuoRpsANTcgibk/2xsbKCJQ1BHOIWRkXEjaHQLNE+CfnYZCCAPrOCKW+RMAhNHnyyGiaOv1YLqBx8dxMrKKvj9+/dk0GqOO3fu8Jw7d44FlLmh+mEjWuC+CLI7MAADAwMAAAD//8KZQZAAB3SdPbz2AJ1rdfHiRVbo9lnQFk9naMecCd1j2DpZDEijUegORK96GdCqQWz9E1CpBpoFhm6DBQ3hJv/48eM3aI4DVNKBRjIG86rboQZAiQ1acoOHyFVUVEArnCNAYQ8aIQTFBfIoJQigN6vR1+ChF4DocY/ecoDJYWmWg/sioMWXr1+/1gLVIpcuXWJ//PgxfLUvNC2DDIbPi2AFDAwMAAAAAP//wppBkBIsKGOwot/+BKo9rly5wv3161fmT58+2bCwsChAVpP8g5+ciD6si54h0Dts6OuzGNAyC7ZaBlaigGbAoQvS7jMzM4cxMzN/B/WZoEtHRmsOGgBQmMIS240bN0D90x8sLCyRjIyMb0AreX78+AFu0iJPCONqJYAA+tAvsjjyWi70kTL0OTPIJceMoCUznN+/f/cEja7evXuX+8yZM6AmIMxe2Inx+GfXGRgYAAAAAP//7Fy7DoAgDARC/AZ/wQ9xd/e3O5u4dmdwAXNYTNN0dfN2QoDyuB5Xd4MoZNEyXhstyBoRQS2fmHmutW6WK+g3pSVemk/YWyUYLcTLglnIJzqM40op7a21E8YgSen9nONDYG5lrWEgQ1r/gPMSRRMQqFib0btN53tPrRET+qeFjSnAuhSdg7RzjhjjysxLKSUTUddFRrEKUdfR5BHRPIQQbgAAAP//wsgg/5AOXgPlMOi948gnlDBfu3aNE1R7fPz40Z6ZmVkHtjQa2cEMaMNoyOur0AMJvcOFa3IQOeOAzAN1ykHLRKAZruz////HQZvJQCPUo6NV9AGwmgTarge1Ng4yMjLWQfuqoIyDsUobuaBDXi2B3OGGpRH0yUVsAzYws5EKXdiIlvD379/9QbXIgwcPuM6dOwfaXgyzH1bwg9I4vPZCAQwMDAAAAAD//8LIIIyI0ytYQB0Z0KQaLHOAhsuuXr3K8vr1a46XL18K/v//3wl5dArdsQxYSgD00gA5Q6HXRMgdcfQABnkU1CkHtXlBQ41//vyZBFphnJOTA+4VjtYc9AOwNADalw/a/66srAxau7UMlIZg/RFsk4jo67dAAH1LAwigz5XhmjwEASRzwX0OJiYmm+fPn8t9/PiR7erVq2wvX74E7yWBmgsq2EHTAeDTXTAAAwMDAAAA///sXDkOAjEMtIMEfIGaH+z3+Ed+lspttLwg7laLBq2lkYNEQ4mrNGmS8ZHxxJODkJ1DkBiNwd57aa1dxhgose6llCWYgbw5NwUzi8XrfIBK0xSVPtYIORRqXDiEiDxV9XHw71ut9Su3/bffGpqwR+kC7dZuZsANeiQr4h7uKu6O8RE44OoigXwKoIypTzouIniQRTDA4ubui7ufzOwK1TlloxjJ9H6sT04iIi8AAAD//0JJSWijSaz/oCeWwcSuXr3K/PTpUw7QVP6vX7/8mZmZ2aCHp6GYjK0KxbasGeZB9LVVyHfkMWG55gB6YALIaaDp8Nj///+/ge4MBHtgtFNOfwBrakGPGAKBN0xMTDnQ/jAjKM6Qm1AggNyXQJ9cRG52Y1sGj17zoANYWoauNvZ7/fq1CGifEqj/jDQwBBvyhY8koHTWGRgYAAAAAP//wlXUsiAdoQJ2PGj32Y0bN0AXwINmzeVAQ7tQT+F0JXrOxjaihd6EwtY/YUDLLKBqGyq34P///3uhJweC90+PZo6BA6BmLXRgBNT3AK3TWs/IyDgXFFeg+RHkviQI4FuMiDwfgm1UFAQItRRghSYTE5Pxx48f9b5//850+/Zt0JAvfO86NI2DEhnmKaMMDAwAAAAA///snT0OgCAMhSEw6I3cPJsek4EDODARJgbzEUJqQ1xdfBdoQh9p6c9jWJAzKbw96DbKWxxCcDHGJefsSykbHyoSwmbp1Qw6vOpa96wsrFOrftBN1cVae3nvT0TTaFBKQYAf3wE/oB3Mth/LZ865A1/hMwZItQiHbPhp5RrJl0FYJQbxhs6zNntXa91TSisZEJmQ5ErvrDe5yodtY8wNAAD//+xdMQ6AIAxsNW7OPoS/+QX/2A8wdiBsMJgjYBqCCaOD/QAloZS7tsdiF27+oAzfFMu5TueJyIqxRlXdocxnwXm/idlgGVXF35iu1n0L4eh625wpJY/P6EMI0EH6i4EfMOARBAYOXc4ZrJFn5qtm/6KAaJ9QMEvVjnquLN6YsY4+LmCdmZ2qHjHGTUSesVzDZhW61/pFRHQDAAD//8JmK+t/tFW7oJnz69evs4GaV58+fZJlYmIygFaPjAwkZg5kgN4JQ++foAcgtGkF2jJ74OfPn4tA8x+gkgq0E3C0aTV4AKg5Dh1cAW9x/vv370RGRsb90FNS4Gfm4mo1gAC2eTAQwLbYFQaQxZFn1qHNLPmvX79qgTrrt27dQp9Zhx0RBDn0GAYYGBgAAAAA//+CZxAkR7DAOuewkvzhw4dMT58+Zf/+/TuomrRjYmLiQz4+lJJRI1hAIC9pRt4dCCs9oLUHIzTQJoNKKdBqdtBmp9FRq8EHQHECqk2gp8mD4mwKtBYBn06Pa2chttEp9GYPSgmPZcU4TBxJLbhp9+fPH5ePHz9yvXnzhv3OnTuQBWWIZS7gpUooTTwGBgYAAAAA//9igikCHf8C6mSB2oqwsS+Y40BLCUAz5x8+fOD7//+/KzPaebnoM9/kgv841mH9g5z2B6s9lv/+/XsdaLwdVnuAImIUDC4AihNQ3IDiCHQFxKdPn9aBahFQlIKWBuEa/kdO6LiO6UFPZ+jzJciTfrCJQ2jNZPPx40dp0CQ3qJkF28OCtNYQvDQelBfA9jIwMAAAAAD//+ydMQ6AIAxFKUY3Jk7A7vm8hifkHDCS16BpCMbVwT8ylbQfCP9D76hijExo6Yq4coS7bfqY55zXftZP3vtkj1e2mC37xu3uDTMzm/2xBH9Pt6yfmA+JjZXoFwS/C3JDjkIIAllE5OAamLckWKGuHIPRvDjDrKYGaeJpXDUR+quXUvZaK20hNjQR0ydFDYxwAC4onHMNAAD//+xdOw6AMAiFxcM4ef+hF9CDsHZqStLEPCKKRHcH2bq0tAECj0+zb3KLP3CYiLCITNC63vuM3AfcK0evcn1/ZDZfJlK2Ak+P4tbA/VYiKq21VVWtxTJPRvnpe3QkcO1/D1XdmLmAScDyUQZibJEVwdcxuH+r04vIVtrHgJwxxlJrBdiEEVXnvLaQNLyEioh2AAAA//9iwrX2CmbBrVu3WEBttk+fPoFOhDCE7QVAn9DBNpsJA/+wHCiGXKXiymSMiPOtYKeNzwIN64JOHweVTKOHLQx+ABtdBPUXoQXabFC8guIP+eIbQkO7MDXok4UggJ65sGUoWDOLkZFR5+PHj0KgAafr16+D1sagr82CD/f++/ePAQAAAP//Qt6/wQjdHAXfTw5qoz1//hx0Cjjzly9fBBkYGNTwLS1B9hC2BI/cV8HmYWwAus8D5M6Dnz9/XgVqz3Jycv5FnkgaBYMXgM4Bhs5V/QOdCcDFxbWFkZHxCmjUCFaLYAPY1t8h1w7YMhRyYY2cvqDpDlzIMjIyyn379k3q58+fTC9fvmR5//49ytos6OpeSHZgYGAAAAAA//9CbmKxQA7Ehl7CB5k9Z3z06BEbaOTh69evuoyMjArQTg08YyF7AJvjsHW+0EcmsPVbYCUD0gb/haDNOTw8PKCTNUY75kMIgOIKtNQENMn8FbLEuhc2L4Jt/wd6esBWeOJrZqGrgbLBA7PMzMygZVLmoAzy/PlztufPnyPPqsPSP6SZxcDAAAAAAP//Qs4gzMg5DqQJtPLxzZs3bKAp+r9//5pCE/vf/2jj0sieYsCR4GG1CiOWlZgwgL6GC9SRg97y9AF0USbUjL+gEmkUDC0AiztoHO9iYGD4AIpbUCZBn/uApStcaQNbkwtZDfr8GpYMaPHp0ycuUDMLtAAX2SzoJhGIgxgYGAAAAAD//4JLQgXhXRBQ2+zevXugphXLx48fef///68OW3uFnJHQAbZahQFLDcOApVpEnzAE3d0BFd//+/fvp6CZWdBqUVC1PQqGFgBN5HJxcYFO1YfNroOGfMGddVzNIuTCFFuzHDnxE2qKwU4+gfZDFD5//iz6/ft35gcPHoCWTsGWucAMhlzyxMDAAAAAAP//YgJ1oKD3PzDDmlewzUYPHz4EtRNBw7siTExMMsj9D2zVGTIbm4eQ500YsFSnyPLQY2JgTbkVyCe0j4KhCUBpjY2NDbyrj5GRcTPSBDDBDICNjasgRqbRaiJwP4SZmRm0kUoWdED306dPWUBnS6M1s0Ajtgzfv39nAAAAAP//YgItLANd9gjNPfAJwu/fvzO+ffsWdEoFaGJHGmQobFIFfcQK1+gVrmYXtsyEbAZ01vMf1K7rHBwca0Ez59A25Gj2GKIA2vQBLyxlZmbeyMDA8BCUGEFxTWhEFH35EUwMnY2tKYbWVwGnob9//yqClp28e/eO9evXr8gJF9yKAuUJLi4uBgAAAAD//4L1QVihI1jwk0tA/Y9Xr16BJghBlyaCJggZoXs/sOb2/2hnHqF3znGVDOjNK1gVCdtKC537+AsaSQMNIoDW+YyCoQmgAyugZhZoe8I7BgaGzdDmPMayXFw1BK50hw7wDBuDJyj//fun/O3bN9DaQpZHjx6BN1Eh6QMpZmZgYGAAAAAA//+CZxD05g9oeBek+fv37yA5eagB/7FZjC0nI8tjKw3QMwt6BgHNfUClD8DMwBYQo2BoAWiCBq/IBsUtiADFNfq+IFyjU9hqGfS0h691gzTcK/vt2zcu0PpC0IQhqjHQPjkDAwMAAAD//4JJMKGn/Ddv3oDnP759+8bJyMgoD3UY1v4HrsSPLSMwYFZ5GABp9OobMzPzKegCtv+jGWToA2gfADyrDY3bb6BTaGDX+RHTBMdWs6D3Z5HTHVrhD+siSH///p0ftCrj7du34KsS0PrKbAwMDAwAAAAA//9igh7fyQTbOgtTBMogoOUB375942dgYJDClzix1RrY2NjEsLUZQWPmUHNvffjw4dHmzZvB/Y/RmfOhD6Bx+A/UVAZdRcHAwHATJAAa7gXRhNIOPnFkgKt5BhNjZGQU/vnzpxio9nr37h0z7IR6aPMKfPHT////GQAAAAD//wKdH8QI670zQBMpSPGrV69AkyX/f/z4oYRUg8BzCb5xakbE7CUDA5ZaBQZw1TCgi+6hTapjoNlXLy8vnKdOjIKhB0B9XB4eHtAtAKA0AFqfhTHciwsQk4Hw6YW2gkDpi+P379+6oHQPGoxCvkIa2hdnZGRkZAQAAAD//2KC5hhGZENAGeTjx4/M0BttYbUHSgcdm+XEiGHLzciZCbqVEtL+Y2Jahzy5OAqGD4Dt+2BiYjoNjXdwPwQ5LeHLDPgKWiIyD7i5/v//fxlQNwJ6QihsvR/CfAYGRgAAAAD//2KCdUaQR7BA652gq3dBcyPiUMtQuinEJFpstQYuxyNNCv2H3icHGvK4BO1Q/ftHYP/xKBg6ANohhx28sZWJiekj9HoNWOcd5/ISUgG2iWkQgGYG8W/fvrGBMgmoBoHZAU/oDAzMAAAAAP//YmJAu9wfxAZtsQVpgo5giWE7fwhbgifFA7jUQu+1Bpn58MOHD68zMzPBHfTR/sfwAdAtuf8PHDgAaq08Y2JiegCdHMY4qhQZ4BrVQmbjSldoaRzW1xb5/v07J6ijDposxDgOlYGBEQAAAP//YoKulEUx9dmzZ0zQ9fugU+cECVV5yPK4Fidiy1jYHA89YQIE7oBOZ58wYQLz6L6P4QdAcWpvb88MimNGRsa7oPQBG8lCB9iGb0kpjLGtLoemSb5fv35xgjLM8+fPwV0KpPT7n4GBgQkAAAD//2LBVoOA7nwATcN///6dg5GRUQg2g45hMx6ALQMxYJlARGcjlSJ3YUtbRo/zGX4AumQINkB0F8SBribHO0eGLI5LjAgArkFAZyv8+vWLB5TWf/78iTwXA8kTDAyMAAAAAP//YkIamQJ3UkBrUEB9EAaIJ9gZGRm5adFBRm8PonfWGRkZH8DUjh7nM/wA9PZc2Ia4+yAPosU/3M/EDgCRAqAZBHRrMzh9f/v2DdTvBucB+Ojr//9MAAAAAP//YoI2xmDzDuC93p8/fwY1r0AYpJkb5iZ6xBL0TFWQ4x/D7uoGBeYoGF4Aesc6eMKQhYXlCfQgEHoNVcImvNn//v3L++vXL8bPnz+DV4oj1WD/GRkZmQAAAAD//2KCLV+HaoDnbCgGzSZy0GuIFVqCMEFHs17h28A/CoY+QGpFvILGP3hyjk4AVilwwYaYQbUIckvm////DAAAAAD//4J10sEAVHKDchNoJS8DpDQHLTPBe8EItQBsiBeaYUH3e7yHNr1Gc8gwBaDRSWgr4T1oMxW07Y+1o04DADtalwuUEUCLckGVA4rdDAyMAAAAAP//YoH2QVA2WXz69Ak0/wHKVRxIN/DQ3NWwzjjohqJ///59hrJHM8gwBUgjlqC4Bl2bBzrWlq6ehdYgoNoD1KUAn8IIl2RgYAAAAAD//2JhRMsy0AsxwaNYoBMeQO0wDFNp51gYBnU6vmEoGAXDCkCvlAbF9zc2NjbQVRY89JwQhiZ9NtD1caCRW7TNeP8ZGBiYAAAAAP//YkGvGZDXzUNPUYcppnkNAp3BZITecfd7dInJ8AZIy9t/g/rtUDZd0hpSHwTcQoItg0cBDAwMAAAAAP//whRBAEZCFxzSzOX///8GLUXA5uBRMPwAqM8JWsSNPMxLRwDbC4VpMQMDAwAAAP//gh3W+x+Wg0BnkyJpoGvxjRRAf0GX1IPYo3vQhy9AWnkBalfBTjyhN4DPA378+BHFAf///2cAAAAA//9iQm/GDPSsNcwto0O8IwOgnX02qAAjIyMDAAAA//+CLXJihA6zMvDz86NvHKEbQAokZuTh51EwfAHoIHLQhnDQ4tgB8iQsjf/n5+dHLZEZGBgAAAAA///CtwrwP70zCBJgBZ2BReqitFEwtAAobpH2/8BveKIzgDXtUNYkggEDAwMAAAD//2LB1jlB2rv7GypEF1cjDTlzQq+h/obN0aNgeACkZjSooww+LhN6eg5dkhuUhqVxTMDAwAAAAAD//8I4rAHsWlZW0AznP2gGoVstgjRqxQ7KJNh2io2C4QNgzXrQZB1oXRTIY/SOb0ZGxl/QqzTQswIjAwPDfwAAAAD//2KCzlTDd1OBpv55eXlhB3mB5iNgw0gYGYkGjoWvsgSt1WdEnDo/CoYhAG/6hsQxLxMTE2hZE10zCDRDfAfNnnNwcPwFHY2KMlHJwMAAAAAA//8CNbH+ojehuLi4/kGP2fn+79+/P9guN6SFY0HVK3SqH3QMjAC0BhnNIMMUwFbv/v//XxB6iSZ4qx+dmtWwM6a/gioDUJrHOLWTgeE/AAAA//8C1yDIQ6u8vLz/oTv4QIn1J3TZB10A1B2w0y3ER2fShzdAajGIQ+Mc530hNACwJe/fYfMxfHx88OsYwJiR8T8AAAD//2JCntpnhJ7qzsnJ+Y+Nje0vKyvrd6SOMs2yNdqBX/+hczGy0FEO8Bmpo2B4AVCcwpaZ//v3TwbkOeSFgjSuRWDLTH4zMzN/ZmVl/QeqQUBnZCGNnDL+////PwAAAP//YoLeNwh3FKiaERAQANcqrKysoD7IVwzjKXEZ2hGn6Gyk9TkKsJl10MHVo2B4AdD8BxJQgMU9tjPVsF0Si++4WxLS4g9mZuYvIDtB/W5YRx2+nZeR8R8AAAD//2KCrYuHrRMD7SgE7fQCjWJxcHCAapD3+C5vR0/82DyADLDtJYYBaFUH67gpwxyFpW04CoY4gF6oCW4tMDIyqoB8g2uIl9CpOaRkJhiAzuB/ZmNj+wIaxQKN3IIOkIDqh+QJBob/AAAAAP//gh85iqxRVFT0P6jaYWdn/w3LIMjnYuFyCANacwldDGY+NnGkM1ZhGpVFRERAzb2/o+uxhh8AnSBy7969v6DzBv7//68MG6QBeRTXSZwggC5OSB5dLbSwB6fl////gzIIeBRLVFQUvf/DyMDA8A8AAAD//0I+8gcsC6rmJCUlQZkDVIOAlpy/wnaKHVq/AcVx2BxOCMDUg45ChepX+PDhgxgoIEFtVQEBAQImjIKhAgQFQffBMjBqaGiAmvMy////VwSlHVjcow/O4DtVET1TEDonC3lACnQENScn5zdQ7SEmJgaqRWDiEEUMDP8BAAAA//9igk21wzCoygNd+s7FxfWXnZ0dJPcKTRNWR+MDpKgFBRI0h4PmQfRg+9RHl74PHwCdIASdpAiKWy8GBgZe0JZbUA1C7JJ3cg8rhA04QenXnJycv0DpXFBQEH54NjyDMjD8BQAAAP//gtUgKKMHHBwcoIVbf0C1CBMT03OkpSdYawn0I0mJlUcW+490RTRoLwgDJJeHIm+gHwXDByAdDGIG8hRsBAtfRxzfoXLEAuQahJGR8RknJ+cfbm7uv6DpDSSzIDOYDAz/AQAAAP//YkK6lwF2UAMD6LplERER0G22TOzs7Pf/////AnZFGyEHYfMgepMMX18GWtUyQfnmoP0p7u7uf0czyPABoDgPDQ39A7kRmsEYRLCysmK0EojNLOhNKfR+Lppa8EQ0aIUICwvLFVC6FxAQ+AMauYUtu4fif/////8PAAAA//9igt76+Q92BCnMQDExMdBdcv+5uLhe/////wla1YThKGTHMmCpKWBqYSUHuh5k81hZWWH5VU1AQEBuz5494GbWaD9k6ANQHILicv369SC2HAMDgzrIU0h9T7gfcXXWkTMNujwxh8xBM95bNjY20Hlc/0VERP5A52XA0tC88I+RkZEBAAAA//+CZdl/6Es6hISEQB110PoU0LGGj6COwcwRaI5G9wA6G9vdheiegPZD/oKOHfr7968FdHM/Iz039I8C2gBoAQmbIDT///8/J6hJjZxBcKURmDyMhq0GxpbO8PR7YSNYzzk5OT+ARmsFBQVBE+PoafEXAwMDAwAAAP//grnkN3rJLyEh8Z+Xl/cPJycnaEXvI1hHnRHpjjh0ByN7Al8ziwFH7kcWB9VeUHk7mHmjGWToA2g6gK23sgcRoFIctrIXBHA1ydHTHkwMGeCaQEQyA7ycBNQq4uLi+gaaRpCUlETeRgurK/4yMDAwAAAAAP//gmcQ2EIx2EiWlJTUPxERkV+gVY7MzMz3oI5nwpZbGbBUcf/Rru3F5lF0x2PrhzAwMLhwcnKygDwCKmVAOx5HwdAEQkJC4OhnZWX9w8rKKvb///8AUByD4ho9HeC7wQwZEEpXyE17GICafZeLi+sXLy/vb0VFRfj9M0in+PxlYGBgAAAAAP//At1BCDpu9D+0HwKv5ri5uf8LCwuDD09gY2N7+vfv30+wZha2jEEsG1ktLBDQzYNlEKhd6r9+/QoCHar979+/0bHeIQygTWUm0NzWnz9/vP///y8NakpD4xruMVyJHMbH1TTH1nHHkoFAV3uAWij3QSNYoA46aJEiUqiCpxlAeeL79+8MAAAAAP//YgJNr0Mv6f+LPJIFEpeTkwMP9fLy8r78+/fvE9gsJD4HoYsjewpWO2HLMOjqobeg/oMGUDTskLHR+ZChC0BxijRI4w8ioIdX40zw+EankNnok4U4zAPfXvbv37+P7OzsD0FpW1pa+g9oLzrSKl5w/xeUJzg4OBgAAAAA//9iQjLkL1QSrBikQElJCbTKEWQA6GjI28gZBF9HCFsGwQZwicMCgo2NDVay2LGwsEgzM4O2ifwdbWYNQQCafP7z5w+oeQVaJS4N6n+A4h8UxyDfwNICMbcpozeX0OWR+9PINQlS/+MxDw/PK1D3QV5eHlSDgFexQ7d+gDRD9qkzMDAAAAAA//9Cds0fBqSEDzII1A8REhL6zc3N/YuZmfk8dBk6E3qziAEpt+PL8cRmKBhgYQGfjAqaAwFtnvKENsuYRy/UGXoAujgQHHf//v0DxaUAbPQKvQ+BrUbABbDNqSGLo7Ohhfx5UKHPw8PzV05ODn45LdQukGWQxX8MDAwAAAAA//9CziB/ocuN4R11ISGh/zIyMj9BCZWDg+Pcv3//nkPvd4NPyzOgnWGFy0O4agv0TIUsDnIPyG6ofJKoqCiLkpISqIM3WosMIQAqoUEZgZmZ+Q8zMzPf////i6Cdc4zjPnGNZJGyXAk9PSKZxwTaIcvCwnKYg4Pjn5iY2E/QukMkO1FGsBgYGBgAAAAA//9Crt7+Q6sYJpgloMkTaWlp8JosXl5e0IThHZhaBiw1BLqDkAEucUKAhYWFCepgyzdv3oTfu3cPVB0yjdYiQwOAVmSDBlhAiRNae/gyMDBoguIUGrc4AXKaQb8imhiAXKv8/w9qXYH7P0+5uLgegQafJCQk/oAqAaQZdNhNB5AJcQYGBgAAAAD//2JCu+DwJ/KNUyCgrKwMWsj1k5+fH3Ta+gVoWw2v+4gdosMFkCeBYJ04qEdTQCMLv379Ai3HZxidWR/8ALpDFRxnoIwCikMQAYpTUByiJWK4fwj1M0CAxHQF639c4+Pje8vNzf1bTU3tN9IEIWyaAzxBCJ5FZ2JiAAAAAP//Qs/BsBPV4aNZMjIy/8TFxX+BDGRlZb3y58+ff4ROPURvZiF3rvB5Ctt4NsiRrKyssLvcHbi5uU1BuR9Ui4zuExn8ABRHoOF5UJxxcnKaguIQFLfIa69wDfHiE0NWS2RGAV9vwMTEdFFAQOC7kJDQL1D/A7TUCtb3gJqDSFQMDAwAAAAA//+CN6d+/foF2sTyF7aBihG6P11KSuq/oqLib1CbjZeX9/bfv38fQxM6xrQ2cm1EbF8EfW0WtmoVVNqAOnTQ/krl58+fQfco/geNtIFGR0bB4ASguAHVEp8+ffoXHx8P6kv2Q/uWoIQJ7lvi61gTmg7AlaZg6QdVCbiJ94Odnf0iaNJZSkrqF2iUFqlFBErz/759+/YXtEAWDBgYGAAAAAD//+ydzQnAIAxGgz0LjuEOncJu6DBdpAv0LkXLEwPF6r2HBgQRlEjyGfMjVoCAIjZCM8ZciiaYZ8x7f5FxdM6dIrI3JL6Y6p2jEeP9mGbbR/FrXU+tCPfIUkqw1m6UJ6eUFmLVOIE/fYuQCbLhcxqAEmMMOecVNcF66Mndf94/68+AMKMnQCilb7pE9OoAIFyv4LH5sjw4Qudr0SLV7HW+iNwAAAD//4KnciQDQXdzgE87ASVMkMGgnCYtLf2Dm5sbNHG4E5QTYat/cTnwP9qiM5gYcucaOTMgn/CNrf0Jyqig9VlQjxaBahHQbCdoVpaUgBsF9AGgOAHFzc+fP/+D4gpp5Ap8rBQsLcDSCHJzGdd6K2ImmbGlLRAF0svMzLxNQEDgs7Cw8E81NTVwQkSaMwE14yGXt8PsZmBgAAAAAP//wqwGGBj+IPdDQA4XExP7r6KiAu6H8PHx3f/37981aILG61psOR9bzYKcMZCbVqBSBjryAa9FoOv0rXl5eRNAM6GgEgrUcR9tag0eABqCB2UC0AHkoL4HDw9PPijOQHEHikNQXCLdR4nRl0CvVZBp5MyCDyClMfDdN6Bbkzk5OS+DdsoqKCj8BPU/kEevoJeIgjvocMDAwAAAAAD//0LJIEi9+Z9QTf+hs50MOjo6oHUrv4SEhD4wMjKehFVNRDoSXirAmlSwgEEODGQ2rLSAZRhGyDFEDLC26////3tZWFhkQEPQ3Nzc4BICNFoyCgYWgDIHqD8LWtIBPVsNdOZVLWzeA9qfRMkcyJkFnY1caML6uMiHu8HuF8GXDKFp6YKQkNBTUCGvra0N6i7Azl9DGb1Cqb0YGBgAAAAA///sXTEKwCAMdHNPFzdHv+nbCn1SQZpAyhVTYhE6dqmToIMawyXmTAYTyw0c9j/YNgEzK+fc4IvEGDdmbpYu0q9mRkl/wqU55k+0sMOawSuQrCsrgkuiqsTMFaV78aIFFPnTA33fID/IAsgOIqyIVFVdIDPIzvPxfOTczHlTBO9/+v6M5PrCz0NBWtyNlYj2lFIrpSAGc0/o2VSuLO+Dcx9COAEAAP//wmXyH9i6FOTRLA0NDVAzCzQvcvf///8n0Y5QAWvE1YRCX0KAXgrAALahXmRzoQMKzFC5ONByeFC8cHFxgZfAjDa1Bg6Awh4UBzw8PKBmFGjGOuj///+J0PksFvQEDgLILQssS9JRagxs4iCAbUU4rDUEXZx4j5ub+xzo9EQlJaWfqqqq4OYVVC9sZQjmnAEDAwMAAAD//+ydPQrAIAyFo8EziFcQvG5v0KFDj9UbdOxQPmgkFedOzeQkkpi8/Dzx5SBD5KZYV/8IvdZ6lVLoIZ8ppY337DYT8YedoYi4vNF3rkwxHjn83GQWIaCfUOzxQ2kIYVXVAvPXeGJ/V+t7QeeWz0MqxSYismATbEV6jIzDQAt6fu2dyNBmhh6GNrbv6HRc/KfjuuecD0qE1hrzvO5AMUYIf/2PkBeCiMgNAAD//8JVg4DAL2g/BO5QBQWF/+rq6j9BHR1eXt4bf//+vQ6bE4E5GtnBDEh9CZg4ej8DfZ0NNjZ6dQxiQ6trUK4X+/fvXwM0gJmkpKRwZtBRQBuAtBqWETRbLikpCVrz1PP//39x6IWs4DVX2FoFyP0Q8Mw1tMBEboojqwcnWiQ16J18tM45yB0fOTg4zoDSrKKi4ncVFRXwFgqoethYFHiKH2OEjIGBAQAAAP//wsgg/xF7fEHNLFAmYYY1s0BnB+np6f0RFhb+ISkp+YqRkXEP0jJhFHOQaxRYRkCuOWB2oe/xQLIfa0cephc6bwM6TBU0gZjGwMBQDnLz48ePWUBuGj3wmn4A1MYHxSOo3wGqyZ88eVL979+/KOjJISAxlCUl2Oa7QAA2uoUtoSK3MtCb67CWCXKtAiq0obP4x8XFxW/z8fGBao9f0tLS8MlBaAUAOj0U+6k5DAwMAAAAAP//wsggsJwOxT9hp7jBPKKrq/tXRUXlO2ipMBcX18E/f/48BXXWYaeyo3eg0BM5micwMgx6RkDmI3fkQBgU8EjbNRtBCxpBbUlQ0xC2p2UU0BaAwhg6xwACoNW6oKUkVSBLQQKwzjDywAw6QB6tRK4ZYPEPSyfoGQI9cyFNVIOPdP7z589PNja2raC1hDIyMt/19PT+giYvYaeXQGsQUBqHF+AogIGBAQAAAP//whRBBaB+CMpedRERkf+gnMjPz/9bQkLiLiMj41rkORFYQkbvXKH3PxjQxr0ZkGoP5OYYcgmCbVQD1B+BLo9h/////4b///+rQE/pA9d8oAAZBbQBoLAFhTEoI4BK4T9//qj++/dvI0gKFJ2guIHFN3JLATlRoze5kNnIaQkZ4JpxhxWasNrj////+0RFRU+Cmlc6Ojo/QM0r5NoD6h7cdxQyMDAAAAAA///snDsOwyAQRBch07igS0dJD4d2jpBTpbUQheVo0OJMHNylzPYIid/OWwaGG+QM69baaX9vTUkpbTHGFRUtzSJP6D0F5y8NyQPTdSanRTl9fS8D/ugplttrdaQZ3/QV2M0Ycy+lzPDUKDA268A/fhsYU51XlNi3Wiu+il2AJGplRyGl9UmL8uPil+f3ik/64cpqpAcrFuZZPKqDqdY59/De1xDCmnOG3GM4xwF6sMcos4mIvAAAAP//wppB0MAPaCccXBqAPAtqxxkZGf0CVV2SkpJ3//37dwA6WvAPfeQB11AcA1Jtg23CEFkNujpY4MIyGXT/OjN034gpKyvrAtBJKL9//waNYoDHwdHuoxgFFABQWEL7HaAJWtD9fiysrKxrGBkZQat1QSNYzKA+IvLEH3qhht4nIdQ/QRZHPzcL1qKAph9Y7XFBWFj4LC8v7y99fX1w7YGUiWBX/YFvT8OVORgYGBgAAAAA///CmUGQNIES/U/YkC/SzDpowdcXUFOLm5t7FXJfBD2no/c/kGsHbCNUcMehjXzBBgtAfFAgw/gM0E47aEABumMshIGBYcGfP3/A4+AcHByMoLby6C5EygEPDw84rEEjU79//wZhUEQvZGFhcf379y9oYAe0vAQcd8hxBQLYhvKRm8z4RjBxpRG0xA2Kb9Cq3V9sbGyLREREPsvLy38FDSyhzZzDag+UnbEYgIGBAQAAAP//wplB0MAv6GpH+CpfBQWFf3p6eqC+yC8JCYnbjIyMG2C1CLJWdI+hz23gaoMiV6sMSM0z9GoaOdNBVyOz/Pv3D9S8imZnZ5/Ex8f37+fPn+DIBA1BjvZJyAegsAPFMajmAIUn9Eio2YyMjFGgzAEKe9gGJNjKB+QVEsjNY+RCE9sCVlxs9IIWuQMPaulAzdovIiJyHLS4FtT30NDQgKdJUAKGDuyAm1d4AQMDAwAAAP//wptBkBwOSnDgAPgP2bsILhXMzMxADvjCx8f3m5eXd+vv37+fgdp/uO6awzVChS4H8zSsdkDuzyC3a0GBDZtkgrVzoZNAoJoE1HfK/fz584yfP3/++/HjBxPsgpbRTEI6ANXA0ETJ9OPHD1BhA9rCOpuFhSUFelU4OHMg9xdBcQOrSUAAfRIQFsewEST0QhHLzDjGRDSsVoJenwbqe4CWQq0UFhb+pqys/MXU1BS0wBY8YQjre0AzB9gQvOu4GBgYAAAAAP//wptBkM8rYmBg+ApbfgLyDKgNCloRaWBg8FNUVPS7uLj4MyYmpnXIe0WweRZXRxwGYGxYRoAFJvpsKwNa5w9ZLTQDsIKaW8zMzOns7OwLeXh4GEFrg6BVMANsZncU4AegsIStqgZFy/fv3//LysqCmqzLGBkZU6DNKhbYiBb6uilYHMHiDHlODF0tvmF+cGLFc64zqC8EzQQ7RUVFrwoICPzU09P7oampCTrjGd73gNKg7ePw/hBOwMDAAAAAAP//wptB0BwFuivkF/LyE5DhVlZWf7W0tL6CzvEVFhZe8ffv31Ogdj+oqYWtg42cmP+jLTWBW4Q2tg2rntEnHWGHyUGH9mDVP2xpPNiJ0HmRuF+/fq3j5uZmB+2MBNWysMAZnSvBDUBhAyoIQQAU76Bdnezs7PwvX77c+f///0hQzQFrVsHiGHojGEYGgNUk0NIepUUAy0jIM+noi1ZBALljD+PDmNCC7zkPD89MQUFBUO3xzdraGr4pCnylM8QPoFW7YI0EDyFkYGAAAAAA//8i9ZjCH8h9EVBggE6FMDU1Be3v/QzqFLGxsa0AicPUoJcM2CaL0EsQWMJHLmnQMwJ6KcUAzbAMSOPh0OYWC7R97Pvnz58j3759kwWdPgcaaIGdKD6aSTAB9LRN8FUUTExMoGF+ULAp/v//fw8DA4MzeuaAFU7QEUV4HMHiGpbRYJkDufbH1r9E7ruAALbMgiQOHrliYmJaIyEh8QTUojE1Nf0uLy8P2i8Eywggf4AUE3/vPwMDAwAAAP//IjqDQB30D5pJWGCz6yDLjYyM/hoYGHwHDfuKi4sf+fv37y7Q8Cp0ASHWpQPoM6fYhgHRaw70ES8YgC11QN6d9h+x8hfecf///78xMzPzRTY2NntmZmZQjIHvWYD1qUZXAkNW5CLFBahUBpW6oAlje0ZGxkugC29AmQW0OhfW50CuudH7GejDsMiZAwTQCztscY4+oIM2yw7qC4H6nDcFBQU3gdKgtrb2V1NTU9DNBLC0AluUCGpawddiEQQMDAwAAAAA///kXDsOAiEQJVZbbULDljSbvQLhYnbewM7D7EXoqGxtLVYTYMwjjJmsxNg7HQ0Jv/m8N7yfHojM80MIW+M7DhxFpmki7z1kVO5a68c4jpeU0hVCxT2p0h5qITdFtRqkCOmffeSQc3J3JkcZzpl5jINknoSIdM55zTkf4R2ttRX2w9z4GgoY818NazfGoJ2oplTLskCJBK1Gp1LKSkTYHDyON5QrPT7XpjiL5pg+EEdZk/Q6JyShCGNnyZDxjkKodz+ltA3DcDbG3EAKOuee8zzX79iCNS8xxopcyfv81ZRSLwAAAP//IroGgRkICjxGRkbQBewssH3rIIdoa2v/NTU1/SEmJvZNSkrqETMz8wpoYGAsrUVvdiFPJiHPbWCrqrFNPCIHKvqqUBgfer4WbIMX2////zt//fq18NmzZ3wsLCzgEpGdnR00QgNWO5IyCsyvoNM8njx5wnTlyhXwItB79+4Jff/+fdH///+bQGEGXT4CzhzoBRby9mj0mgN9kSoIoPdPsC1rR844yP1V5GFdaBrbKikpeQ5UexgZGX0zNTUFNQdhaQTkZlDt8UNVVRVsHrGZg4GBgQEAAAD//+RdOw4CIRCdxJ5swSUoqUm4gq3ezkN4GBvjIbZyJBnzkLdOSCy2lmQLSh6Z4c3nze4elT5AeI5n98ApdACtlNJyzuuyLK8Y4xV89UP3v1TLF/zmlC7BIkA+5vAH4957JXowPuE0Ch+/4Bu9W30EpYicVfXRWsN/KhpoIQtd/9LsiLMCL2ShIERDrWhkpo6qeheR0+jsxmyrLpllME4aS0cGzFhBH7Lb7Q5nijxnJ3/JIPz9YTkD7NTKzG4hhAuMI6W01lo3OS0Dc3Slmxny0vvAEZE3AAAA///cXTEKAyEQNBEUzi6JhRY+wBf40hBIkTqQX+UFIZDEQj3mUFiWpLg2V11zyJ7Kzo6z46oNMgLqO/s1+sPHIg4htJQS/Iae1tqHMeZUSsE10lvaeUjFjOIHpUvTsfhyospdMCh2pd/yorBrtzY9AwJy7VprVynlWSm1h9Ax5ywh3QaGBQvyj9J5xITYtNZYaRIWSvgf0zQdlFKXWusNVwp2SIV6Y6H3KePIoS+lcil0opmFPzzr8zMTOq+M6YIRHHpNjs65u/ce0OoTY1wsRvtYmGe8vEeduyZ7CCHEDAAA///cXbENwyAQBDEBonCBmMIVEmKWFGmTgTJBBskUXoPIhVEgOitvnSxXSRc6N5Ye++/4+3/+l2EbSB4u2DsJ2GHAOI6vnPM8DMMcQpiMMTfuGTnaTEW1OmrnMEytrFYJijFT7FlIUI6Dfs66011bCPLOtdap934BP9daWykFhXgGTVjOub9IMIrTw37YhtwQbIVzaK2vYI3W2ukzTKnhSCWsQOXkGzOwOsUqlDyL7CvvYBkYi0Hx6Ptjyb9BALmOwdBa3733D2vtklJ6xhg31Fyzhsag4xSy7ncXOSul3gAAAP//1F0xCsMwDBSJIQ8I5BNZsvkX7UPyjsyhnTtk7gf6gXboK/IGQ4OHxqhciEAVbaBjtXkJRrJO0kl2fnYQUyTFdbQ8EwUCmbz3c9M0D2y8qqoLEZ1RsINH145gCzdBEFuI26gjkYBUurWFXkI/2lF7rPF/inX0AB/Cy+NH59y1KIpd13U8DEMaxzELISypLNIuGBsO8y+CvQpw4FTFGN00TVnf96ltW+hgn+f5nZkPGNRde0dIqd4eeNN6w1oDlNa9buSKbQSwbIEuInb/RMiYBvHC0jPzrSzLE67R1nUdvPdPkEUKjHEm55TS0hT8FsE2hYheAAAA//8iXQcUIHkONITKC5rihw2ZghLR8ePHmdesWcN19uxZ4SdPnoi9fft2OgsLiypoNAGUWWClDLZlCeg1DLI8ciQgN7VwzZEgZwj0IWLkAAOZC1oiDZVjhC7bOcrExJT/8+fPs7AVBLAJM9AivX/Qm7hAp3gMRgBzG3TilBHaZv8LW3bBzs5u/O/fv2n///83gw5e/IeuVWKEhSf6UiH0wRHk2hmcGND46MO+sMwEy1wweZj5IHnkkTFY3EALOFDVAVpBDLrbI11BQeGKurr6x4CAgM8+Pj5/QP0emPtYWFhAh/9+wnYYHNGAgYEBAAAA///EXUsOgkAMHSoYWLkdLkIi3sCbeA9deCCvoFfwDLA0gglpap62ZJwQTNzYJTsG2r73pp/5e/YvpoeEaIMFO0sgEyQTvDyqfZumebRtexuGAVBl3/f9MUkSr7fstituPASJihXtIxmRiyVAJd0fOHZKTZGojXfqmUYuUrL5LvsU2TDzJcuyMwYQ5Hl+StP0DiILjoKMAgWsKAoxp9Xl+H8z9GnAKay0AhBJF2AAcuBnWRHRVkR2zLzWagObMrgweVaiJqeQNMNCzhAqWXFXYAzLYHNSr0G2MOBp5ntVZjMzugQPZVlevfddXdddVVXm9CO0IiJcCGI81M/O4ZxzTwAAAP//vF07DsMgDIUgLtFzZEYZe6Lu3XqD3iWnyNiJLScorSIEpHrIllxLXTI0Y6REwR9iP2y/ww6idpcXdZUNFNNbKCqEUNZ1TSklX2tdYoz3UsqVT1t18iYPDWVOwcKG8GgE6VcpNaMoEq1ixRlV8/XrHse4eD9+4SRwGA7WFUAZtm3bw1p7c87N3vtOJoTaJBgZOS4o6xqvBZDxP67OpUcGlnMemCE45wz0CUk20JxTrfW87/sFxKhinhm+17EcebeXiI8uFZJok9x0dFObdg4ZTkPOHKZx+KtRJtYJPQtIF2aGvGPGXIRxHJ/TNCGM79QK1P3ap6q01t5cc3fYQYwxHwAAAP//rF07DsIwDHWljhFTxcIFGHMCcgcOwbEQV+gEG+pVegBE6dDBSoJeZUuWxQRkr/pJnWc/P9s/IcgHI9mgBkN/ZjR5SCnxPM/TMAygEK/jOO5yzqe2bcsalLgWlD5HYuMPe6KokSjFa5HEbqL3dXXZBCW57LsxYB0ZluX59rXWSynl1TTNnYj6EEK/LMsDr4A+tKCKxQ2Db5+VBlfq8x9LJeWaBNPJsaIWKFLZiUKmLTNjmuyRmQ9S7VctYgDxPdukaGApWd0jn4/yxWse5fXA826W1WJ5Ja+9XjR1cMtB1966rjtjdEGM8QnjQKcdMY71dmAniWgyMdf3X5yI3gAAAP//rF07DsIwDHUKlVi7VFRiYsghsrAwczWOwBU4Als5A3uHTlErVSKUVOhVtmSibpC1UuS4jn/PsX+6IMmKDCBuEI+A8RAIoLHOuZf3vgshrEMIl7Zt9zHGIw/lXE1JEwdxvYRx+hKkZQ3yXQbtiGWhxGdeWjr1K1ZHYyxCAzQX7zmxgCGYP2FS6zAMDRFdieiW5/m9qqrGWvuu65qQIeJ4JlNtN6VQbhZiOYvWttqKsvY0vA9xI7S5epXnNwLtnnhIZtb3/c4YA9fpMI4jGrdtmWbD/njGJTb6fF881Vkk+Tdp0kToXIrzUhBwCffQcaXGsrRA815YYNajKIpzWZbeWts55554+i10MyCIrBWY/p/xY0T0AQAA//+kXasOwlAMPdsNyRIwKAwej+EbbrK/4Sv4nmH3AyDn0BOYGSy7gZzRkpMbEsQqJ7pH27TntLebF14mmQMuU0oLazZN+vkCbduGpmlWXdet+77fDMNwKstyzyChA+bdci+flM5V0KaG0+6q1s/4UTvnooBQuXcFnWpEu9fLlh3zgXXfKdH6rSiKK4BLCOFcVdWdZRh1W8DMFjKF6bOLmFlrO45jBHDgrBmAnS1NcEkWIMH/+6LOrmWtUrX6Pf+Bdb/mOt3mbhvNSvl0AyWn5wWTTFgVAKd0jwTl7LHVdf2IMT5JQvgpQWYOm6/7gsC52QMA3gAAAP//rF29SsZAEJxcEgiC2AdEVKy/OkWqPJnY+n4WglpY2Ui85StyZ1YmZnE57DTdHSk2e7d/mWH3XyJIocBjCOGMtdIOxG0ebxzHz3mejzFGgnBhWZY7Eblf1/Wa6Rb3Sm9l+SnDrE+nSmDRA1jqOP6+8Pcy+vSA7xrZ0QOXFo0YBb1M+yUiSNXoz7xGMxZ2zz6o6oGjxnLOUUSeQwhPKaXHtm1fqqp6BfBW1/W7qkb+hmyaJhH0Ih6B77QisLjOOZNxfKKqp+SQAWAjtvOU0gWAGxG5BHBFtkhxJN4o6jKSmkNRN+IOzjGZDjxWYTrcPviXCce2b2tvHJ7l4I3Dr8vIsnfxpwAfXdfd9n3/wKJ8GAaZpokDncywNi4d00revVKmPz0AvgAAAP//rF1RCsIwDA358QK9QC+wS6z0Wv757am8hfQMUwpKpZU3GnkUHQrus4zSLU3ykr40f4NYJAAs+NLjkRflHanfeZ4ftdYrTqpV9ZxS2uecj601ByWx5pyMHRk+ccqXYxB7X9/Q31noNqcONQdMiBs9ilLdiRCVnqwdJmVladalC+RYEZlqrZP9J9tE3Xggir+VUkCiw6GrUXIQU4D7BLi66x5h62ZuUwgdvdqnTW2wUsiLChkNpv9syZy/icc4HT9yrUYy6VgM11kXgFW48PrgvT855+4hhCXGiA4Da2PQvrYVviLu+KbG/KdHRJ4AAAD//6JmHwQ5k4BW+35lYWGBzY+AS2l+fv7/oPVaL168+Hz48GGmnz9/Xnv8+HH1jx8/OkAlJGjNFmz4F7njDT2dG6XPAeOjNwnQS0psw7oMSHePIFfvyKNr6O1y5ISAPrCAHASwPgeU/x8Nw9QwQU/G54ZiYsB/pD4Qsl0YGQLZTdgSL3Lhg9xxRk7cyJO52OaU0M3GJo6+fAS5D4Lc7EKbzAXPL4GcwcrK2iYlJbUHdFe/iYnJRxsbm98yMjKgm8WQa3TQCOJnWmQOBgYGBgAAAAD//8JXKpEFGhoaYNr+QWc02UGjK6BO+1/I3eugs1tBzYnfb9++BQ0vvvj8+fOTP3/+gC5YYQfNkcA6oujt5f9oS6MZ0HaFYYtA5IzDiLY3Ab3PgjzUy4i0XwW504m8VRdX5x8JIGcYZMyIdm0Eekb6z4B59wqGOdA+HlEpAuZ25GHt/9Bl6cj+Rd5bAxPHlskwPIqlmYXeYUe/EAl5WBmWOWD6WVhYJsnLy68AXVdgY2PzztfX96euri786B5op5yNiYkJ1OeA30xLVcDAwAAAAAD//6K+iZjL2bn//fvH/ufPn1+wmXZQwFy/fp1p586d7IcPHxZ4/vw515MnTyy+f//ewcjIyA9dxsyEPFmIPNyIXAoxIGUS5FIOvdRDD0D0fQyweRfkjIlcojJgKY1xNV/oBbDUYCgAPcH8R1u2gz5aBfMXE9rpI8j9Plg8IN/FguwObEPBsDhEH91C7nOAmpegWXKQFWxsbB0yMjJrREVFf1pZWb11c3MDLWMHD7UjZQ7QZCCoafoNucCjKmBgYAAAAAD//6JqEws9wEATZRwcHF9BtQdolAG69RVck4BKg2/fvv368+fPB1Bzi4mJ6diDBw+afv78WQfKJLDmFraaBH3lLvJkI/qkI7oa9MSPvA4MNr8AqsKR+zbICQdm1kBlCmSAy358TSDkkSr0yVTk5T7I803IM97IK6xhAD1zoBdUIIDcYUevSWDLj0D9MjY2tsmgzCEsLPzL2Nj4vb29/U/QTDkss0FnykF3jYBqDZpmDgYGBgYAAAAA//+iSQZhwCzdvoA67aCaE5ZJQJ0s0MYWPj4+0Dr99ydPngSd6rbn4cOHn3/8+NEOupUIOiPKjDzaAUug2CYEGXDUJgxIM7kMSMOKsCYHLEHASjPYLC9yyYrcvMOVAAcLwOYu5CYP+pA3co2BvAwEeTQQefQJedEnshnItTy6GHLmRF6eAm1SgxeLsrOzt8rJyW0AZQ4jI6P3Pj4+oL3l4LkOKIAN54IEwMO5tMwcDAwMDAAAAAD//6JZBmHAzCSfmJiY+KBLy//C1mxpamr++/jxI2h9zYdjx47x//v37/TTp09Lvn37Vv/v3z8F0GQ7KH2jT6Shz7jDAHrNwYDZ5MOYAEPesoueQGAdzH9oJ20QmoQcLADbAAW2GgB5pS36sC/67DoxE4i4xJCXw8NqDtCoJwcHR5ukpOQeMTGxH+bm5uCaA5Q5kEdHoTUHKAeCOuVY/UJVwMDAAAAAAP//oq3pSBGAZB8fdEXmH+TVv5cvX2bas2cPGyiTPH/+nPPx48cqnz59avj7968etH2KsiyF0EQVoYhCnvhDmzWHL5hD7twjDzMPdYCtfwbLGMjNL1iTCDnDIA+KYEucuDrryIUUbJ4DugXiBRcXV4O8vPwxUM1hY2MDyhy/zM3N4QsQoZkDutzv/2fYOQe0zhwMDAwMAAAAAP//omkNAgPIpQCoJmFmZgbND4DXmcCWpIBqEg4Ojp/MzMwfjxw5Ahrqu8fKylry7t272r9//9pCaxJQJmHEVmoTGuZFHqWBtbVhTSlYpkCf+4DVWsjNBmyZc6gBdP8gD5cjD5nDRuyQa0/kfhn6yCJyXKDHPcxM6FIb8HZZZmbmOzw8PLVSUlI3REVFf5mYmHzw9PT8CToqFK1ZBZsIpGvmYGBgYAAAAAD//6JLBmHAzCSfmZmZ+WHNLVhNoqqq+p+Tk/MnDw/Ph0OHDv1hZWUF7WirevPmTcLv37+ToYEF69DBzWXA0ueAAeSl8Yxoa4FgEQ67BAaUUWGz9wxoexn+oR1YhpzxcNlNQRjRFGAb9EBOxMjzQehLbmAAuZmFDNDjBW1dHKjgAx/cx8rKuktAQKBXUlLyuaSk5DfQJCBonkNbW/sf0qJO8HE90DMNaDbXgRMwMDAAAAAA//+in01QgNzcAi2hAJUkoH0ksEwCCvC3b98y7t69m/XYsWOcN27cEHj37h3n8+fPQ3/+/Jn6//9/AdA6KOjmH4hBWJpX/9E2TSFPEDIgtYXxrcNiQCphGbDsWRlqABZOTGinhTCgNSeR+24ggLwcHXnGG1abII8iYosPKB/clwQNzbKysq4WERGZLSYm9kFRUfETaPkIaIYcNAmIpB6UOUBLbUD6BiRzMDAwMAAAAAD//6J7BmHAHGXhg/Yv/kCvewMPt4IW9h05coR59+7dXFevXuV//fo12/Pnz/W/fv3a/vfvXylozcMEXeqBAZDXZTGindYIq00YkfYw/EU6FfA/0v519GYE+jAyFv8MSoCrdkIedEAuTGCjeTD/IffXkAsUEEBfHoQMYE0qyOgsM+j8qiZJSckdwsLCoHPUPjk5OX11cnL6AzqhE2lHIHjTE2iDF2j3IMhu2EmPdAUMDAwAAAAA//8akAzCgJmoQJOJbMjbdmGlP2jr7pEjR9hOnTol8Pr1a84XL17IfPz4Mfn379+B0FW14PYshgVobW30GuI/0kK9/0hnyjIglabIk2PIp6jAAHIGwZUAByvA1TyEZQDkdW3IKwyQh27x7beAioMDFFSQsbCwHOfh4ZkkISFxE7TZSV9f/4ONjc0Pe3v7P6DVybChd6RJQFA7i6orc0kGDAwMAAAAAP//olsfBB2gJSjQZCIoYGB7SeClto2NzV/QQjV+fv53p06d4gH1S7i5uTtevnx5/9evXyn//v3jg+6LAC/hQF4qjxzJ/9E2Q8EiHr1DiiyOPNOOLUGhN+mGEkCuLWCJD1bbohcYyOvekGsSbMPFsOYRUq0Bupd8pZCQ0ExxcfF3oBW5oJ2AoJEqQ0ND0G5H5MwBcgNo+QhoESfVV+aSDBgYGAAAAAD//xo4m6EALWGBLuLkBmUS2CYfWJPrxYsXjEeOHGE5ePAg161bt/g+fPjA8uzZM52vX7/m/vnzx+Lfv3//YatA/6EdFgeLcBhAXoAIW3vEiLTEAj0T/EPbaorH/UMKoPsJPdMghx/6UnXkyUK08EGuNW5zcHBMlpSUPAI6TFBJSemTlZXVVwcHh9+KiorgE0igdoFXIkMXHoJqjZ+0niEnCjAwMAAAAAD//xrwDMKAmchAJ4nzgPZ3I49wgTIJ6GjM06dPg5pc7BcuXOB79eoV5+vXr0Gd+Jhfv36Fg2oTWARBD14GG4i8LAX9gAfkIV30hYzIw8DYwFDqg2ADTGjHfsIAE9J2VfSOOfJsOHINi7TSGNzXYGFh2cHPzz9HXFz8CWiLrLa29ic7O7vvoGUjSHvIwU0q0BZl0D56RkbGL8i3zg505mBgYGAAAAAA//8aFBkEBtBGuPigp1jAO++wRHv79m2mY8eOsRw7dozrwYMHPO/evWN/+fKl8pcvXwpBq4JB21GhK0NBG5sYkZsE6McJMSDtjYCVkv/QjsKEZa6hniGwAWyjWAxINScIwA6qBgHkgzZgezdgtT10zd1tLi6uTnFxcdBZuaCL+79aWVl9sbCw+G1gYACe30Dub0A74yCLQEf0DMhIFU7AwMAAAAAA//8aVBmEAXPWnQvUeQdlEug6NUZY5/r9+/eMJ06cYD59+jT7pUuXeEEd+Hfv3vG+ffvW+efPn/F///5Vgq6d/getTRjRM8R/tFPDGZEWMzIgTZAhg6HWGScE0Ffaos+uI8sj18jQK89gs+GMrKysr0F9DQEBgQ2ioqKvhYSEwGflgo4DBZ14CDrUDWkQBJyhkBcdDsbMwcDAwAAAAAD//xp0GYQBs4QG90tAmQQ6ycT4H+nQ5Pv37zOClqhcuHCB48GDB9yg2uTNmzfCHz58iPr9+3fs379/2ZEzCqxGYUQ6tAB9dIsBepoG8mTZcMoUDASW66Ovu0LftQla1QA9SAF0EAXoIs/tvLy800VFRZ+CMgboCgLQKeu2tra/QWflgvTA1npBd5yCp8Zh/Q1kNw0qwMDAAAAAAP//GpQZBAaQIg0UGTxIk4oMsNoElJBB93pcvHgRPBx8/fp1rqdPn/J8+PABlFGUvn79Gvznzx/3P3/+CEAj5x90SysjWlMBxd6/aDcbIc+bMGBm4kEN0IejsYQvykgdzI/I80agGgO6RAQ8Wgia8GNjYzvMxcW1XEhI6IqgoOB30NUX2traoOYUaKn6X9DcBlI4wg7XA89vQIdwYR36wRl8DAwMAAAAAP//GtQZhAHzTFXQAQbsf6E9eFhtAhtZefToEePZs2dZQM2u27dvc719+5YDVKO8fftW8+vXr2l//vwxB9Uo0CoetgCSEX2zEPLxmNgywlDpi2AbhsUG0PeNo/cxoHe8gGsMUOJmZWW9zsnJOVdYWPg46D5A0NUDysrKoFudQCtwf4Mur0Hva4DaYaAmFehoKNgQLrZ1XIMKMDAwAAAAAP//GvQZhAEzYkFLTEB9E9Attr9hfRPYSBcoUT9+/Jjp+PHjLJcuXWK/desWqDZh+/jxI8fbt2+1v3375vXnzx+nf//+iUCbXv+hNQYTclOKCctxN1jcAgaMaDvqsKmhNcCWaYl1C5YFhQywi49ANQa00PgGqjFAx68KCQmdFhQU/AK6uBV01TLoNlnQhZmgOwFBR/Eg3ewEjhvoeVWgwRZQrQHv1A3mmgMMGBgYAAAAAP//GhIZBAaQOpKgkowT1IEHzX/AhoNhiQGUUUDn0967dw802sV67do19jt37vB8/PiR7cuXLyyvX7/W+v79e+CvX7+s//37JwHNGP+hZwaDMhsT+qaiwTBrjs9efHIMRLgfVluAambo8h1wxmBlZf3IxsZ2koODY5OwsPBJXl7en3x8fH/k5eW/6OjogDY0gfsZoIPrkK82gN0LCO2z/Pr79y/oJgD4vZZDAjAwMAAAAAD//xpSGYQBswRnho50gQ+qA42qwOZNQBEBmzu5e/cu0/nz51muXLkCzihfv35l/vTpEyijyH/9+tX458+f/n/+/DGEDg+D8xqIDV1aP2jCCFvCptQ8aCn/H5qBQCn3P/SCoQfs7OxruLi4TgkLC9/m5+f/yc3N/Q/UATc0NPyhp6f3R11d/R/opBrkuSWoWbBJP9gIFby0GQq1BhwwMDAAAAAA///UnbEKAjEMQHs3tGKhPThXt44dpD/gt/sBglvHu8FR2kKHiJQqkUb7C5dPCGTIS/KyuQKheLfb8UazkFTtmg2FHFFDP8jCRn5d1xF7FO+9WJZlH2PkADCGEA455xMAnEsp+BLgWP8iukpYsoGBzeaMFgG7iffQ7ChErh6c85sQ4iKlvM7zfEcZt9YaN23BWvt0zhVjTFVK/R4idXSKvMSYM+w1XiklNk3T5grjG4yxDwAAAP//GrKRDQPIyx2YmJg4/v//zwlrdkH7GCjDwiD2s2fPGM+cOcNy7do11tu3b3O8efOG/du3b6BLcjg/fPgg9vXrV+2fP3+6/P371+HPnz+csCURUHP+QTuyRB+5MwjAf6TbhiGnrSG2G/9mZmY+y87Ovo2Li+sSHx/fUwEBga+gjAGaAVdUVPyuq6sLWjf1R0lJCXxpK9r8EHLGADFBByn8RB79G7KAgYEBAAAA///UnDELwyAQhS0S26jg7tAhkC1D//+/CIROLl3iDzhQMDGWAy3XrVvbG539UN679/4ekDZUEsYmwlKKqI74Gyg0NOW9P6ErvyxL55w7r+t6AYAuhMABQGIRdIxxSind9n2fcs5XrDCiOYrqrxTSgcW+BQ6B4CU+4N2tsmyDAoHwnPO7EGLu+35WSj2MMSClzFrrDbuohmHAHqptHMfDWnu0UBldSCRg4Bl+p/DV+Hnp9uNhjD0BAAD//9xdMQoDIRA0aBCWbcJVqWLpE0Kenzq1oJWQlOkUTNB4YeHuOMTumpCtrMVhdl1m5m8AMle7iZ+AsjDKephfC4G89ztjDLfW7p1z9DUsaVahHL8YowwhUCTyMaV0LqVcaq2nnPOBmGRsRFnUlk0PiHVAs/XOx+Y8m60t7RJr/K2EEKTgfHDOb1LKKwDcEfGJiC8AoMD9OgzDWylFIfxZa/0hV/5eRnmHMWjRR6bdP7kJ31SMsS8AAAD//9xdywrCMBBMw9qGwB69BkTo/39Qgwg5CUFoWmhNamRLlFgDnnV/ITts9jEzfwcQ9tnI87SNr9N0akkJ/AJKlZlIOucq2qdorVewGGNqa23d9/1unmcS3ebDMMhxHPfTNCnv/SGEcKTqkohc5Jor4kY/azMGjoVEL0X+Pm9fuvw0JhvNElXgyjmnCmEAoAOAkxDiLKW8IKJrmoa0pyIi+nQrdWvblshLi1LqThbKBU3jZ/Kv64zUY1DFIHD8bAP+NRhjDwAAAP//3J0xCwIxDIWP4Jl2KDod3Fq40f//Q9zKrQcHIjhJG2wrr/Swg4PgIgY6dOmUR0hSvveXAmkjv+wTMJlB4nJN3lzdsHKbeC1FEOC7eZ4JexXn3G5dV5wiFlQWESmGmCEE3LX3/igiQwjhhEY/5zxg35JSOsQYTRXOfhPnpxOp9lsIRIAGmIjAGoORz5WILkS09H1/ZuZFKXVTSt2ZWbTW4E0BiBGNMRG0wnEcH9M0lb2FtRYNd3n7DbWlVIsqjM1oBxwzjynft+5NPx9d1z0BAAD//9xdsQpDIQy0okW7ODoI/v9vCbpmemitIuUeWmzpFzw3twyJMcnl7vIBss4fSD0cda9T1kv4cd5f5hMiuqFuiTHyEIJIKUkiEmgZ55wRKCfWC0EH3fFSyr3WiiJft9YA4ceuC/bwIWqILcrHtAP2iI30ekz9EQxCX5zzAg5aBMb8Lh1SymMO754gulBK9SnAeSpbaa1ByteNMd0517z3mF0MQM2ttWPHmq1O1LbstGcLXEEbi4wBmYYvoutLH8bYGwAA///knTELAyEMhfWskziK//+XObkruBVr+WhCpXS85bgs6uTiIy8vibkNQNR+ugApmEMefggwlnqWvd5r77iTB7PGGLb3bpGPSymu1upaa0jGDtAQvzDmAUpGPoUVqqJn860WPvQesU/gIrSGIaF7PzwUiS2dlXyF471nWu0MIUzAkFICFK+c88Q7xBgXo/CgTgr+XZX7Aworn2KjWD1FkbpU9vs0M8a8AQAA///knbEKACEMQyseuDr5/5/n4ODickuPJ9Ur/oJCcXQKaSJNrwOIPwerPI5ZZhwRYDERvNswZ/fuVswnfowxAM7UMr330FoLtVaWBgEmKlo+V8AtAzBLADudouu/JaWkuEsAgncQ1DlnGEBLKYhrCk2htErcfqZc/hXXclivutLhrX2KtkbvNebaU2K3gWIfEfkAAAD//+SdsRKAMAhDU8/+/9+yOOi9Sk6O0VG7MHUjbUMD+TVA6mon+ExTnD2NXG6UPJxFleSfzU5hay5ZftcDGBIb7sLNQ7Tmiw47D8VDOesmJfYnSFbyR8QgetA25Ve1MaG917z8mMucIuPSXKGTSkAcX61GvVqSLgAAAP//Gg0FLADLchbQAkl2aA3zHymzMCANs2JkHBhAOpAAYzEkse75j3SSCPJ1DFhOtoe7AzbMjA5AGQE6PPt3qC0epCtgYGAAAAAA//8aDREiAFJNAe4HQ5tjsNqFiQGhBpZ5GJCaZjCA0p8hZ00V2tZfbBkBxmFEyjSwDv9f6MX6f+l9fOeQBQwMDAAAAAD//xoNITIAKJE+efKEQUpKCjx8DB19AmUaNmhtAzYUbfIQniPQMg6hnII894HBRhp9YoQuUf8HvXHpD9qVbaMZglTAwMAAAAAA//8aDTEqArR+DCO0ecbEgHoVGyPyvefIGQB5MhEJwAz9j3RAAizh/4fWDMj80YxALcDAwAAAAAD//wMArZYALnHnrJEAAAAASUVORK5CYII="

/***/ }),

/***/ 88:
/*!*********************************************************************!*\
  !*** C:/Users/王佳豪/Desktop/wyy/node_modules/pubsub-js/src/pubsub.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module) {/**
 * Copyright (c) 2010,2011,2012,2013,2014 Morgan Roderick http://roderick.dk
 * License: MIT - http://mrgnrdrck.mit-license.org
 *
 * https://github.com/mroderick/PubSubJS
 */

(function (root, factory) {
  'use strict';

  var PubSub = {};

  if (root.PubSub) {
    PubSub = root.PubSub;
    console.warn("PubSub already loaded, using existing version");
  } else {
    root.PubSub = PubSub;
    factory(PubSub);
  }
  // CommonJS and Node.js module support
  if (true) {
    if (module !== undefined && module.exports) {
      exports = module.exports = PubSub; // Node.js specific `module.exports`
    }
    exports.PubSub = PubSub; // CommonJS module 1.1.1 spec
    module.exports = exports = PubSub; // CommonJS
  }
  // AMD support
  /* eslint-disable no-undef */else
    {}

})(typeof window === 'object' && window || this, function (PubSub) {
  'use strict';

  var messages = {},
  lastUid = -1,
  ALL_SUBSCRIBING_MSG = '*';

  function hasKeys(obj) {
    var key;

    for (key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        return true;
      }
    }
    return false;
  }

  /**
     * Returns a function that throws the passed exception, for use as argument for setTimeout
     * @alias throwException
     * @function
     * @param { Object } ex An Error object
     */
  function throwException(ex) {
    return function reThrowException() {
      throw ex;
    };
  }

  function callSubscriberWithDelayedExceptions(subscriber, message, data) {
    try {
      subscriber(message, data);
    } catch (ex) {
      setTimeout(throwException(ex), 0);
    }
  }

  function callSubscriberWithImmediateExceptions(subscriber, message, data) {
    subscriber(message, data);
  }

  function deliverMessage(originalMessage, matchedMessage, data, immediateExceptions) {
    var subscribers = messages[matchedMessage],
    callSubscriber = immediateExceptions ? callSubscriberWithImmediateExceptions : callSubscriberWithDelayedExceptions,
    s;

    if (!Object.prototype.hasOwnProperty.call(messages, matchedMessage)) {
      return;
    }

    for (s in subscribers) {
      if (Object.prototype.hasOwnProperty.call(subscribers, s)) {
        callSubscriber(subscribers[s], originalMessage, data);
      }
    }
  }

  function createDeliveryFunction(message, data, immediateExceptions) {
    return function deliverNamespaced() {
      var topic = String(message),
      position = topic.lastIndexOf('.');

      // deliver the message as it is now
      deliverMessage(message, message, data, immediateExceptions);

      // trim the hierarchy and deliver message to each level
      while (position !== -1) {
        topic = topic.substr(0, position);
        position = topic.lastIndexOf('.');
        deliverMessage(message, topic, data, immediateExceptions);
      }

      deliverMessage(message, ALL_SUBSCRIBING_MSG, data, immediateExceptions);
    };
  }

  function hasDirectSubscribersFor(message) {
    var topic = String(message),
    found = Boolean(Object.prototype.hasOwnProperty.call(messages, topic) && hasKeys(messages[topic]));

    return found;
  }

  function messageHasSubscribers(message) {
    var topic = String(message),
    found = hasDirectSubscribersFor(topic) || hasDirectSubscribersFor(ALL_SUBSCRIBING_MSG),
    position = topic.lastIndexOf('.');

    while (!found && position !== -1) {
      topic = topic.substr(0, position);
      position = topic.lastIndexOf('.');
      found = hasDirectSubscribersFor(topic);
    }

    return found;
  }

  function publish(message, data, sync, immediateExceptions) {
    message = typeof message === 'symbol' ? message.toString() : message;

    var deliver = createDeliveryFunction(message, data, immediateExceptions),
    hasSubscribers = messageHasSubscribers(message);

    if (!hasSubscribers) {
      return false;
    }

    if (sync === true) {
      deliver();
    } else {
      setTimeout(deliver, 0);
    }
    return true;
  }

  /**
     * Publishes the message, passing the data to it's subscribers
     * @function
     * @alias publish
     * @param { String } message The message to publish
     * @param {} data The data to pass to subscribers
     * @return { Boolean }
     */
  PubSub.publish = function (message, data) {
    return publish(message, data, false, PubSub.immediateExceptions);
  };

  /**
      * Publishes the message synchronously, passing the data to it's subscribers
      * @function
      * @alias publishSync
      * @param { String } message The message to publish
      * @param {} data The data to pass to subscribers
      * @return { Boolean }
      */
  PubSub.publishSync = function (message, data) {
    return publish(message, data, true, PubSub.immediateExceptions);
  };

  /**
      * Subscribes the passed function to the passed message. Every returned token is unique and should be stored if you need to unsubscribe
      * @function
      * @alias subscribe
      * @param { String } message The message to subscribe to
      * @param { Function } func The function to call when a new message is published
      * @return { String }
      */
  PubSub.subscribe = function (message, func) {
    if (typeof func !== 'function') {
      return false;
    }

    message = typeof message === 'symbol' ? message.toString() : message;

    // message is not registered yet
    if (!Object.prototype.hasOwnProperty.call(messages, message)) {
      messages[message] = {};
    }

    // forcing token as String, to allow for future expansions without breaking usage
    // and allow for easy use as key names for the 'messages' object
    var token = 'uid_' + String(++lastUid);
    messages[message][token] = func;

    // return token for unsubscribing
    return token;
  };

  PubSub.subscribeAll = function (func) {
    return PubSub.subscribe(ALL_SUBSCRIBING_MSG, func);
  };

  /**
      * Subscribes the passed function to the passed message once
      * @function
      * @alias subscribeOnce
      * @param { String } message The message to subscribe to
      * @param { Function } func The function to call when a new message is published
      * @return { PubSub }
      */
  PubSub.subscribeOnce = function (message, func) {
    var token = PubSub.subscribe(message, function () {
      // before func apply, unsubscribe message
      PubSub.unsubscribe(token);
      func.apply(this, arguments);
    });
    return PubSub;
  };

  /**
      * Clears all subscriptions
      * @function
      * @public
      * @alias clearAllSubscriptions
      */
  PubSub.clearAllSubscriptions = function clearAllSubscriptions() {
    messages = {};
  };

  /**
      * Clear subscriptions by the topic
      * @function
      * @public
      * @alias clearAllSubscriptions
      * @return { int }
      */
  PubSub.clearSubscriptions = function clearSubscriptions(topic) {
    var m;
    for (m in messages) {
      if (Object.prototype.hasOwnProperty.call(messages, m) && m.indexOf(topic) === 0) {
        delete messages[m];
      }
    }
  };

  /**
        Count subscriptions by the topic
      * @function
      * @public
      * @alias countSubscriptions
      * @return { Array }
     */
  PubSub.countSubscriptions = function countSubscriptions(topic) {
    var m;
    // eslint-disable-next-line no-unused-vars
    var token;
    var count = 0;
    for (m in messages) {
      if (Object.prototype.hasOwnProperty.call(messages, m) && m.indexOf(topic) === 0) {
        for (token in messages[m]) {
          count++;
        }
        break;
      }
    }
    return count;
  };


  /**
        Gets subscriptions by the topic
      * @function
      * @public
      * @alias getSubscriptions
     */
  PubSub.getSubscriptions = function getSubscriptions(topic) {
    var m;
    var list = [];
    for (m in messages) {
      if (Object.prototype.hasOwnProperty.call(messages, m) && m.indexOf(topic) === 0) {
        list.push(m);
      }
    }
    return list;
  };

  /**
      * Removes subscriptions
      *
      * - When passed a token, removes a specific subscription.
      *
     * - When passed a function, removes all subscriptions for that function
      *
     * - When passed a topic, removes all subscriptions for that topic (hierarchy)
      * @function
      * @public
      * @alias subscribeOnce
      * @param { String | Function } value A token, function or topic to unsubscribe from
      * @example // Unsubscribing with a token
      * var token = PubSub.subscribe('mytopic', myFunc);
      * PubSub.unsubscribe(token);
      * @example // Unsubscribing with a function
      * PubSub.unsubscribe(myFunc);
      * @example // Unsubscribing from a topic
      * PubSub.unsubscribe('mytopic');
      */
  PubSub.unsubscribe = function (value) {
    var descendantTopicExists = function descendantTopicExists(topic) {
      var m;
      for (m in messages) {
        if (Object.prototype.hasOwnProperty.call(messages, m) && m.indexOf(topic) === 0) {
          // a descendant of the topic exists:
          return true;
        }
      }

      return false;
    },
    isTopic = typeof value === 'string' && (Object.prototype.hasOwnProperty.call(messages, value) || descendantTopicExists(value)),
    isToken = !isTopic && typeof value === 'string',
    isFunction = typeof value === 'function',
    result = false,
    m,message,t;

    if (isTopic) {
      PubSub.clearSubscriptions(value);
      return;
    }

    for (m in messages) {
      if (Object.prototype.hasOwnProperty.call(messages, m)) {
        message = messages[m];

        if (isToken && message[value]) {
          delete message[value];
          result = value;
          // tokens are unique, so we can just stop here
          break;
        }

        if (isFunction) {
          for (t in message) {
            if (Object.prototype.hasOwnProperty.call(message, t) && message[t] === value) {
              delete message[t];
              result = true;
            }
          }
        }
      }
    }

    return result;
  };
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! (webpack)/buildin/module.js */ 89)(module)))

/***/ }),

/***/ 89:
/*!***********************************!*\
  !*** (webpack)/buildin/module.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = function(module) {
	if (!module.webpackPolyfill) {
		module.deprecate = function() {};
		module.paths = [];
		// module.parent = undefined by default
		if (!module.children) module.children = [];
		Object.defineProperty(module, "loaded", {
			enumerable: true,
			get: function() {
				return module.l;
			}
		});
		Object.defineProperty(module, "id", {
			enumerable: true,
			get: function() {
				return module.i;
			}
		});
		module.webpackPolyfill = 1;
	}
	return module;
};


/***/ }),

/***/ 90:
/*!****************************************************************!*\
  !*** C:/Users/王佳豪/Desktop/wyy/node_modules/dayjs/dayjs.min.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

!function (t, e) { true ? module.exports = e() : undefined;}(this, function () {"use strict";var t = 1e3,e = 6e4,n = 36e5,r = "millisecond",i = "second",s = "minute",u = "hour",a = "day",o = "week",f = "month",h = "quarter",c = "year",d = "date",$ = "Invalid Date",l = /^(\d{4})[-/]?(\d{1,2})?[-/]?(\d{0,2})[Tt\s]*(\d{1,2})?:?(\d{1,2})?:?(\d{1,2})?[.:]?(\d+)?$/,y = /\[([^\]]+)]|Y{1,4}|M{1,4}|D{1,2}|d{1,4}|H{1,2}|h{1,2}|a|A|m{1,2}|s{1,2}|Z{1,2}|SSS/g,M = { name: "en", weekdays: "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"), months: "January_February_March_April_May_June_July_August_September_October_November_December".split("_") },m = function m(t, e, n) {var r = String(t);return !r || r.length >= e ? t : "" + Array(e + 1 - r.length).join(n) + t;},g = { s: m, z: function z(t) {var e = -t.utcOffset(),n = Math.abs(e),r = Math.floor(n / 60),i = n % 60;return (e <= 0 ? "+" : "-") + m(r, 2, "0") + ":" + m(i, 2, "0");}, m: function t(e, n) {if (e.date() < n.date()) return -t(n, e);var r = 12 * (n.year() - e.year()) + (n.month() - e.month()),i = e.clone().add(r, f),s = n - i < 0,u = e.clone().add(r + (s ? -1 : 1), f);return +(-(r + (n - i) / (s ? i - u : u - i)) || 0);}, a: function a(t) {return t < 0 ? Math.ceil(t) || 0 : Math.floor(t);}, p: function p(t) {return { M: f, y: c, w: o, d: a, D: d, h: u, m: s, s: i, ms: r, Q: h }[t] || String(t || "").toLowerCase().replace(/s$/, "");}, u: function u(t) {return void 0 === t;} },v = "en",D = {};D[v] = M;var p = function p(t) {return t instanceof _;},S = function t(e, n, r) {var i;if (!e) return v;if ("string" == typeof e) {var s = e.toLowerCase();D[s] && (i = s), n && (D[s] = n, i = s);var u = e.split("-");if (!i && u.length > 1) return t(u[0]);} else {var a = e.name;D[a] = e, i = a;}return !r && i && (v = i), i || !r && v;},w = function w(t, e) {if (p(t)) return t.clone();var n = "object" == typeof e ? e : {};return n.date = t, n.args = arguments, new _(n);},O = g;O.l = S, O.i = p, O.w = function (t, e) {return w(t, { locale: e.$L, utc: e.$u, x: e.$x, $offset: e.$offset });};var _ = function () {function M(t) {this.$L = S(t.locale, null, !0), this.parse(t);}var m = M.prototype;return m.parse = function (t) {this.$d = function (t) {var e = t.date,n = t.utc;if (null === e) return new Date(NaN);if (O.u(e)) return new Date();if (e instanceof Date) return new Date(e);if ("string" == typeof e && !/Z$/i.test(e)) {var r = e.match(l);if (r) {var i = r[2] - 1 || 0,s = (r[7] || "0").substring(0, 3);return n ? new Date(Date.UTC(r[1], i, r[3] || 1, r[4] || 0, r[5] || 0, r[6] || 0, s)) : new Date(r[1], i, r[3] || 1, r[4] || 0, r[5] || 0, r[6] || 0, s);}}return new Date(e);}(t), this.$x = t.x || {}, this.init();}, m.init = function () {var t = this.$d;this.$y = t.getFullYear(), this.$M = t.getMonth(), this.$D = t.getDate(), this.$W = t.getDay(), this.$H = t.getHours(), this.$m = t.getMinutes(), this.$s = t.getSeconds(), this.$ms = t.getMilliseconds();}, m.$utils = function () {return O;}, m.isValid = function () {return !(this.$d.toString() === $);}, m.isSame = function (t, e) {var n = w(t);return this.startOf(e) <= n && n <= this.endOf(e);}, m.isAfter = function (t, e) {return w(t) < this.startOf(e);}, m.isBefore = function (t, e) {return this.endOf(e) < w(t);}, m.$g = function (t, e, n) {return O.u(t) ? this[e] : this.set(n, t);}, m.unix = function () {return Math.floor(this.valueOf() / 1e3);}, m.valueOf = function () {return this.$d.getTime();}, m.startOf = function (t, e) {var n = this,r = !!O.u(e) || e,h = O.p(t),$ = function $(t, e) {var i = O.w(n.$u ? Date.UTC(n.$y, e, t) : new Date(n.$y, e, t), n);return r ? i : i.endOf(a);},l = function l(t, e) {return O.w(n.toDate()[t].apply(n.toDate("s"), (r ? [0, 0, 0, 0] : [23, 59, 59, 999]).slice(e)), n);},y = this.$W,M = this.$M,m = this.$D,g = "set" + (this.$u ? "UTC" : "");switch (h) {case c:return r ? $(1, 0) : $(31, 11);case f:return r ? $(1, M) : $(0, M + 1);case o:var v = this.$locale().weekStart || 0,D = (y < v ? y + 7 : y) - v;return $(r ? m - D : m + (6 - D), M);case a:case d:return l(g + "Hours", 0);case u:return l(g + "Minutes", 1);case s:return l(g + "Seconds", 2);case i:return l(g + "Milliseconds", 3);default:return this.clone();}}, m.endOf = function (t) {return this.startOf(t, !1);}, m.$set = function (t, e) {var n,o = O.p(t),h = "set" + (this.$u ? "UTC" : ""),$ = (n = {}, n[a] = h + "Date", n[d] = h + "Date", n[f] = h + "Month", n[c] = h + "FullYear", n[u] = h + "Hours", n[s] = h + "Minutes", n[i] = h + "Seconds", n[r] = h + "Milliseconds", n)[o],l = o === a ? this.$D + (e - this.$W) : e;if (o === f || o === c) {var y = this.clone().set(d, 1);y.$d[$](l), y.init(), this.$d = y.set(d, Math.min(this.$D, y.daysInMonth())).$d;} else $ && this.$d[$](l);return this.init(), this;}, m.set = function (t, e) {return this.clone().$set(t, e);}, m.get = function (t) {return this[O.p(t)]();}, m.add = function (r, h) {var d,$ = this;r = Number(r);var l = O.p(h),y = function y(t) {var e = w($);return O.w(e.date(e.date() + Math.round(t * r)), $);};if (l === f) return this.set(f, this.$M + r);if (l === c) return this.set(c, this.$y + r);if (l === a) return y(1);if (l === o) return y(7);var M = (d = {}, d[s] = e, d[u] = n, d[i] = t, d)[l] || 1,m = this.$d.getTime() + r * M;return O.w(m, this);}, m.subtract = function (t, e) {return this.add(-1 * t, e);}, m.format = function (t) {var e = this,n = this.$locale();if (!this.isValid()) return n.invalidDate || $;var r = t || "YYYY-MM-DDTHH:mm:ssZ",i = O.z(this),s = this.$H,u = this.$m,a = this.$M,o = n.weekdays,f = n.months,h = function h(t, n, i, s) {return t && (t[n] || t(e, r)) || i[n].slice(0, s);},c = function c(t) {return O.s(s % 12 || 12, t, "0");},d = n.meridiem || function (t, e, n) {var r = t < 12 ? "AM" : "PM";return n ? r.toLowerCase() : r;},l = { YY: String(this.$y).slice(-2), YYYY: this.$y, M: a + 1, MM: O.s(a + 1, 2, "0"), MMM: h(n.monthsShort, a, f, 3), MMMM: h(f, a), D: this.$D, DD: O.s(this.$D, 2, "0"), d: String(this.$W), dd: h(n.weekdaysMin, this.$W, o, 2), ddd: h(n.weekdaysShort, this.$W, o, 3), dddd: o[this.$W], H: String(s), HH: O.s(s, 2, "0"), h: c(1), hh: c(2), a: d(s, u, !0), A: d(s, u, !1), m: String(u), mm: O.s(u, 2, "0"), s: String(this.$s), ss: O.s(this.$s, 2, "0"), SSS: O.s(this.$ms, 3, "0"), Z: i };return r.replace(y, function (t, e) {return e || l[t] || i.replace(":", "");});}, m.utcOffset = function () {return 15 * -Math.round(this.$d.getTimezoneOffset() / 15);}, m.diff = function (r, d, $) {var l,y = O.p(d),M = w(r),m = (M.utcOffset() - this.utcOffset()) * e,g = this - M,v = O.m(this, M);return v = (l = {}, l[c] = v / 12, l[f] = v, l[h] = v / 3, l[o] = (g - m) / 6048e5, l[a] = (g - m) / 864e5, l[u] = g / n, l[s] = g / e, l[i] = g / t, l)[y] || g, $ ? v : O.a(v);}, m.daysInMonth = function () {return this.endOf(f).$D;}, m.$locale = function () {return D[this.$L];}, m.locale = function (t, e) {if (!t) return this.$L;var n = this.clone(),r = S(t, e, !0);return r && (n.$L = r), n;}, m.clone = function () {return O.w(this.$d, this);}, m.toDate = function () {return new Date(this.valueOf());}, m.toJSON = function () {return this.isValid() ? this.toISOString() : null;}, m.toISOString = function () {return this.$d.toISOString();}, m.toString = function () {return this.$d.toUTCString();}, M;}(),T = _.prototype;return w.prototype = T, [["$ms", r], ["$s", i], ["$m", s], ["$H", u], ["$W", a], ["$M", f], ["$y", c], ["$D", d]].forEach(function (t) {T[t[1]] = function (e) {return this.$g(e, t[0], t[1]);};}), w.extend = function (t, e) {return t.$i || (t(e, _, w), t.$i = !0), w;}, w.locale = S, w.isDayjs = p, w.unix = function (t) {return w(1e3 * t);}, w.en = D[v], w.Ls = D, w.p = {}, w;});

/***/ })

}]);
//# sourceMappingURL=../../.sourcemap/mp-weixin/common/vendor.js.map