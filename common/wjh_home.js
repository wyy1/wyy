
import {baseURL} from './config'

 export function banner(){
	return new Promise(function(res,req){
		 uni.request({
		 		 url:`${baseURL}/banner`,
		 		 method:"GET",
		 		 data:{},
		 		 success:item=> {
		 			 res(item.data)
		 		 }
		 })
	 }
	)
 }
 export function ground(){
 	return new Promise(function(res,req){
 		 uni.request({
 		 		 url:`${baseURL}/homepage/dragon/ball/limit=8`,
 		 		 method:"GET",
 		 		 data:{},
 		 		 success:item=> {
 		 			 res(item.data)
 		 		 }
 		 })
 	 }
 	)
 }
 // 推荐歌单
 export function tui(){
 	return new Promise(function(res,req){
 		 uni.request({
 		 		 url:`${baseURL}/personalized?limit=6`,
 		 		 method:"GET",
 		 		 data:{},
 		 		 success:item=> {
 		 			 res(item.data)
 		 		 }
 		 })
 	 }
 	)
 }
 // 推荐歌单
 export function alge(){
 	return new Promise(function(res,req){
 		 uni.request({
 		 		 url:`${baseURL}/personalized/newsong`,
 		 		 method:"GET",
 		 		 data:{},
 		 		 success:item=> {
 		 			 res(item.data)
 		 		 }
 		 })
 	 }
 	)
 }
 // 获取歌单
 export function song(id){
 	return new Promise(function(res,req){
 		 uni.request({
 		 		 url:`${baseURL}/playlist/detail?id=${id}`,
 		 		 method:"GET",
 		 		 data:{},
 		 		 success:item=> {
 		 			 res(item.data)
 		 		 }
 		 })
 	 }
 	)
 }
 // 获取歌单详情
 export function allsong(id){
 	return new Promise(function(res,req){
 		 uni.request({
 		 		 url:`${baseURL}/playlist/track/all?id=${id}`,
 		 		 method:"GET",
 		 		 data:{},
 		 		 success:item=> {
 		 			 res(item.data)
 		 		 }
 		 })
 	 }
 	)
 }
 // 播放歌词
 export function plays(id){
 	return new Promise(function(res,req){
 		 uni.request({
 		 		 url:`${baseURL}/lyric?id=${id}`,
 		 		 method:"GET",
 		 		 data:{},
 		 		 success:item=> {
 		 			 res(item.data)
 		 		 }
 		 })
 	 }
 	)
 }
 // 获取歌单详情
 export function play(id){
 	return new Promise(function(res,req){
 		 uni.request({
 		 		 url:`${baseURL}/song/detail?ids=${id}`,
 		 		 method:"GET",
 		 		 data:{},
 		 		 success:item=> {
 		 			 res(item.data)
 		 		 }
 		 })
 	 }
 	)
 }
 // 播放歌曲
 export function playAuido(id){
 	return new Promise(function(res,req){
 		 uni.request({
 		 		 url:`${baseURL}/song/url?id=${id}`,
 		 		 method:"GET",
 		 		 data:{},
 		 		 success:item=> {
 		 			 res(item.data)
 		 		 }
 		 })
 	 }
 	)
 }
 
 
 
